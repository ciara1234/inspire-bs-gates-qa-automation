#Writing test results to Testrail using api calls
#Also validates the response back from TestRail Api to ensure a comment was added
#https://iii.testrail.com/index.php?/cases/view/836373

Feature: Make a test run, add a test case, and add a comment and pass the test, then fail the test, then add an attachment to the result
Background:

  * def testRailData = __arg

  * def credentials = read('../../testrail/testrail-credentials.json')
  * header Authorization = call read('../../testrail/basic-auth.js') { username: '#(credentials.username)', password: '#(credentials.password)' }

  #* def caseId = 836373
  * def suiteId = 2192
  * def projectId = 24

  * print testRailData.caseId
  * def caseId = testRailData.caseId

  Scenario: get userid, create a run, add a test and add results etc

    Given url 'https://iii.testrail.com/index.php?/api/v2/get_user_by_email'
    And param email = credentials.username
    And header Content-Type = 'application/json'
    When method get
    Then status 200
    * def userId = response.id

    Given url 'https://iii.testrail.com/index.php?/api/v2/add_run/'+ projectId
    And header Authorization = call read('../../testrail/basic-auth.js') { username: '#(credentials.username)', password: '#(credentials.password)' }
    And header Content-Type = 'application/json'
    And request
  """
  {
  "suite_id": #(suiteId),
  "name": #(runName),
  "assignedto_id": #(userId),
  "include_all": false,
  "case_ids": [#(testRailData.caseId)]
  }
  """
    When method POST
    Then status 200
    * def runId = response.id

    Given url 'https://iii.testrail.com/index.php?/api/v2/get_tests/' + runId
    And header Authorization = call read('../../testrail/basic-auth.js') { username: '#(credentials.username)', password: '#(credentials.password)' }
    And header Content-Type = 'application/json'
    When method get
    Then status 200
    * def testId = response[0].case_id


    Given url 'https://iii.testrail.com/index.php?/api/v2/add_result_for_case/' + runId + '/' + testId
    * def credentials = read('../../testrail/testrail-credentials.json')
    * header Authorization = call read('../../testrail/basic-auth.js') { username: '#(credentials.username)', password: '#(credentials.password)' }
    * def testResults = "yeet! let's get it!"
    And header Content-Type = 'application/json'
    And request
   """
   {
       	comment: '#(testResults)',
       	status_id: 1
   }
   """
    When method POST
    Then status 200

    Given url 'https://iii.testrail.com/index.php?/api/v2/add_result_for_case/' + runId + '/' + testId
    * def credentials = read('../../testrail/testrail-credentials.json')
    * header Authorization = call read('../../testrail/basic-auth.js') { username: '#(credentials.username)', password: '#(credentials.password)' }
    * def testResults = "yeet! let's get it!"
    And header Content-Type = 'application/json'
    And request
   """
   {
       	comment: '#(testResults + " This is extra")',
       	status_id: 5
   }
   """
    When method POST
    Then status 200
    * def resultId = response.id

    Given url 'https://iii.testrail.com/index.php?/api/v2/add_attachment_to_result/' + resultId
    * def credentials = read('../../testrail/testrail-credentials.json')
    * header Authorization = call read('../../testrail/basic-auth.js') { username: '#(credentials.username)', password: '#(credentials.password)' }
    And header Content-Type = 'multipart/form-data'
    And multipart file attachment = { read: '../../testrail/karate.png', filename: 'karate.png', contentType: 'image/png' }
    When method POST
    Then status 200

