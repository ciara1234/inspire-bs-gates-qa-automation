@kctoken
@ignore
Feature: Create Keycloak token for env
  Background:
    * def resJson = {url : 'https://performance-indexing.iii-conv.com'}
    * print resJson
    * print resJson.url
    * def host = resJson.url.substring(resJson.url.indexOf('/') + 2)
    * print host

  Scenario: Get Keycloak Token for ADMIN role

    Given url 'https://auth.' + host + '/auth/realms/admin/protocol/openid-connect/token'
    And header 'Content-Type' = 'application/x-www-form-urlencoded'
    And form field grant_type = 'password'
    And form field username = 'admin'
    And form field password = 'admin'
    And form field client_id = 'convergence'
    When method post
    Then status 200
  #  * def access_token = response.access_token


  Scenario: Get Keycloak Token for master realm, admin role

    Given url 'https://auth.' + host + '/auth/realms/master/protocol/openid-connect/token'
    And header 'Content-Type' = 'application/x-www-form-urlencoded'
    And form field grant_type = 'password'
    And form field username = 'admin'
    And form field password = 'admin'
    And form field client_id = 'admin-cli'
    When method post
    Then status 200
