@kctoken
@ignore
Feature: Disable LDAP for the customer so that Patron UI can be used
  Background:
    #* def resJson = __arg
    * def resJson = { url: "https://sandbox-stargates.iii-inspire.dev"}
    * print resJson
    * print resJson.url
    * def host = resJson.url.substring(resJson.url.indexOf('/') + 2)
    * print host
    * def siteCode = 'patronholds1565866624294'

  #Scenario: Disable LDAP
    #to do
    #make api call
    #to disable ldap on keycloak realm
    #ldap must be disabled in keycloak
    #
    #https://auth.ds-conv-is-gates-edge-pr-40.iii-inspire.dev/auth/admin/master/console/#/realms
      #https://auth.sandbox-stargates.iii-inspire.dev/auth/admin/master/console/#/realms

#ds-inspire-bs-get-it-pr-4.iii-inspire.dev
  #https://auth.ds-inspire-bs-get-it-pr-4.iii-inspire.dev/auth/admin/master/console/#/realms

#patronholds1565866624294
  Scenario: Get Keycloak Token for master realm, admin role

    Given url 'https://auth.' + host + '/auth/realms/master/protocol/openid-connect/token'
    And header 'Content-Type' = 'application/x-www-form-urlencoded'
    And form field grant_type = 'password'
    And form field username = 'admin'
    And form field password = 'admin'
    And form field client_id = 'admin-cli'
    When method post
    Then status 200


    * def accessToken = response.access_token

    # get all realms - we need master token, admin is not enough
    Given url 'https://auth.' + host + '/auth/admin/realms'
    And header Authorization = 'Bearer ' + accessToken
    And header Accept = 'application/json'
    And header Content-Type = 'application/json'
    When method get
    Then status 200


    * def responseBody = response

    #* def temp = karate.jsonPath(cat, "$.kittens[?(@.name=='" + bob.name + "')]")
    #[?(@.realm=="patronholds1565866624294")].id

    * string realmId = karate.jsonPath(responseBody,"[?(@.realm=='patronholds1565866624294')].id")

  * def parentId = realmId.substring(2, realmId.length - 2);

    * print parentId
    #* def parentId = response.

#where do I get 713eed0d-d87f-4fd0-abe1-8b5035b5e6ba

    Given url 'https://auth.' + host + '/auth/admin/realms/patronholds1565866624294/components?parent=' + parentId
    And header Authorization = 'Bearer ' + accessToken
    And header Accept = 'application/json'
    And header Content-Type = 'application/json'
    When method get
    Then status 200
    * def aResponseBody = response

    * string temp = karate.jsonPath(aResponseBody,"[?(@.name=='iii-ldap')].id")

    * print temp

    * def otherId = temp.substring(2, temp.length - 2);

    * print realmId

    * print otherId

    * def iiiLdap =
    """
    {
    "config": {
        "allowKerberosAuthentication": [
            "false"
        ],
        "authType": [
            "simple"
        ],
        "batchSizeForSync": [
            "1000"
        ],
        "bindCredential": [
            "**********"
        ],
        "bindDn": [
            "CN=Next Gen LDAP Read,OU=Service Accounts,OU=Global Accounts,OU=Innovative,DC=iii,DC=com"
        ],
        "cachePolicy": [
            "DEFAULT"
        ],
        "changedSyncPeriod": [
            "-1"
        ],
        "connectionPooling": [
            "true"
        ],
        "connectionPoolingAuthentication": [
        ],
        "connectionPoolingDebug": [
        ],
        "connectionPoolingInitSize": [
        ],
        "connectionPoolingMaxSize": [
        ],
        "connectionPoolingPrefSize": [
        ],
        "connectionPoolingProtocol": [
        ],
        "connectionPoolingTimeout": [
        ],
        "connectionTimeout": [
        ],
        "connectionUrl": [
            "ldaps://oakdc-dc-01.iii.com"
        ],
        "customUserSearchFilter": [
        ],
        "debug": [
            "false"
        ],
        "editMode": [
            "READ_ONLY"
        ],
        "enabled": [
            "false"
        ],
        "evictionDay": [
        ],
        "evictionHour": [
        ],
        "evictionMinute": [
        ],
        "fullSyncPeriod": [
            "-1"
        ],
        "importEnabled": [
            "false"
        ],
        "kerberosRealm": [
        ],
        "keyTab": [
        ],
        "maxLifespan": [
        ],
        "pagination": [
            "true"
        ],
        "priority": [
            "0"
        ],
        "rdnLDAPAttribute": [
            "cn"
        ],
        "readTimeout": [
        ],
        "searchScope": [
            "2"
        ],
        "serverPrincipal": [
        ],
        "syncRegistrations": [
            "false"
        ],
        "useKerberosForPasswordAuthentication": [
            "false"
        ],
        "usernameLDAPAttribute": [
            "sAMAccountName"
        ],
        "userObjectClasses": [
            "user"
        ],
        "usersDn": [
            "OU=Innovative,DC=iii,DC=com"
        ],
        "useTruststoreSpi": [
            "ldapsOnly"
        ],
        "uuidLDAPAttribute": [
            "objectGUID"
        ],
        "validatePasswordPolicy": [
            "false"
        ],
        "vendor": [
            "ad"
        ]
    },
    "id": "713eed0d-d87f-4fd0-abe1-8b5035b5e6ba",
    "name": "iii-ldap",
    "parentId": "767f8634-9511-427b-979d-28390baa15cb",
    "providerId": "ldap",
    "providerType": "org.keycloak.storage.UserStorageProvider"
}
    """


    * set iiiLdap.parentId = parentId
    * set iiiLdap.id = otherId
    * set iiiLdap.config.enabled = false

    * print iiiLdap


    Given url 'https://auth.' + host + '/auth/admin/realms/patronholds1565866624294/components/' + parentId
    And header Authorization = 'Bearer ' + accessToken
    And header Accept = 'application/json'
    And header Content-Type = 'application/json'
    And request iiiLdap
    When method put
    Then status 200


    #PUT /auth/admin/realms/patronholds1565866624294/components/713eed0d-d87f-4fd0-abe1-8b5035b5e6ba HTTP/1.1
