@kctoken
@ignore
Feature: delete realm in keycloak after archiving customer
  Background:
   # * def resJson = __arg
   # * print resJson
   # * print resJson.url
   # * def host = resJson.url.substring(resJson.url.indexOf('/') + 2)
   # * print host

  Scenario: get auth token


    Given url 'https://auth.ds-devops-9283.iii-inspire.dev/auth/realms/master/protocol/openid-connect/token'
    And header 'Content-Type' = 'application/x-www-form-urlencoded'
    And form field grant_type = 'password'
    And form field username = 'admin'
    And form field password = 'admin'
    And form field client_id = 'convergence'
    When method post
    Then status 200
    * def access_token = response.access_token

    Given url 'https://auth.ds-devops-9283.iii-inspire.dev/auth/admin/realms/gates1564149847930'
    And header Authorization = 'Bearer ' + access_token
    And header Content-Type = 'application/json'
    When method delete
    Then status 200

  #login to https://auth.staging.iii-conv.com/auth/admin/master/console/#/realms
  # afmin amin

  # then call DELETE /auth/admin/realms/patronholds1563552650480

   # Given url 'https://auth.' + host + '/auth/realms/admin/protocol/openid-connect/token'
   # And header 'Content-Type' = 'application/x-www-form-urlencoded'
   # And form field grant_type = 'password'
   # And form field username = 'admin'
   # And form field password = 'admin'
   # And form field client_id = 'convergence'
   # When method post
   # Then status 200
  #  * def access_token = response.access_token



  #DELETE http://{{keycloak_host}}:{{keycloak_port}}/auth/admin/realms/{{working_realm}}
  #Content-Type: application/json
  #Authorization: bearer {{access_token}}
  #


