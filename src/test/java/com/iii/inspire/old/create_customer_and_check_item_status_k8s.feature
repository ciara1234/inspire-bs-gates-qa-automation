@Customer
Feature: Create customer on Customer API and assign connection and and check item status metadata
  Background:
    * def now = function(){ return java.lang.System.currentTimeMillis() }

  Scenario: Get III-Admin Token from Keycloak and Create Customer with Connection and check item status metadata

    * def site_code = "polaris" + now()
    * def cust_domain = "admin.ds-conv-bs-gates-polaris-pr-14.iii-inspire.dev"
    * def envUrl = "ds-conv-bs-gates-polaris-pr-14.iii-inspire.dev"

    Given url 'https://auth.ds-conv-bs-gates-polaris-pr-14.iii-inspire.dev/auth/realms/admin/protocol/openid-connect/token'
    And header 'Content-Type' = 'application/x-www-form-urlencoded'
    And form field grant_type = 'password'
    And form field username = 'admin'
    And form field password = 'admin'
    And form field client_id = 'convergence'
    When method post
    Then status 200
    * def access_token = response.access_token

    Given url 'https://' + envUrl + '/api/customer/customers/'
    And header 'Content-Type' = 'application/json'
    And header Authorization = 'Bearer ' + access_token
    And header iii-customer-domain = cust_domain
    And header api-version = 1
    And request
  """
  {
  name : "Polaris Gates QA Test: Item Status",
  ilsSiteCode :'#(site_code)',
  types : [ {
  "type" : "LIBRARY" }]
  }
  """
    When method post
    Then status 201
    * def customer_id = response.id

    Given url 'https://' + envUrl + '/api/customer/dictionaries/license/names'
    And header 'Content-Type' = 'application/json'
    And header Authorization = 'Bearer ' + access_token
    And header iii-customer-domain = cust_domain
    And header api-version = 1
    When method get
    Then status 200

    * def discovery = response[1].id
    * def context_engine = response[0].id

    Given url 'https://' + envUrl + '/api/customer/customers/' + customer_id + '/licenses'
    And header 'Content-Type' = 'application/json'
    And header Authorization = 'Bearer ' + access_token
    And header iii-customer-domain = cust_domain
    And header api-version = 2
    And request
  """
  {
  licenseName :"Context Engine",
  status : "ACTIVE",
  expirationDate : "2020-10-05"
  }
  """
    When method post
    Then status 201

    Given url 'https://' + envUrl + '/api/customer/customers/' + customer_id + '/licenses'
    And header 'Content-Type' = 'application/json'
    And header Authorization = 'Bearer ' + access_token
    And header iii-customer-domain = cust_domain
    And header api-version = 2
    And request
  """
  {
  licenseName :"Discovery",
  status : "ACTIVE",
  expirationDate : "2020-10-05"
  }
  """
    When method post
    Then status 201

    Given url 'https://' + envUrl + '/api/customer/customers/' + customer_id + '/connection'
    And header 'Content-Type' = 'application/json'
    And header api-version = 3
    And header Authorization = 'Bearer ' + access_token
    And header iii-customer-domain = cust_domain
    And request
  """
  {
      "type" : "polaris",
      "clientId" : "polarisdev",
      "clientSecret" : "4B3C1D34-7BA6-4D5E-8112-1C9A6BB7CD74",
      "apiUrl" : "https://rd-polaris.polarislibrary.com/PAPIService/REST",
      "staffUsername": "polarisexec",
      "staffPassword": "P0laris!@",
      "staffDomain": "iii"
  }
  """
    When method post
    Then status 201

    Given url 'https://' + envUrl + '/api/customer/customers/' + customer_id + '/connection?type=polaris'
    And header 'Content-Type' = 'application/json'
    And header api-version = 3
    And header Authorization = 'Bearer ' + access_token
    And header iii-customer-domain = cust_domain
    When method get
    Then status 200

    * def customer_id = response.customerId


    Given url 'https://' + envUrl + '/api/gates-edge/gates/itemstatus'
    And header Content-Type = 'application/json'
    And header Authorization = 'Bearer ' + access_token
    And header iii-customer-domain = cust_domain
    And header iii-customer-site-code = site_code
    And header iii-customer-id = customer_id
    And header X-III-POLARIS-ORG-ID = 3
    And header Accept-Encoding =  '*'
    And header api-version = 1
    When method get
    Then status 200
    * def expectedResponse =
    """
  {
  "entries": [ {
  "code": "1",
  "description": "In"
  }, {
  "code": "2",
  "description": "Out"
  }, {
  "code": "3",
  "description": "Out-ILL"
  }, {
  "code": "4",
  "description": "Held"
  }, {
  "code": "5",
  "description": "Transferred"
  }, {
  "code": "6",
  "description": "In-Transit"
  }, {
  "code": "7",
  "description": "Lost"
  }, {
  "code": "8",
  "description": "Claim Returned"
  }, {
  "code": "9",
  "description": "Claim Never Had"
  }, {
  "code": "10",
  "description": "Missing"
  }, {
  "code": "11",
  "description": "Withdrawn"
  }, {
  "code": "12",
  "description": "Bindery"
  }, {
  "code": "13",
  "description": "On-Order"
  }, {
  "code": "14",
  "description": "In-Repair"
  }, {
  "code": "15",
  "description": "In-Process"
  }, {
  "code": "16",
  "description": "Unavailable"
  }, {
  "code": "17",
  "description": "Returned-ILL"
  }, {
  "code": "18",
  "description": "Routed"
  }, {
  "code": "19",
  "description": "Shelving"
  }, {
  "code": "20",
  "description": "Non-circulating"
  }, {
  "code": "21",
  "description": "Claim Missing Parts"
  }, {
  "code": "22",
  "description": "EContent External Loan"
  } ]
  }
    """
    * match response contains expectedResponse

    Given url 'https://' + envUrl + '/api/gates-edge/gates/itemstatus'
    And header Content-Type = 'application/json'
    And header Authorization = 'Bearer ' + access_token
    And header iii-customer-domain = cust_domain
    And header iii-customer-site-code = "fake-site"
    And header iii-customer-id = "fake-id"
    And header X-III-POLARIS-ORG-ID = 3
    And header Accept-Encoding =  '*'
    And header api-version = 1
    When method get
    Then status 400
    And match response.error == "Bad Request"
    And match response.message == "Invalid Customer ID format: fake-id"

    Given url 'https://' + envUrl + '/api/gates-edge/gates/itemstatus'
    And header Content-Type = 'application/json'
    And header Authorization = 'Bearer ' + access_token
    And header iii-customer-domain = cust_domain
    And header iii-customer-site-code = "fakesite"
    And header iii-customer-id = "6343a48a-dead-c0de-d00d-0fbccb7a3b2f"
    And header X-III-POLARIS-ORG-ID = 3
    And header Accept-Encoding =  '*'
    And header api-version = 1
    When method get
    Then status 400
    And match response.error == "Bad Request"
    And match response.message == "Customer '6343a48a-dead-c0de-d00d-0fbccb7a3b2f' doesn't exist"
