@Customer
Feature: Create customer on Customer API and assign connection and check locations
  Background:
    * def now = function(){ return java.lang.System.currentTimeMillis() }

  Scenario: Get III-Admin Token from Keycloak and Create Customer with Connection and check locations

    * def site_code = "polaris" + now()
    * def cust_domain = "admin.ds-conv-bs-gates-polaris-pr-15.iii-conv.com"
    * def envUrl = "ds-conv-bs-gates-polaris-pr-15.iii-conv.com"

    Given url 'https://auth.' + envUrl + '/auth/realms/admin/protocol/openid-connect/token'
    And header 'Content-Type' = 'application/x-www-form-urlencoded'
    And form field grant_type = 'password'
    And form field username = 'admin'
    And form field password = 'admin'
    And form field client_id = 'convergence'
    When method post
    Then status 200
    * def access_token = response.access_token

    Given url 'https://' + envUrl + ':10000/api/customer/customers/'
    And header 'Content-Type' = 'application/json'
    And header Authorization = 'Bearer ' + access_token
    And header iii-customer-domain = cust_domain
    And header api-version = 1
    And request
  """
  {
  name : "Polaris Gates QA Test: 12345",
  ilsSiteCode :'#(site_code)',
  types : [ {
  "type" : "LIBRARY" }]
  }
  """
    When method post
    Then status 201
    * def customer_id = response.id

    Given url 'https://' + envUrl + ':10000/api/customer/dictionaries/license/names'
    And header 'Content-Type' = 'application/json'
    And header Authorization = 'Bearer ' + access_token
    And header iii-customer-domain = cust_domain
    And header api-version = 1
    When method get
    Then status 200

    * def discovery = response[1].id
    * def context_engine = response[0].id

    Given url 'https://' + envUrl + ':10000/api/customer/customers/' + customer_id + '/licenses'
    And header 'Content-Type' = 'application/json'
    And header Authorization = 'Bearer ' + access_token
    And header iii-customer-domain = cust_domain
    And header api-version = 2
    And request
  """
  {
  licenseName :"Context Engine",
  status : "ACTIVE",
  expirationDate : "2020-10-05"
  }
  """
    When method post
    Then status 201

    Given url 'https://' + envUrl + ':10000/api/customer/customers/' + customer_id + '/licenses'
    And header 'Content-Type' = 'application/json'
    And header Authorization = 'Bearer ' + access_token
    And header iii-customer-domain = cust_domain
    And header api-version = 2
    And request
  """
  {
  licenseName :"Discovery",
  status : "ACTIVE",
  expirationDate : "2020-10-05"
  }
  """
    When method post
    Then status 201

    Given url 'https://' + envUrl + ':10000/api/customer/customers/' + customer_id + '/connection'
    And header 'Content-Type' = 'application/json'
    And header api-version = 3
    And header Authorization = 'Bearer ' + access_token
    And header iii-customer-domain = cust_domain
    And request
  """
  {
      "type" : "polaris",
      "clientId" : "polarisdev",
      "clientSecret" : "4B3C1D34-7BA6-4D5E-8112-1C9A6BB7CD74",
      "apiUrl" : "https://rd-polaris.polarislibrary.com/PAPIService/REST",
      "staffUsername": "polarisexec",
      "staffPassword": "P0laris!@",
      "staffDomain": "iii"
  }
  """
    When method post
    Then status 201

    Given url 'https://' + envUrl + ':10000/api/customer/customers/' + customer_id + '/connection?type=polaris'
    And header 'Content-Type' = 'application/json'
    And header api-version = 3
    And header Authorization = 'Bearer ' + access_token
    And header iii-customer-domain = cust_domain
    When method get
    Then status 200

    * def customer_id = response.customerId


    Given url 'https://' + envUrl + ':10000/api/gates-edge/gates/locations'
    And header Content-Type = 'application/json'
    And header Authorization = 'Bearer ' + access_token
    And header iii-customer-domain = cust_domain
    And header iii-customer-site-code = site_code
    And header iii-customer-id = customer_id
    And header X-III-POLARIS-ORG-ID = 3
    And header Accept-Encoding =  '*'
    And header api-version = 2
    When method get
    Then status 200


   #  Given url 'https://ds-conv-bs-gates-polaris-pr-10.iii-inspire.dev/api/gates-edge/gates/patrons/21756003332022'
    # And header Content-Type = 'application/json'
     #And header Authorization = 'Bearer ' + access_token
     #And header iii-customer-domain = "admin.ds-conv-bs-gates-polaris-pr-10.iii-inspire.dev"
     #And header iii-customer-site-code = site_code
     #And header iii-customer-id = customer_id
     #And header X-III-POLARIS-ORG-ID = 3
     #And header Accept-Encoding =  '*'
     #And header api-version = 100
     #When method get
     #Then status 500


     #Given url 'https://ds-conv-bs-gates-polaris-pr-10.iii-inspire.dev/api/gates-edge/gates/patrons/21756003332022'
     #And header Content-Type = 'application/json'
     #And header Authorization = 'Bearer ' + access_token
     #And header iii-customer-domain = "admin.ds-conv-bs-gates-polaris-pr-10.iii-inspire.dev"
     #And header iii-customer-site-code = site_code
     #And header iii-customer-id = customer_id
     #And header X-III-POLARIS-ORG-ID = 3
     #And header Accept-Encoding =  '*'
   # And header api-version = 100
     #When method get
     #Then status 500
