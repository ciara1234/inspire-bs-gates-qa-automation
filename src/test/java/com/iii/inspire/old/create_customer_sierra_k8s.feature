@Customer
Feature: Create customer on Customer API and assign connection and check locations
  Background:
    * def now = function(){ return java.lang.System.currentTimeMillis() }

  Scenario: Get III-Admin Token from Keycloak and Create Customer with Connection and check locations

    * def site_code = "sierra" + now()
   # * def site_code = "cairn"
    * def cust_domain = "admin.ds-conv-bs-gates-connector-manager-pr-23.iii-inspire.dev"
    * def envUrl = "ds-conv-bs-gates-connector-manager-pr-23.iii-inspire.dev"

    Given url 'https://auth.ds-conv-bs-gates-connector-manager-pr-23.iii-inspire.dev/auth/realms/admin/protocol/openid-connect/token'
    And header 'Content-Type' = 'application/x-www-form-urlencoded'
    And form field grant_type = 'password'
    And form field username = 'admin'
    And form field password = 'admin'
    And form field client_id = 'convergence'
    When method post
    Then status 200
    * def access_token = response.access_token

    Given url 'https://' + envUrl + '/api/customer/customers/'
    And header 'Content-Type' = 'application/json'
    And header Authorization = 'Bearer ' + access_token
    And header iii-customer-domain = cust_domain
    And header api-version = 1
    And request
  """
  {
  name : "ZZZlastcustomer",
  ilsSiteCode :'#(site_code)',
  types : [ {
  "type" : "LIBRARY" }]
  }
  """
    When method post
    Then status 201
    * def customer_id = response.id

    Given url 'https://' + envUrl + '/api/customer/dictionaries/license/names'
    And header 'Content-Type' = 'application/json'
    And header Authorization = 'Bearer ' + access_token
    And header iii-customer-domain = cust_domain
    And header api-version = 1
    When method get
    Then status 200

    * def discovery = response[1].id
    * def context_engine = response[0].id

    Given url 'https://' + envUrl + '/api/customer/customers/' + customer_id + '/licenses'
    And header 'Content-Type' = 'application/json'
    And header Authorization = 'Bearer ' + access_token
    And header iii-customer-domain = cust_domain
    And header api-version = 2
    And request
  """
  {
  licenseName :"Context Engine",
  status : "ACTIVE",
  expirationDate : "2020-10-05"
  }
  """
    When method post
    Then status 201

    Given url 'https://' + envUrl + '/api/customer/customers/' + customer_id + '/licenses'
    And header 'Content-Type' = 'application/json'
    And header Authorization = 'Bearer ' + access_token
    And header iii-customer-domain = cust_domain
    And header api-version = 2
    And request
  """
  {
  licenseName :"Discovery",
  status : "ACTIVE",
  expirationDate : "2020-10-05"
  }
  """
    When method post
    Then status 201

    Given url 'https://' + envUrl + '/api/customer/customers/' + customer_id + '/connection'
    And header 'Content-Type' = 'application/json'
    And header api-version = 3
    And header Authorization = 'Bearer ' + access_token
    And header iii-customer-domain = cust_domain
    And request
  """
  {
  "type" : "sierra",
  "casUrl" : "https://devops-6916-app.iii-lab.eu/iii/cas",
  "clientId" : "iii",
  "clientSecret" : "29663dev",
  "apiUrl" : "https://devops-8225-app.iii-lab.eu:443/iii/sierra-api",
  "stompLoginHeader" : "message_ro",
  "stompPasscodeHeader" : "aUL5NQtWjkCsEeZAdevops-6916",
  "stompUrl" : "wss://devops-6916-app.iii-lab.eu/messaging"
  }
  """
    When method post
    Then status 201

    Given url 'https://' + envUrl + '/api/customer/customers/' + customer_id + '/connection?type=sierra'
    And header 'Content-Type' = 'application/json'
    And header api-version = 3
    And header Authorization = 'Bearer ' + access_token
    And header iii-customer-domain = cust_domain
    When method get
    Then status 200

    * def customer_id = response.customerId


    #Given url 'https://' + envUrl + '/api/gates-edge/gates/locations'
    #And header Content-Type = 'application/json'
    #And header Authorization = 'Bearer ' + access_token
    #And header iii-customer-domain = cust_domain
    #And header iii-customer-site-code = site_code
    #And header iii-customer-id = customer_id
    #And header X-III-POLARIS-ORG-ID = 3
    #And header Accept-Encoding =  '*'
    #And header api-version = 2
    #When method get
    #Then status 200


   #Scenario: Expire licenses to make customer inactive after test
  # get todays date
  #$ curl 'https://admin.cf-gates.iii-conv.com/api/customer/customers/90ad9e59-2854-4991-bab9-a67aa023696a/licenses/7b59f4a9-74f6-401c-a151-879ea93c40c5' -i -X PUT \
  #-H 'Content-Type: application/json' \
  #-H 'api-version: 2' \
  #-d '{
  #"licenseName" : "Discovery",
  #"status" : "ACTIVE",
  #"expirationDate" : "2019-07-20"
  #}'
