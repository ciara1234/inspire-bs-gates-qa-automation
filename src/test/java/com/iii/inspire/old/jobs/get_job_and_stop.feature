@jobs
Feature: Get Marc Extract Jobs
  Background:

    * def site_code = "jeffa"
    * def cust_domain = "admin.prodlite.iii-conv.com"
    * def envUrl = "prodlite.iii-conv.com"


  Scenario:
    Given url 'https://auth.prodlite.iii-conv.com/auth/realms/admin/protocol/openid-connect/token'
    And header 'Content-Type' = 'application/x-www-form-urlencoded'
    And form field grant_type = 'password'
    And form field username = 'admin'
    And form field password = 'admin'
    And form field client_id = 'convergence'
    When method post
    Then status 200
    * def access_token = response.access_token

    Given url 'https://admin.prodlite.iii-conv.com/api/data-flow-runner/flows/' + '33280e7a-4e1f-46b7-9d25-a5a49eefcfd'
    And header Content-Type = 'application/json'
    And header Content-Type = 'application/json;charset=ISO-8859-1'
    And header Authorization = 'Bearer ' + access_token
    And header iii-customer-domain = "admin.prodlite.iii-conv.com"
    And header iii-customer-site-code = site_code
    And header iii-customer-id = "d310a667-487a-43c5-babf-db19a5c5f72d"
    And header Accept-Encoding =  '*'
    And header api-version = 1
    And request {}
    When method delete
    Then status 200
