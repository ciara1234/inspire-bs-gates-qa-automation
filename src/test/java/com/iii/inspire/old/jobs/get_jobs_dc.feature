@jobs
Feature: Get Marc Extract Jobs
  Background:
  * def site_code = "rollup"
  * def customer_id = "ddaf8a6f-6ef8-40c4-ac88-f3373ca98bf2 "
  * def flow_id = "92b6c248-00ba-454d-b214-03d739568760"

  Scenario: Get keycloak token and then get all jobs
      Given url 'https://auth.sandbox-stargates.iii-inspire.dev/auth/realms/admin/protocol/openid-connect/token'
    And header 'Content-Type' = 'application/x-www-form-urlencoded'
    And form field grant_type = 'password'
    And form field username = 'admin'
    And form field password = 'admin'
    And form field client_id = 'convergence'
    When method post
    Then status 200
    * def access_token = response.access_token


    #  Given url 'https://ds-conv-bs-gates-polaris-pr-9.iii-conv.com:10000/api/gates-edge/gates/jobs'
     # And header Content-Type = 'application/json'
     # And header Authorization = 'Bearer ' + access_token
     # And header iii-customer-domain = "admin.ds-conv-bs-gates-polaris-pr-9.iii-conv.com"
  #  And header iii-customer-site-code = site_code
   # And header iii-customer-id = customer_id
  #  And header X-III-POLARIS-ORG-ID = 1,
     # And header Accept-Encoding =  '*',
    #  And header api-version = 1
       # When method get
    #  Then status 200



  #  Given url 'https://ds-conv-bs-gates-polaris-pr-9.iii-conv.com:10000/api/gates-edge/gates/jobs/flow-created/stop'
  #  And header Content-Type = 'application/json'
  #  And header Authorization = 'Bearer ' + access_token
  #  And header iii-customer-domain = "admin.ds-conv-bs-gates-polaris-pr-9.iii-conv.com"
  #  And header iii-customer-site-code = site_code
   # And header iii-customer-id = customer_id
  #  And header X-III-POLARIS-ORG-ID = 1,
  #  And header Accept-Encoding =  '*',
  #  And header api-version = 1
  #  And request {}
  #  When method post
  #  Then status 200



    #Given url 'https://ds-conv-bs-gates-polaris-pr-9.iii-conv.com:10000/api/gates-edge/gates/jobs/ec9ba460-bdc5-4ad9-a365-97434a78f072'
    #And header Content-Type = 'application/json'
    #And header Authorization = 'Bearer ' + access_token
    #And header iii-customer-domain = "admin.ds-conv-bs-gates-polaris-pr-9.iii-conv.com"
  #  And header iii-customer-site-code = site_code
   # And header iii-customer-id = customer_id
  #  And header X-III-POLARIS-ORG-ID = 1,
  #  And header Accept-Encoding =  '*',
  #  And header api-version = 1
  #  And request {}
    #When method delete
    #Then status 200

    #Given url 'https://ds-conv-bs-gates-polaris-pr-9.iii-conv.com:10000/api/gates-edge/gates/jobs/4ed7183e-59d0-4587-b650-64f79cd546c7'
    #And header Content-Type = 'application/json'
    #And header Authorization = 'Bearer ' + access_token
    #And header iii-customer-domain = "admin.ds-conv-bs-gates-polaris-pr-9.iii-conv.com"
  #  And header iii-customer-site-code = site_code
   # And header iii-customer-id = customer_id
  #  And header X-III-POLARIS-ORG-ID = 1,
  #  And header Accept-Encoding =  '*',
  #  And header api-version = 1
  #  And request {}
    #When method delete
    #Then status 200


    #Given url 'https://ds-conv-bs-gates-polaris-pr-9.iii-conv.com:10000/api/gates-edge/gates/jobs/polaris1560260295825-flow-updated'
    #And header Content-Type = 'application/json'
    #And header Authorization = 'Bearer ' + access_token
    #And header iii-customer-domain = "admin.ds-conv-bs-gates-polaris-pr-9.iii-conv.com"
  #  And header iii-customer-site-code = site_code
   # And header iii-customer-id = customer_id
  #  And header X-III-POLARIS-ORG-ID = 1,
  #  And header Accept-Encoding =  '*',
  #  And header api-version = 1
  #  And request {}
    #When method delete
    #Then status 200


    #Given url 'https://ds-conv-bs-gates-polaris-pr-9.iii-conv.com:10000/api/gates-edge/gates/jobs/polaris1560260974500-flow-created-updated'
    #And header Content-Type = 'application/json'
    #And header Authorization = 'Bearer ' + access_token
    #And header iii-customer-domain = "admin.ds-conv-bs-gates-polaris-pr-9.iii-conv.com"
  #  And header iii-customer-site-code = site_code
   # And header iii-customer-id = customer_id
  #  And header X-III-POLARIS-ORG-ID = 1,
  #  And header Accept-Encoding =  '*',
  #  And header api-version = 1
  #  And request {}
    #When method delete
    #Then status 200
    Given url 'https://sandbox-stargates.iii-inspire.dev/api/gates-edge/gates/jobs'
    And header Content-Type = 'application/json'
    And header Authorization = 'Bearer ' + access_token
    And header iii-customer-domain = "admin.sandbox-stargates.iii-inspire.dev"
  #  And header iii-customer-site-code = site_code
   # And header iii-customer-id = customer_id
  #  And header X-III-POLARIS-ORG-ID = 1,
     # And header Accept-Encoding =  '*',
    And header api-version = 1
    When method get
    Then status 200



    Given url 'https://sandbox-stargates.iii-inspire.dev/api/gates-edge/gates/jobs/' + flow_id +'/stop'
    And header Content-Type = 'application/json'
    And header Authorization = 'Bearer ' + access_token
    And header iii-customer-domain = "admin.sandbox-stargates.iii-inspire.dev"
    And header iii-customer-site-code = site_code
    And header iii-customer-id = customer_id
  #  And header X-III-POLARIS-ORG-ID = 1,
  #  And header Accept-Encoding =  '*',
    And header api-version = 1
    And request {}
    When method post
    Then status 200





    Given url 'https://sandbox-stargates.iii-inspire.dev/api/gates-edge/gates/jobs'
     And header Content-Type = 'application/json'
     And header Authorization = 'Bearer ' + access_token
     And header iii-customer-domain = "admin.sandbox-stargates.iii-inspire.dev"
  #  And header iii-customer-site-code = site_code
   # And header iii-customer-id = customer_id
  #  And header X-III-POLARIS-ORG-ID = 1,
     # And header Accept-Encoding =  '*',
      And header api-version = 1
     When method get
     Then status 200
