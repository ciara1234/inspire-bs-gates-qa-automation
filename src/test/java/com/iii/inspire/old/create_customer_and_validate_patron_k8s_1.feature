@Customer
Feature: Create customer on Customer API and assign connection and check that a patron barcode and pin can be validated
  Background:
    * def now = function(){ return java.lang.System.currentTimeMillis() }

    * def envUrl = "ds-conv-bs-gates-polaris-pr-15.iii-inspire.dev"
    * def site_code = "polaris" + now()
    * def cust_domain = "admin.ds-conv-bs-gates-polaris-pr-15.iii-inspire.dev"

    * def json = { url: '#(envUrl)'}
    * def kc = karate.call('../keycloak/keycloak_token.feature',json)
    * def access_token = kc.response.access_token
    #* def customer_id = ""

  Scenario: Create Customer with Connection and check locations

    Given url 'https://' + envUrl + '/api/customer/customers/'
    And header 'Content-Type' = 'application/json'
    And header Authorization = 'Bearer ' + access_token
    And header iii-customer-domain = cust_domain
    And header api-version = 1
    And request
  """
  {
  name : "Polaris Gates QA Test: Validate Patron",
  ilsSiteCode :'#(site_code)',
  types : [ {
  "type" : "LIBRARY" }]
  }
  """
    When method post
    Then status 201
    * def customer_id = response.id

    Given url 'https://' + envUrl + '/api/customer/dictionaries/license/names'
    And header 'Content-Type' = 'application/json'
    And header Authorization = 'Bearer ' + access_token
    And header iii-customer-domain = cust_domain
    And header api-version = 1
    When method get
    Then status 200

    * def discovery = response[1].id
    * def context_engine = response[0].id

    Given url 'https://' + envUrl + '/api/customer/customers/' + customer_id + '/licenses'
    And header 'Content-Type' = 'application/json'
    And header Authorization = 'Bearer ' + access_token
    And header iii-customer-domain = cust_domain
    And header api-version = 2
    And request
  """
  {
  licenseName :"Context Engine",
  status : "ACTIVE",
  expirationDate : "2020-10-05"
  }
  """
    When method post
    Then status 201
    * def contextLicense = response.licenseId

    Given url 'https://' + envUrl + '/api/customer/customers/' + customer_id + '/licenses'
    And header 'Content-Type' = 'application/json'
    And header Authorization = 'Bearer ' + access_token
    And header iii-customer-domain = cust_domain
    And header api-version = 2
    And request
  """
  {
  licenseName :"Discovery",
  status : "ACTIVE",
  expirationDate : "2020-10-05"
  }
  """
    When method post
    Then status 201
    * def disLicense = response.licenseId

    Given url 'https://' + envUrl + '/api/customer/customers/' + customer_id + '/connection'
    And header 'Content-Type' = 'application/json'
    And header api-version = 3
    And header Authorization = 'Bearer ' + access_token
    And header iii-customer-domain = cust_domain
    And request
  """
  {
      "type" : "polaris",
      "clientId" : "polarisdev",
      "clientSecret" : "4B3C1D34-7BA6-4D5E-8112-1C9A6BB7CD74",
      "apiUrl" : "https://rd-polaris.polarislibrary.com/PAPIService/REST",
      "staffUsername": "polarisexec",
      "staffPassword": "P0laris!@",
      "staffDomain": "iii"
  }
  """
    When method post
    Then status 201

    Given url 'https://' + envUrl + '/api/customer/customers/' + customer_id + '/connection?type=polaris'
    And header 'Content-Type' = 'application/json'
    And header api-version = 3
    And header Authorization = 'Bearer ' + access_token
    And header iii-customer-domain = cust_domain
    When method get
    Then status 200

    * def customer_id = response.customerId


#  Scenario: Patron Validate - good barcode, good pin

#good barcode good pin
    Given url 'https://' + envUrl + '/api/gates-edge/gates/patrons/validate'
    And header Content-Type = 'application/json'
    And header Authorization = 'Bearer ' + access_token
    And header iii-customer-domain = cust_domain
    And header iii-customer-site-code = site_code
    And header iii-customer-id = customer_id
    And header X-III-POLARIS-ORG-ID = 3
    And header Accept-Encoding =  '*'
    And header api-version = 1
    And request
    """
    {
          barcode:"20191234123412",
          pin:"1234"
    }
   """
    When method post
    Then status 200
    And match response ==
    """
    {
      "valid": true,
      "patronId": "20191234123412"
    }
    """

#good barcode no pin

    Given url 'https://' + envUrl + '/api/gates-edge/gates/patrons/validate'
    And header Content-Type = 'application/json'
    And header Authorization = 'Bearer ' + access_token
    And header iii-customer-domain = cust_domain
    And header iii-customer-site-code = site_code
    And header iii-customer-id = customer_id
    And header X-III-POLARIS-ORG-ID = 3
    And header Accept-Encoding =  '*'
    And header api-version = 1
    And request
    """
    {
          barcode:"20191234123412",
          pin:""
    }
   """
    When method post
    Then status 500
    And match response ==
    """
    {
       "path": "/patrons/validate",
        "error": "Internal Server Error",
        "message": "Password is invalid",
        "timestamp": "#string",
        "status": 500
    }
    """

    #good barcode bad pin


    Given url 'https://' + envUrl + '/api/gates-edge/gates/patrons/validate'
    And header Content-Type = 'application/json'
    And header Authorization = 'Bearer ' + access_token
    And header iii-customer-domain = cust_domain
    And header iii-customer-site-code = site_code
    And header iii-customer-id = customer_id
    And header X-III-POLARIS-ORG-ID = 3
    And header Accept-Encoding =  '*'
    And header api-version = 1
    And request
    """
    {
          barcode:"20191234123412",
          pin:"1234XXX"
    }
   """
    When method post
    Then status 500
    And match response ==
    """
    {
      "path": "/patrons/validate",
      "error": "Internal Server Error",
      "message": "Unable to authenticate the patron credentials.",
      "timestamp": "#string",
      "status": 500
    }
    """

    #bad barcode bad pin

    Given url 'https://' + envUrl + '/api/gates-edge/gates/patrons/validate'
    And header Content-Type = 'application/json'
    And header Authorization = 'Bearer ' + access_token
    And header iii-customer-domain = cust_domain
    And header iii-customer-site-code = site_code
    And header iii-customer-id = customer_id
    And header X-III-POLARIS-ORG-ID = 3
    And header Accept-Encoding =  '*'
    And header api-version = 1
    And request
    """
    {
          barcode:"20191234123412XXX",
          pin:"1234XXX"
    }
   """
    When method post
    Then status 500
    And match response ==
    """
  {
      "path": "/patrons/validate",
      "error": "Internal Server Error",
      "message": "Unable to authenticate the patron credentials.",
      "timestamp": "#string",
      "status": 500
  }
    """

    #bad barcode no pin


    Given url 'https://' + envUrl + '/api/gates-edge/gates/patrons/validate'
    And header Content-Type = 'application/json'
    And header Authorization = 'Bearer ' + access_token
    And header iii-customer-domain = cust_domain
    And header iii-customer-site-code = site_code
    And header iii-customer-id = customer_id
    And header X-III-POLARIS-ORG-ID = 3
    And header Accept-Encoding =  '*'
    And header api-version = 1
    And request
    """
    {
          barcode:"20191234123412XXX",
          pin:""
    }
   """
    When method post
    Then status 500
    And match response ==
    """
    {
       "path": "/patrons/validate",
        "error": "Internal Server Error",
        "message": "Password is invalid",
        "timestamp": "#string",
        "status": 500
    }
    """

    #no barcode no pin


    Given url 'https://' + envUrl + '/api/gates-edge/gates/patrons/validate'
    And header Content-Type = 'application/json'
    And header Authorization = 'Bearer ' + access_token
    And header iii-customer-domain = cust_domain
    And header iii-customer-site-code = site_code
    And header iii-customer-id = customer_id
    And header X-III-POLARIS-ORG-ID = 3
    And header Accept-Encoding =  '*'
    And header api-version = 1
    And request
    """
    {
          barcode:"",
          pin:""
    }
   """
    When method post
    Then status 500
    And match response ==
    """
    {
      "path": "/patrons/validate",
      "error": "Internal Server Error",
      "message": "Barcode is invalid, Password is invalid",
      "timestamp": "#string",
      "status": 500
    }
    """

    #no body


    Given url 'https://' + envUrl + '/api/gates-edge/gates/patrons/validate'
    And header Content-Type = 'application/json'
    And header Authorization = 'Bearer ' + access_token
    And header iii-customer-domain = cust_domain
    And header iii-customer-site-code = site_code
    And header iii-customer-id = customer_id
    And header X-III-POLARIS-ORG-ID = 3
    And header Accept-Encoding =  '*'
    And header api-version = 1
    And request
    """
    {
    }
   """
    When method post
    Then status 400
    And match response ==
    """
          {

          "path":"/patrons/validate",
          "error":"Bad Request",
          "message":"Validation failure",
          "timestamp":"#string",
          "status":400

          }
    """

    #no body at all

    Given url 'https://' + envUrl + '/api/gates-edge/gates/patrons/validate'
    And header Content-Type = 'application/json'
    And header Authorization = 'Bearer ' + access_token
    And header iii-customer-domain = cust_domain
    And header iii-customer-site-code = site_code
    And header iii-customer-id = customer_id
    And header X-III-POLARIS-ORG-ID = 3
    And header Accept-Encoding =  '*'
    And header api-version = 1
    And request ""
    When method post
    Then status 400
    And match response ==
    """
  {
      "path": "/patrons/validate",
      "error": "Bad Request",
      "message": "Request body is missing: public reactor.core.publisher.Mono<com.iii.inspire.gates.polaris.payload.patron.PatronValidateResponse> com.iii.inspire.gates.polaris.controller.PatronController.validatePatron(org.springframework.web.server.ServerWebExchange,com.iii.inspire.gates.polaris.payload.patron.PatronValidateRequestBody)",
      "timestamp": "#string",
      "status": 400
  }
    """

    Given url 'https://' + envUrl + '/api/customer/customers/' + customer_id + '/licenses/' + disLicense
    And header 'Content-Type' = 'application/json'
    And header Authorization = 'Bearer ' + access_token
    And header iii-customer-domain = cust_domain
    And header api-version = 2
    And request
  """
  {
  licenseName :"Discovery",
  status : "ARCHIVED",
  expirationDate : null
  }
  """
    When method put
    Then status 200



    Given url 'https://' + envUrl + '/api/customer/customers/' + customer_id + '/licenses/' + contextLicense
    And header 'Content-Type' = 'application/json'
    And header Authorization = 'Bearer ' + access_token
    And header iii-customer-domain = cust_domain
    And header api-version = 2
    And request
  """
  {
  licenseName :"Context Engine",
  status : "ARCHIVED",
  expirationDate : null
  }
  """
    When method put
    Then status 200

