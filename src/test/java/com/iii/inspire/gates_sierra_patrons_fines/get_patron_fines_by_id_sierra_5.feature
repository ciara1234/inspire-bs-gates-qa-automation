Feature: Create customer on Customer API and assign connection and check Patron

  Background:

    * def now = function(){ return java.lang.System.currentTimeMillis() }

    * def envUrl = "https://ds-conv-bs-gates-polaris-pr-24.iii-inspire.dev"
    * def ilsSiteCode = "gates" + now()
    * def ilsDescription = "Gates QA Test for Sierra: get patron by id"

    * def custDomain = "admin.ds-conv-bs-gates-polaris-pr-24.iii-inspire.dev"

    * def customerSiteDetails = { url: '#(envUrl)', ilsSiteCode: '#(ilsSiteCode)', custdomain: '#(custDomain)',description: '#(ilsDescription)' }
    * def customerInfo = callonce read('../customer/create_sierra_8985_customer.feature') customerSiteDetails

    * def envUrlAsJson = { url: '#(envUrl)'}
    * def keycloak = karate.call('../keycloak/keycloak_token.feature', envUrlAsJson)
    * def accessToken = keycloak.response.access_token

    * url envUrl

  Scenario: Get Patron by Id in Sierra - v1 Api

    * def patronFinesStructure =
     """
    {
       fineId: '#string',
       itemId: '##string',
       fineDate : '##regex [0-9]{4}[-]{1}[0-9]{2}[-]{1}[0-9]{2}[T]{1}[0-9]{2}[:]{1}[0-9]{2}[:]{1}[0-9]{2}[Z]{1}',
       finePaidDate : '##regex [0-9]{4}[-]{1}[0-9]{2}[-]{1}[0-9]{2}[T]{1}[0-9]{2}[:]{1}[0-9]{2}[:]{1}[0-9]{2}[Z]{1}',
       fineType : '##string',
       fineDescription : '##string',
       locationCode :  '#string',
       paidAmount : '#number',
       outstandingAmount" : '##number'
    }
    """
    Given path '/api/gates-edge/gates/patrons/2183607/fines'
    And header Content-Type = 'application/json'
    And header Authorization = 'Bearer ' + accessToken
    And header iii-customer-domain = custDomain
    And header iii-customer-site-code = ilsSiteCode
    And header iii-customer-id = customerInfo.customerData.id
    And header Accept-Encoding =  '*'
    And header api-version = 1
    When method get
    Then status 200
    And match each response.fines contains patronFinesStructure


  Scenario: After tests - Make customer Archived

    * def results = karate.call('../customer/make_licences_inactive.feature',customerInfo)
