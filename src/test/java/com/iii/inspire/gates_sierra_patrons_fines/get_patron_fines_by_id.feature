Feature: Create customer on Customer API and assign connection and check Patron

  Background:

    * def now = function(){ return java.lang.System.currentTimeMillis() }

    * url envUrl
    * def ilsSiteCode = "sierra" + now()
    * def ilsDescription = "Sierra Gates QA Test: get patron fines by id"

    * def customerSiteDetails = { url: '#(envUrl)', ilsSiteCode: '#(ilsSiteCode)', custdomain: '#(custDomain)',description: '#(ilsDescription)' }

    * def customerInfo = callonce read('../customer/create_sierra_6916_customer_k8s_1.feature') customerSiteDetails

    * def envUrlAsJson = { url: '#(envUrl)'}
    * def keycloak = karate.call('../keycloak/keycloak_token.feature', envUrlAsJson)
    * def accessToken = keycloak.response.access_token



  Scenario: Get Patron by Id in Sierra - no params added

    Given path '/api/gates-edge/gates/patrons/2183478/fines'
    And header Content-Type = 'application/json'
    And header Authorization = 'Bearer ' + accessToken
    And header iii-customer-domain = custDomain
    And header iii-customer-site-code = ilsSiteCode
    And header iii-customer-id = customerInfo.customerData.id
    And header Accept-Encoding =  '*'
    And header api-version = 1
    When method get
    Then status 200
    * def responseBody = response
    And assert responseBody.total == 7
    And assert responseBody.start == 0
    And match $.fines.length() == 7


  Scenario: Get Patron by fines by Id in Sierra - just limit 1

    Given path '/api/gates-edge/gates/patrons/2183478/fines'
    And header Content-Type = 'application/json'
    And header Authorization = 'Bearer ' + accessToken
    And header iii-customer-domain = custDomain
    And header iii-customer-site-code = ilsSiteCode
    And header iii-customer-id = customerInfo.customerData.id
    And header Accept-Encoding =  '*'
    And header api-version = 1
    And param limit = 1
    When method get
    Then status 200
    * def responseBody = response
    And assert responseBody.total == 7
    And assert responseBody.start == 0
    And match $.fines.length() == 1


  Scenario: Get Patron by fines by Id in Sierra - just limit 2

    Given path '/api/gates-edge/gates/patrons/2183478/fines'
    And header Content-Type = 'application/json'
    And header Authorization = 'Bearer ' + accessToken
    And header iii-customer-domain = custDomain
    And header iii-customer-site-code = ilsSiteCode
    And header iii-customer-id = customerInfo.customerData.id
    And header Accept-Encoding =  '*'
    And header api-version = 1
    And param limit = 2
    When method get
    Then status 200
    * def responseBody = response
    And assert responseBody.total == 7
    And assert responseBody.start == 0
    And match $.fines.length() == 2


  Scenario: Get Patron by fines by Id in Sierra - just offset

    Given path '/api/gates-edge/gates/patrons/2183478/fines'
    And header Content-Type = 'application/json'
    And header Authorization = 'Bearer ' + accessToken
    And header iii-customer-domain = custDomain
    And header iii-customer-site-code = ilsSiteCode
    And header iii-customer-id = customerInfo.customerData.id
    And header Accept-Encoding =  '*'
    And header api-version = 1
    And param limit = 1
    When method get
    Then status 200
    * def responseBody = response
    And assert responseBody.total == 7
    And assert responseBody.start == 0
    And match $.fines.length() == 1

  Scenario: Get Patron by fines by Id in Sierra - limit 1 offset 0

    Given path '/api/gates-edge/gates/patrons/2183478/fines'
    And header Content-Type = 'application/json'
    And header Authorization = 'Bearer ' + accessToken
    And header iii-customer-domain = custDomain
    And header iii-customer-site-code = ilsSiteCode
    And header iii-customer-id = customerInfo.customerData.id
    And header Accept-Encoding =  '*'
    And header api-version = 1
    And param limit = 1
    And param offset = 0
    When method get
    Then status 200
    * def responseBody = response
    And assert responseBody.total == 7
    And assert responseBody.start == 0
    And match $.fines.length() == 1


  Scenario: Get Patron by fines by Id in Sierra - limit 7 offset 0

    Given path '/api/gates-edge/gates/patrons/2183478/fines'
    And header Content-Type = 'application/json'
    And header Authorization = 'Bearer ' + accessToken
    And header iii-customer-domain = custDomain
    And header iii-customer-site-code = ilsSiteCode
    And header iii-customer-id = customerInfo.customerData.id
    And header Accept-Encoding =  '*'
    And header api-version = 1
    And param limit = 7
    And param offset = 0
    When method get
    Then status 200
    * def responseBody = response
    And assert responseBody.total == 7
    And assert responseBody.start == 0
    And match $.fines.length() == 7



  Scenario: Get Patron by fines by Id in Sierra - limit 6 offset 0

    Given path  '/api/gates-edge/gates/patrons/2183478/fines'
    And header Content-Type = 'application/json'
    And header Authorization = 'Bearer ' + accessToken
    And header iii-customer-domain = custDomain
    And header iii-customer-site-code = ilsSiteCode
    And header iii-customer-id = customerInfo.customerData.id
    And header Accept-Encoding =  '*'
    And header api-version = 1
    And param limit = 6
    And param offset = 0
    When method get
    Then status 200
    * def responseBody = response
    And assert responseBody.total == 7
    And assert responseBody.start == 0
    And match $.fines.length() == 6



  Scenario: Get Patron by fines by Id in Sierra - limit 7 offset 7

    Given path '/api/gates-edge/gates/patrons/2183478/fines'
    And header Content-Type = 'application/json'
    And header Authorization = 'Bearer ' + accessToken
    And header iii-customer-domain = custDomain
    And header iii-customer-site-code = ilsSiteCode
    And header iii-customer-id = customerInfo.customerData.id
    And header Accept-Encoding =  '*'
    And header api-version = 1
    And param limit = 7
    And param offset = 7
    When method get
    Then status 200
    * def responseBody = response
    And assert responseBody.total == 0
    And match $.fines.length() == 0


  Scenario: Get Patron by fines by Id in Sierra - limit 7 offset 1

    Given path '/api/gates-edge/gates/patrons/2183478/fines'
    And header Content-Type = 'application/json'
    And header Authorization = 'Bearer ' + accessToken
    And header iii-customer-domain = custDomain
    And header iii-customer-site-code = ilsSiteCode
    And header iii-customer-id = customerInfo.customerData.id
    And header Accept-Encoding =  '*'
    And header api-version = 1
    And param limit = 7
    And param offset = 1
    When method get
    Then status 200
    * def responseBody = response
    And assert responseBody.total == 7
    And assert responseBody.start == 1
    And match $.fines.length() == 6

  #Scenario: Clean up after tests - Make customer inactive

   # * def results = karate.call('../customer/make_licences_inactive.feature',customerInfo)
