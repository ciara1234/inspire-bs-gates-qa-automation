Feature: Create customer on Customer API and assign connection and check Patron

  Background:

    * def now = function(){ return java.lang.System.currentTimeMillis() }

    * url envUrl
    * def ilsSiteCode = "getthatbook"
    * def ilsDescription = "GatesTest"

    * def customerSiteDetails = { url: '#(envUrl)', ilsSiteCode: '#(ilsSiteCode)', custdomain: '#(custDomain)',description: '#(ilsDescription)' }

    * def customerInfo = callonce read('../customer/create_sierra_8985_customer.feature') customerSiteDetails

    * def envUrlAsJson = { url: '#(envUrl)'}
    * def keycloak = karate.call('../keycloak/keycloak_token.feature', envUrlAsJson)
    * def accessToken = keycloak.response.access_token

  Scenario: Get Patron by Id in Sierra - no params added

    Given path '/api/gates-edge/gates/patrons/2183478/fines'
    And header Content-Type = 'application/json'
    And header Authorization = 'Bearer ' + accessToken
    And header iii-customer-domain = custDomain
    And header iii-customer-site-code = ilsSiteCode
    And header iii-customer-id = customerInfo.customerData.id
    And header Accept-Encoding =  '*'
    And header api-version = 1
    When method get
    Then status 200

   #Scenario: Clean up after tests - Make customer inactive

   # * def results = karate.call('../customer/make_licences_inactive.feature',customerInfo)
