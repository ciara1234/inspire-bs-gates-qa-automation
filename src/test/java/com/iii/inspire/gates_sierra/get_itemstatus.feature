Feature: Create customer on Customer API and assign connection and check item status

  Background:

    * def now = function(){ return java.lang.System.currentTimeMillis() }

    * def envUrl = "https://prodlite.iii-conv.com"
    * def ilsSiteCode = "gatestest" + now()
    * def ilsDescription = "Sierra Gates QA Test: get itemstatus"

    * def custDomain = "admin.prodlite.iii-conv.com"

    * def customerSiteDetails = { url: '#(envUrl)', ilsSiteCode: '#(ilsSiteCode)', custdomain: '#(custDomain)',description: '#(ilsDescription)' }
    * def customerInfo = callonce read('../customer/create_sierra_jeffa_clone_customer_k8s_1.feature') customerSiteDetails

    * def envUrlAsJson = { url: '#(envUrl)'}
    * def keycloak = karate.call('../keycloak/keycloak_token.feature', envUrlAsJson)
    * def accessToken = keycloak.response.access_token

    * url envUrl

  Scenario: Get item status in Sierra

    Given path '/api/gates-edge/gates/itemstatus'
    And header Content-Type = 'application/json'
    And header Authorization = 'Bearer ' + accessToken
    And header iii-customer-domain = custDomain
    And header iii-customer-site-code = ilsSiteCode
    And header iii-customer-id = customerInfo.customerData.id
    And header Accept-Encoding =  '*'
    And header api-version = 1
    When method get
    Then status 200


  Scenario: Make customer inactive

    * def results = karate.call('../customer/make_licences_inactive.feature',customerInfo)
