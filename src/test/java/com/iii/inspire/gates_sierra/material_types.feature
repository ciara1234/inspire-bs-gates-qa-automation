Feature: material types

  Background:
    * def now = function(){ return java.lang.System.currentTimeMillis() }

    * def envUrl = "ds-conv-bs-gates-polaris-pr-17.iii-inspire.dev"
  #  * def envUrl = "cf-gates.iii-conv.com"
    * def ilsSiteCode = "archivemesierra" + now()
    * def description = "Gates check"

    * def custDomain = "admin.ds-conv-bs-gates-polaris-pr-17.iii-inspire.dev/ "
   # * def custDomain = "admin.ds-conv-bs-gates-sierra-pr-40.iii-inspire.dev"
    * def json = { url: '#(envUrl)', ilsSiteCode: '#(ilsSiteCode)', custdomain: '#(custDomain)', description: '#(description)' }

    * def res = callonce read('../customer/create_sierra_customer_k8s_1.feature') json

    * def json1 = { url: '#(envUrl)'}
    * def kc = karate.call('../keycloak/keycloak_token.feature',json1)
    * def access_token = kc.response.access_token

  Scenario: material types

    Given url 'https://' + envUrl + '/api/gates-edge/gates/materialtypes'
    And header Content-Type = 'application/json'
    And header Authorization = 'Bearer ' + access_token
    And header iii-customer-domain = custDomain
    And header iii-customer-site-code = ilsSiteCode
    And header iii-customer-id = res.customerData.id
    And header Accept-Encoding =  '*'
    And header api-version = 1
    When method get
    Then status 200


  Scenario: locations

    Given url 'https://' + envUrl + '/api/gates-edge/gates/locations'
    And header Content-Type = 'application/json'
    And header Authorization = 'Bearer ' + access_token
    And header iii-customer-domain = custDomain
    And header iii-customer-site-code = ilsSiteCode
    And header iii-customer-id = res.customerData.id
    And header Accept-Encoding =  '*'
    And header api-version = 2
    When method get
    Then status 200


  Scenario: material types tidy up
   * def results = karate.call('../customer/make_licences_inactive.feature',res)

