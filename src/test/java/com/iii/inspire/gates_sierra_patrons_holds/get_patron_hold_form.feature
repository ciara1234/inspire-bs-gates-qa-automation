Feature: Create customer on Customer API and assign connection and check Patron hold form for pickup location

  Background:

    * def now = function(){ return java.lang.System.currentTimeMillis() }

    * def envUrl = "https://cf-gates.iii-conv.com"
    * def ilsSiteCode = "patronfines" + now()
    * def ilsDescription = "Sierra Gates QA Test: get patron hold form"

    * def custDomain = "admin.cf-gates.iii-conv.com"

    * def customerSiteDetails = { url: '#(envUrl)', ilsSiteCode: '#(ilsSiteCode)', custdomain: '#(custDomain)',description: '#(ilsDescription)' }
    * def customerInfo = callonce read('../customer/create_sierra_6916_customer_k8s_1.feature') customerSiteDetails

    * def envUrlAsJson = { url: '#(envUrl)'}
    * def keycloak = karate.call('../keycloak/keycloak_token.feature', envUrlAsJson)
    * def accessToken = keycloak.response.access_token

    * url envUrl

  Scenario: Hold form - v1 - correct params - success

    Given path '/api/gates-edge/gates/patrons/2183478/holds/request-form'
    And header Content-Type = 'application/json'
    And header Authorization = 'Bearer ' + accessToken
    And header iii-customer-domain = custDomain
    And header iii-customer-site-code = ilsSiteCode
    And header iii-customer-id = customerInfo.customerData.id
    And header Accept-Encoding =  '*'
    And header api-version = 1
    And request {"recordNumber": 1204955,"recordType":"b"}
    When method post
    Then status 200
    And match response.pickupLocations[0].code == "ap"



  Scenario: Hold form - v2 - correct params - success

    Given path '/api/gates-edge/gates/patrons/2183478/holds/request-form'
    And header Content-Type = 'application/json'
    And header Authorization = 'Bearer ' + accessToken
    And header iii-customer-domain = custDomain
    And header iii-customer-site-code = ilsSiteCode
    And header iii-customer-id = customerInfo.customerData.id
    And header Accept-Encoding =  '*'
    And header api-version = 2
    And request {"recordNumber": 1204955,"recordType":"b"}
    When method post
    Then status 200
    And match response.pickupLocations[0].code == "ap"


  Scenario: Make customer inactive

    * def results = karate.call('../customer/make_licences_inactive.feature',customerInfo)
