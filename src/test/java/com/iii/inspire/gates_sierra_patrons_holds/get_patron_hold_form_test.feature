Feature: Create customer on Customer API and assign connection and check patron get hold form locations

  Background:

    * def now = function(){ return java.lang.System.currentTimeMillis() }

    * url envUrl

    * def ilsSiteCode = "getitgetit"
    #* def ilsSiteCode = "rabbit"
    #* def ilsDescription = "Ģ̸̡̝̖̮̰͓͉̩̭̫͚̜̜͍̙̱̠̪͚̞̟͎̘̱̫̥͚̬̝͇̝̠̜̹͇̻̫̺̫̪̫̙̟̯̼̞̻̤̩̰̾̎͌̈́̆̀͒̀̆͊̀́̒̏̇̎̌̽̊͗͜͜͜͜͜͠ͅͅͅͅȀ̴̢̧̢̡̨̨̡̧̢̛̗̻̤̳̳̱͔̦̗̱̳͎̪͔̱̱̮͖̬̭̫͉̮̻̳̱̼̻̤̪̝̺̞̩̤̹̞͉͓̗͓̯̻̙̬̠̟͈̑̽̅̐̍̊̑̌͋͆̈́̍͐̀̽̃̀̈́̉̔̌̂̇̎̈́̀͐̓̀̉̔̊̽͂́̽̇̐̂̂́͆̀̇̒͗͛͗̏͛̊͗̀͒͐̽̈̅̈́͌͘̕̚̚͜T̸̡̛̛̛̛̮͈͕̬̝͍͓͙͎̭̬̻̹̼̜͓̲̗̳̱͎̘̟̯̞̭̰̯̰̯͎̭̄̓̅̆͗͂̃̓͌̽͒̓̌͛͋͋̇̾̂̎̈́͗͂́͆̋̆͌̅͆̂͒̽̄̿́͋̿̏͆̿͛̐͊̓̍̄̑́͋͂͊̂͛͒̚̚̚̚͜͝͠͝͝͝͝͠E̴̢͎̯̬͇̼͓̲̤̠̝̮̙͙̮̫̠͕̬̞͔̞̙̝̗̥̞͈̩͙̩̠̹̮͚̟̯̫̟̥͉͛̇̌͐̎͑̂͐̇͋́́̌̏͆͆͊̋̈͂͊̈̈́̂̀̈́̋͌́̆̈́̐̄̅̌͊̊̃͌̌̆͊͒̂̐̚͘̚͝͝͝S̷̨̡̢̢̛̛̺̮̱͖͙̪̝͓͙̩̪͔̰͈̭̖̥̯̬̫̪̤͂͂̏͌̆̽̄͛́̃͆̈́̑͆̏́̌͛̋͐̑̆͛̕͜͜͜͝ ̶̡̡͈͙̲̥̲̦̹̗̳͖̯̼̟̣̝̞͕̙͔̥͈̱̮̟̝̖̲̮͔̼̠̥̲͂͂̾͋̏́̑́̌̓̏̒͛͐̂̋̓͛͐͐̏͋̕͘̚͜͝͝͠͠ͅT̸̡̧̧̛̛̻͔̺̯̟̭͉̠̙̞̘̦͓̼̤͔͓̼̟̱͍̭̳̲̙͚̠͍͎̳̺̩͍̯̼̮͚͎̯͕̹̻̼̞̳̦͙̠̭̗͐͂̄̑̈͌̇̌̈̏̇͋͛͋̊͛̓̀͌͒̇̋̇̓̉͆̆̆͑̐̿̾͒͋̓̊̅́̈́͑̉͊͊̐͋͋̓̉̅̏̚͜͜͝͝ͅḜ̸̢̡̰̫̗͉͕̹̬͉̼̖͈̞͔̞͙̪̬̱̹̻̙̫̳̯̣̼̱̮̬͎̩̣̣̹͖̬͕̩͕̳͇̥̺̌̍̒͋̆̚͠ͅS̵̨̱̫̘̘̳̥̰̟̹̜̫̰̰̞̆̀͑͌̑́̃͐͗́͂̅͐̌͐̃̈́͒͆̓͒̒̓͆͑̈͂̒͋̆̊̈͌̐̆̊̀́̾̀́̓̒̎̒͘Ţ̷̛̛̪̰̦͓̘̠̗̣̣͚͉̙̲̥̪͚͍͉̮̝̫̗̟̰̰͚̟̤̭͔̰̲̝̱͙̬̮̱͍̖͋́̐͒͐͌͗̆̑̂̽͋̀̿̈́͑́̓̈́̊̌̓̿̅͂͌̽̃̇̍͋̃͌̀́͑̃͗́̽͊̌̍̾̎̿̈́͛̀̅̐͘̚͘̚̕͠ ̷̧̨̢̱̭̟͈͕̤̯̠͙͗̿́̾͛̈́͆͗͑͗͋̆̍͌̑͐̑͊̆̃̂̈̌͊̂̂̌́͑͆̋̿͑́͆̈͐̆̿̋̈́̚̚̕͠͝͝Ç̴̢̡̛̛̻̥̹̜͙͕͙̼̩͈͈̙͎̮̺̥̗͎̻̳̤̭̮̲̤̫̯̫̈́͗̄͛́͗͋͂̈́̄̍̂̌̏̆͛̃́͒͊́̃̓͑̆͂̃̌̇̈͂́̔̅̈̍̈́́̽͐̑̑̇̃̓͛͛͐͛͊̄̀̅̏̐͒̕̕͘͜͝͝͠ͅƯ̴̢̧̨̨̥̭̘͉͇̬̘̻̝͓͈̟͖̺̜͓̖͎̣̱̟̫͕̦̟͙̫̳̫̆͗͊͋͐̃͂̍́̊͆̈́̋̃̒͋̈́̈́͘͠͝ͅŞ̶̧̨̟͕̪͈͈̗̣̤̖̦̮̩̟͈̰̤̞̪̠̦̙͇̙̼̝̯̬͕̰̼̣͖̻̱̜͕͍̹͚͖͓͎̺̭̳̾͐͒͐̓̓͜͝ͅͅT̷̢̨̲͉͇̜͎̺̰͓̮̜̺̘̭̥͖̱̦̼̭̻̥̩͔͇̗͚̻̗̣̹̙̪̳̖͔̳̟̃̓͆̿̾̉͂͊̾̅̊̈́͊̎̇͌̎̐̔͗̀̀̿́̇̿̓́̌̈́̍̔̀̑̀͐̅̐̕͠͝͝͝͝ͅǪ̵̢̛̥͓̲̬̙̺͍̭͔͚̬̓͐̌͗̉̂̊̔͋̉̃̕͘M̴̧̢̨̧̨̨̛̛̛̛̬̮͔͇͚͕̮̲͈͙̖̝̪͖̼͇̼̭͕̞̤̰͉̝͓̪̹̼̫͕͓͎̪̳̣͙̯̭̪̮̫̜̰̺̜̤̼̹̲̮̳̤͕̩̱̘̯̭̓̀̇́̔̏̀̒̊̆̑̋͌̉̔̔̐̔̌́̇̑͆͂̄̈̽̾͊̂͛̒̌͂̃́̍̋͐̉̇̈́̈́͌͑̈̅͌̎̿̕͘̕͜͜͜͠͝͠͝ͅͅE̶̫͙̝͇̜̞̭͈̺̪̠̙͉̝͋̚R̷̨̛̘͇̙̥̞̰̯̲̦̠͈͙̩͔̘̞̗̰̫̈́͒͋̅̑̐͑́̊͌̔̾̊̒̀͋́̌̽͒̓̈́̓͆̂͋̀́̄̏̊̇͘͝͝"

    * def ilsDescription = "get it test"

    * def customerSiteDetails = { url: '#(envUrl)', ilsSiteCode: '#(ilsSiteCode)', custdomain: '#(custDomain)',description: '#(ilsDescription)' }
    * def customerInfo = callonce read('../customer/create_sierra_8225_customer.feature') customerSiteDetails

    * def envUrlAsJson = { url: '#(envUrl)'}
    * def keycloak = karate.call('../keycloak/keycloak_token.feature', envUrlAsJson)
    * def accessToken = keycloak.response.access_token



  Scenario: 01 Get Patron hold form - no available locations

    Given path '/api/gates-edge/gates/patrons/1011764/holds/request-form'
    And header Content-Type = 'application/json'
    And header Authorization = 'Bearer ' + accessToken
    And header iii-customer-domain = custDomain
    And header iii-customer-site-code = ilsSiteCode
    And header iii-customer-id = customerInfo.customerData.id
    And header Accept-Encoding =  '*'
    And header api-version = 1
    And request {"recordNumber": 1005048,"recordType":"b"}
    When method post
    Then status 500
    And match response.message == "Sorry, your request cannot be placed. Please contact your library."


  #Scenario: After tests - Make customer Archived

   #* def results = karate.call('../customer/make_licences_inactive.feature',customerInfo)
