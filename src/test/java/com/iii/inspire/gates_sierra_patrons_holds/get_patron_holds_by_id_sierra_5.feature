Feature: Create customer on Customer API and assign connection and check Patron holds on Sierra 5.0

  Background:
#2183607
    * def now = function(){ return java.lang.System.currentTimeMillis() }

    * url envUrl

    * def ilsSiteCode = "patronholds" + now()
    * def ilsDescription = "Gates QA Test: get patron holds by id - canbefrozen"

    * def customerSiteDetails = { url: '#(envUrl)', ilsSiteCode: '#(ilsSiteCode)', custdomain: '#(custDomain)',description: '#(ilsDescription)' }
    * def customerInfo = callonce read('../customer/create_sierra_8985_customer.feature') customerSiteDetails

    * def envUrlAsJson = { url: '#(envUrl)'}
    * def keycloak = karate.call('../keycloak/keycloak_token.feature', envUrlAsJson)
    * def accessToken = keycloak.response.access_token




  Scenario: Just get the patron

    * def patronStructure =
     """
    {
        id: '#number',
        barcodes: '##array',
        patronName: '##array',
        emailAddress: '##array',
        birthDate: '##regex [0-9]{4}[-]{1}[0-9]{2}[-]{1}[0-9]{2}',
        homeLibraryCode: '##string',
        messages:'##array',
        blocks: '##array',
        address: '##array',
        phoneNumber: '##array',
        expirationDate: '#regex [0-9]{4}[-]{1}[0-9]{2}[-]{1}[0-9]{2}',
        moneyOwed: '#number',
        credit: '##number',
        deposit: '##number'
    }
    """

    Given path '/api/gates-edge/gates/patrons/2183772'
    And header Content-Type = 'application/json'
    And header Authorization = 'Bearer ' + accessToken
    And header iii-customer-domain = custDomain
    And header iii-customer-site-code = ilsSiteCode
    And header iii-customer-id = customerInfo.customerData.id
    And header Accept-Encoding =  '*'
    And header api-version = 1
    When method get
    Then status 200
    And match response contains patronStructure

  Scenario: Get Patron holds by patron Id in Sierra - no limits or offsets

    * def patronHoldsStructure =
     """
    {

       holdRequestId : '#string',
       recordId : '#string',
       recordType : '#string',
       activationDate : '##string',
       neededBy : '##string',
       location : '#string',
       expirationDate :'##string',
       status : '#string',
       frozen : '#boolean',
       canBeFrozen : '#boolean',
       priority : '#number',
       priorityQueueLength: '#number',
       note: '##string'

       }
    """

    Given path '/api/gates-edge/gates/patrons/2183772/holds'
    And header Content-Type = 'application/json'
    And header Authorization = 'Bearer ' + accessToken
    And header iii-customer-domain = custDomain
    And header iii-customer-site-code = ilsSiteCode
    And header iii-customer-id = customerInfo.customerData.id
    And header Accept-Encoding =  '*'
    And header api-version = 1
    When method get
    Then status 200
   # And match each response.holds contains patronHoldsStructure
   # * def responseBody = response
   # And assert responseBody.total == 2
   # And assert responseBody.start == 0



  Scenario: Make customer inactive

    * def results = karate.call('../customer/make_licences_inactive.feature',customerInfo)
