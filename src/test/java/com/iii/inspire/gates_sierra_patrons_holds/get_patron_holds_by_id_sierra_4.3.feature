Feature: Create customer on Customer API and assign connection and check Patron

  Background:

    * def now = function(){ return java.lang.System.currentTimeMillis() }

    * url envUrl

    * def ilsSiteCode = "patronholds" + now()
    * def ilsDescription = "Sierra Gates QA Test: get patron hold form"

    * def customerSiteDetails = { url: '#(envUrl)', ilsSiteCode: '#(ilsSiteCode)', custdomain: '#(custDomain)',description: '#(ilsDescription)' }
    * def customerInfo = callonce read('../customer/create_sierra_customer_k8s_1.feature') customerSiteDetails

    * def envUrlAsJson = { url: '#(envUrl)'}
    * def keycloak = karate.call('../keycloak/keycloak_token.feature', envUrlAsJson)
    * def accessToken = keycloak.response.access_token





  Scenario: 01 Get Patron holds by patron Id in Sierra - no limits or offsets

    * def patronHoldsStructure =
     """
    {

       holdRequestId : '#string',
       recordId : '#string',
       recordType : '#string',
       activationDate : '##string',
       neededBy : '##string',
       location : '#string',
       expirationDate :'##string',
       status : '#string',
       frozen : '#boolean',
       priority : '#number',
       priorityQueueLength: '#number',
       note: '##string'

       }
    """

    Given path '/api/gates-edge/gates/patrons/2183478/holds'
    And header Content-Type = 'application/json'
    And header Authorization = 'Bearer ' + accessToken
    And header iii-customer-domain = custDomain
    And header iii-customer-site-code = ilsSiteCode
    And header iii-customer-id = customerInfo.customerData.id
    And header Accept-Encoding =  '*'
    And header api-version = 1
    When method get
    Then status 200
    And match each response.holds contains patronHoldsStructure
    * def responseBody = response
    And assert responseBody.total == 2
    And assert responseBody.start == 0

  Scenario: 02 Get Patron holds by Id in Sierra - limit 2 offset 0

    * def patronHoldsStructure =
     """
    {

       holdRequestId : '#string',
       recordId : '#string',
       recordType : '#string',
       activationDate : '##string',
       neededBy : '##string',
       location : '#string',
       expirationDate :'##string',
       status : '#string',
       frozen : '#boolean',
       priority : '#number',
       priorityQueueLength: '#number',
       note: '##string'

       }
    """

    Given path '/api/gates-edge/gates/patrons/2183478/holds'
    And header Content-Type = 'application/json'
    And header Authorization = 'Bearer ' + accessToken
    And header iii-customer-domain = custDomain
    And header iii-customer-site-code = ilsSiteCode
    And header iii-customer-id = customerInfo.customerData.id
    And header Accept-Encoding =  '*'
    And header api-version = 1
    And param limit = 2
    And param offset = 0
    When method get
    Then status 200
    And match each response.holds contains patronHoldsStructure
    * def responseBody = response
    And assert responseBody.total == 2
    And assert responseBody.start == 0


  Scenario: 03 Get Patron holds by Id in Sierra - limit 2 no offset

    * def patronHoldsStructure =
     """
    {

       holdRequestId : '#string',
       recordId : '#string',
       recordType : '#string',
       activationDate : '##string',
       neededBy : '##string',
       location : '#string',
       expirationDate :'##string',
       status : '#string',
       frozen : '#boolean',
       priority : '#number',
       priorityQueueLength: '#number',
       note: '##string'

       }
    """
    Given path '/api/gates-edge/gates/patrons/2183478/holds'
    And header Content-Type = 'application/json'
    And header Authorization = 'Bearer ' + accessToken
    And header iii-customer-domain = custDomain
    And header iii-customer-site-code = ilsSiteCode
    And header iii-customer-id = customerInfo.customerData.id
    And header Accept-Encoding =  '*'
    And header api-version = 1
    And param limit = 2
    When method get
    Then status 200
    And match each response.holds contains patronHoldsStructure
    * def responseBody = response
    And assert responseBody.total == 2
    And assert responseBody.start == 0



  Scenario: 04 Get Patron holds by Id in Sierra - with limit 1 and offset 0

    * def patronHoldsStructure =
      """
    {

       holdRequestId : '#string',
       recordId : '#string',
       recordType : '#string',
       activationDate : '##string',
       neededBy : '##string',
       location : '#string',
       expirationDate :'##string',
       status : '#string',
       frozen : '#boolean',
       priority : '#number',
       priorityQueueLength: '#number',
       note: '##string'

       }
    """

    Given path '/api/gates-edge/gates/patrons/2183478/holds'
    And header Content-Type = 'application/json'
    And header Authorization = 'Bearer ' + accessToken
    And header iii-customer-domain = custDomain
    And header iii-customer-site-code = ilsSiteCode
    And header iii-customer-id = customerInfo.customerData.id
    And header Accept-Encoding =  '*'
    And header api-version = 1
    And param limit = 1
    And param offset = 0
    When method get
    Then status 200
    And match each response.holds contains patronHoldsStructure
    * def responseBody = response
    And assert responseBody.total == 2
    And assert responseBody.start == 0


  Scenario: 05 Get Patron holds by Id in Sierra - with limit 1 and offset 0 when patron has no holds

    Given path '/api/gates-edge/gates/patrons/2183000/holds'
    And header Content-Type = 'application/json'
    And header Authorization = 'Bearer ' + accessToken
    And header iii-customer-domain = custDomain
    And header iii-customer-site-code = ilsSiteCode
    And header iii-customer-id = customerInfo.customerData.id
    And header Accept-Encoding =  '*'
    And header api-version = 1
    And param limit = 1
    And param offset = 0
    When method get
    Then status 200
    * def responseBody = response
    And assert responseBody.total == 0
    And assert responseBody.start == 0


  Scenario: 06 Get Patron holds by Id in Sierra - with limit and offset when patron has no holds

    Given path '/api/gates-edge/gates/patrons/2183000/holds'
    And header Content-Type = 'application/json'
    And header Authorization = 'Bearer ' + accessToken
    And header iii-customer-domain = custDomain
    And header iii-customer-site-code = ilsSiteCode
    And header iii-customer-id = customerInfo.customerData.id
    And header Accept-Encoding =  '*'
    And header api-version = 1
    And param limit = 0
    And param offset = 0
    When method get
    Then status 400
    * def responseBody = response
    And assert responseBody.message == "Limit out of bounds!"



  Scenario: Make customer inactive

    * def results = karate.call('../customer/make_licences_inactive.feature',customerInfo)
