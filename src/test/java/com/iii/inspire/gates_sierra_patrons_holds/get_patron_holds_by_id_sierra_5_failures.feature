Feature: Create customer on Customer API and assign connection and check Patron holds - failure scenarios

  Background:

    * def now = function(){ return java.lang.System.currentTimeMillis() }

    * url envUrl
    * def ilsSiteCode = "patronholds" + now()
    * def ilsDescription = "Sierra Gates QA Test: get patron holds by id - canbefrozen"

    * def customerSiteDetails = { url: '#(envUrl)', ilsSiteCode: '#(ilsSiteCode)', custdomain: '#(custDomain)',description: '#(ilsDescription)' }
    * def customerInfo = callonce read('../customer/create_sierra_8985_customer.feature') customerSiteDetails

    * def envUrlAsJson = { url: '#(envUrl)'}
    * def keycloak = karate.call('../keycloak/keycloak_token.feature', envUrlAsJson)
    * def accessToken = keycloak.response.access_token



    Scenario: 01 Get holds by id in Sierra - invalid patron id

      Given path '/api/gates-edge/gates/patrons/2183772555555'
      And header Content-Type = 'application/json'
      And header Authorization = 'Bearer ' + accessToken
      And header iii-customer-domain = custDomain
      And header iii-customer-site-code = ilsSiteCode
      And header iii-customer-id = customerInfo.customerData.id
      And header Accept-Encoding =  '*'
      And header api-version = 1
      When method get
      Then status 500
      * def responseBody = response
      And assert responseBody.message == "Backend service response error."


  Scenario: 02 Get holds by id in Sierra : fake site code, bad format for customer id

    Given path '/api/gates-edge/gates/patrons/2183607/holds'
    And header Content-Type = 'application/json'
    And header Authorization = 'Bearer ' + accessToken
    And header iii-customer-domain = custDomain
    And header iii-customer-site-code = 'fakey'
    And header iii-customer-id = 'fakefake'
    And header Accept-Encoding =  '*'
    And header api-version = 1
    When method get
    Then status 400
    * def responseBody = response
    And assert responseBody.message == "Invalid Customer ID format: fakefake"

  Scenario: 03 Get holds by id in Sierra : good id format, not a customer


    Given path '/api/gates-edge/gates/patrons/2183607/holds'
    And header Content-Type = 'application/json'
    And header Authorization = 'Bearer ' + accessToken
    And header iii-customer-domain = custDomain
    And header iii-customer-site-code = 'fakey'
    And header iii-customer-id = '6343a48a-dead-c0de-d00d-0fbccb7a3b2f'
    And header Accept-Encoding =  '*'
    And header api-version = 1
    When method get
    Then status 400
    * def responseBody = response
    And assert responseBody.message == "Customer '6343a48a-dead-c0de-d00d-0fbccb7a3b2f' doesn't exist"

  Scenario: 04 Get holds by id in Sierra : good customer,no api header

    Given path '/api/gates-edge/gates/patrons/2183607/holds'
    And header Content-Type = 'application/json'
    And header Authorization = 'Bearer ' + accessToken
    And header iii-customer-domain = custDomain
    And header iii-customer-site-code = ilsSiteCode
    And header iii-customer-id = customerInfo.customerData.id
    And header Accept-Encoding =  '*'
    #And header api-version = 1
    When method get
    Then status 404
    * def responseBody = response
    And assert responseBody.error == "Not Found"

  Scenario: 05 Get holds by id in Sierra : good customer, bad api header

    Given path '/api/gates-edge/gates/patrons/2183607/holds'
    And header Content-Type = 'application/json'
    And header Authorization = 'Bearer ' + accessToken
    And header iii-customer-domain = custDomain
    And header iii-customer-site-code = ilsSiteCode
    And header iii-customer-id = customerInfo.customerData.id
    And header Accept-Encoding =  '*'
    And header api-version = 12
    When method get
    Then status 404
    * def responseBody = response
    And assert responseBody.error == "Not Found"

  Scenario: Make customer inactive

    * def results = karate.call('../customer/make_licences_inactive.feature',customerInfo)
