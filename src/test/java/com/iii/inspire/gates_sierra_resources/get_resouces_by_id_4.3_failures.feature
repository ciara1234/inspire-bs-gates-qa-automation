Feature: Create customer on Customer API and assign connection and check resource

  Background:

    * def now = function(){ return java.lang.System.currentTimeMillis() }
    * url envUrl

   # * def envUrl = "https://ds-conv-bs-gates-sierra-pr-52.iii-inspire.dev"
    * def ilsSiteCode = "sierracust" + now()
    * def ilsDescription = "Sierra Gates QA Test: get patron fines by id"
    #* def custDomain = "admin.ds-conv-bs-gates-sierra-pr-52.iii-inspire.dev"

    * def customerSiteDetails = { url: '#(envUrl)', ilsSiteCode: '#(ilsSiteCode)', custdomain: '#(custDomain)',description: '#(ilsDescription)' }

    * def customerInfo = callonce read('../customer/create_sierra_customer_k8s_1.feature') customerSiteDetails

    * def envUrlAsJson = { url: '#(envUrl)'}
    * def keycloak = karate.call('../keycloak/keycloak_token.feature', envUrlAsJson)
    * def accessToken = keycloak.response.access_token


  Scenario: 01 Get resource by Id in Sierra: bib doesn't exist

    Given path '/api/gates-edge/gates/resource/12345'
    And header Content-Type = 'application/json'
    And header Authorization = 'Bearer ' + accessToken
    And header iii-customer-domain = custDomain
    And header iii-customer-site-code = ilsSiteCode
    And header iii-customer-id = customerInfo.customerData.id
    And header Accept-Encoding =  '*'
    And header api-version = 1
    When method get
    Then status 404
    * def responseBody = response
    And assert responseBody.message == "Requested resource was not found."


  Scenario: 02 Get resource by Id in Sierra: invalid characters - %00

    Given path '/api/gates-edge/gates/resource/%00'
    And header Content-Type = 'application/json'
    And header Authorization = 'Bearer ' + accessToken
    And header iii-customer-domain = custDomain
    And header iii-customer-site-code = ilsSiteCode
    And header iii-customer-id = customerInfo.customerData.id
    And header Accept-Encoding =  '*'
    And header api-version = 1
    When method get
    Then status 403
    * def responseBody = response
    And assert responseBody.message == "The request was rejected because the URL contained a potentially malicious String \"%25\""


  Scenario: 03 Get resource by Id in Sierra: invalid characters 1001300A

    Given path '/api/gates-edge/gates/resource/1001300A'
    And header Content-Type = 'application/json'
    And header Authorization = 'Bearer ' + accessToken
    And header iii-customer-domain = custDomain
    And header iii-customer-site-code = ilsSiteCode
    And header iii-customer-id = customerInfo.customerData.id
    And header Accept-Encoding =  '*'
    And header api-version = 1
    When method get
    Then status 400
    * def responseBody = response
    And assert responseBody.error == "Bad Request"

  Scenario: 04 Get resource by Id in Sierra: invalid number 2345834736252

    Given path '/api/gates-edge/gates/resource/2345834736252'
    And header Content-Type = 'application/json'
    And header Authorization = 'Bearer ' + accessToken
    And header iii-customer-domain = custDomain
    And header iii-customer-site-code = ilsSiteCode
    And header iii-customer-id = customerInfo.customerData.id
    And header Accept-Encoding =  '*'
    And header api-version = 1
    When method get
    Then status 500
    * def responseBody = response
    And assert responseBody.message == "Backend service response error."

  Scenario: 05 Get resource by Id in Sierra: invalid number 0

    Given path '/api/gates-edge/gates/resource/0'
    And header Content-Type = 'application/json'
    And header Authorization = 'Bearer ' + accessToken
    And header iii-customer-domain = custDomain
    And header iii-customer-site-code = ilsSiteCode
    And header iii-customer-id = customerInfo.customerData.id
    And header Accept-Encoding =  '*'
    And header api-version = 1
    When method get
    Then status 404
    * def responseBody = response
    And assert responseBody.message == "Requested resource was not found."

  Scenario: 06 Get resource by Id in Sierra : fake site code, bad format for customer id

    Given path '/api/gates-edge/gates/resource/1017409'
    And header Content-Type = 'application/json'
    And header Authorization = 'Bearer ' + accessToken
    And header iii-customer-domain = custDomain
    And header iii-customer-site-code = 'fakey'
    And header iii-customer-id = 'fakefake'
    And header Accept-Encoding =  '*'
    And header api-version = 1
    When method get
    Then status 400
    * def responseBody = response
    And assert responseBody.message == "Invalid Customer ID format: fakefake"

  Scenario: 07 Get resource by Id in Sierra: good customer id format, not a customer

    Given path '/api/gates-edge/gates/resource/1017409'
    And header Content-Type = 'application/json'
    And header Authorization = 'Bearer ' + accessToken
    And header iii-customer-domain = custDomain
    And header iii-customer-site-code = 'fakey'
    And header iii-customer-id = '6343a48a-dead-c0de-d00d-0fbccb7a3b2f'
    And header Accept-Encoding =  '*'
    And header api-version = 1
    When method get
    Then status 400
    * def responseBody = response
    And assert responseBody.message == "Customer '6343a48a-dead-c0de-d00d-0fbccb7a3b2f' doesn't exist"

  Scenario: 08 Get resource by Id in Sierra : good customer,no api header

    Given path '/api/gates-edge/gates/resource/1017409'
    And header Content-Type = 'application/json'
    And header Authorization = 'Bearer ' + accessToken
    And header iii-customer-domain = custDomain
    And header iii-customer-site-code = ilsSiteCode
    And header iii-customer-id = customerInfo.customerData.id
    And header Accept-Encoding =  '*'
   # And header api-version = 1
    When method get
    Then status 404
    * def responseBody = response
    And assert responseBody.error == "Not Found"

  Scenario: 09 Get resource by Id in Sierra: good customer, bad api header

    Given path '/api/gates-edge/gates/resource/1017409'
    And header Content-Type = 'application/json'
    And header Authorization = 'Bearer ' + accessToken
    And header iii-customer-domain = custDomain
    And header iii-customer-site-code = ilsSiteCode
    And header iii-customer-id = customerInfo.customerData.id
    And header Accept-Encoding =  '*'
    And header api-version = 27
    When method get
    Then status 404
    * def responseBody = response
      And assert responseBody.error == "Not Found"

  Scenario: Clean up after tests - Make customer inactive

    * def results = karate.call('../customer/make_licences_inactive.feature',customerInfo)
