Feature: Create customer on Customer API and assign connection and check resource

  Background:

    * def now = function(){ return java.lang.System.currentTimeMillis() }
    * url envUrl

   # * def envUrl = "https://ds-conv-bs-gates-sierra-pr-52.iii-inspire.dev"
    * def ilsSiteCode = "sierracust" + now()
    * def ilsDescription = "Gates QA Test Sierra: get resources by id"
    #* def custDomain = "admin.ds-conv-bs-gates-sierra-pr-52.iii-inspire.dev"

    * def customerSiteDetails = { url: '#(envUrl)', ilsSiteCode: '#(ilsSiteCode)', custdomain: '#(custDomain)',description: '#(ilsDescription)' }

    * def customerInfo = callonce read('../customer/create_sierra_8985_customer.feature') customerSiteDetails

    * def envUrlAsJson = { url: '#(envUrl)'}
    * def keycloak = karate.call('../keycloak/keycloak_token.feature', envUrlAsJson)
    * def accessToken = keycloak.response.access_token


  Scenario: 01 Get Resource by Id in Sierra when bib is not suppressed and not deleted

    * def resourceStructure =
     """
    {
       itemId : '#string',
       locationCode : '#string',
       callNumber : '##string',
       status : {
          description : '##string',
          duedate : '##regex [0-9]{4}[-]{1}[0-9]{2}[-]{1}[0-9]{2}[T]{1}[0-9]{2}[:]{1}[0-9]{2}[:]{1}[0-9]{2}[Z]{1}'
        }
    }
    """

    Given path '/api/gates-edge/gates/resource/1000094'
    And header Content-Type = 'application/json'
    And header Authorization = 'Bearer ' + accessToken
    And header iii-customer-domain = custDomain
    And header iii-customer-site-code = ilsSiteCode
    And header iii-customer-id = customerInfo.customerData.id
    And header Accept-Encoding =  '*'
    And header api-version = 1
    When method get
    Then status 200
    And match each response.entries contains resourceStructure
    * def responseBody = response
    And assert responseBody.total == 2
    And assert responseBody.copies == 2
    And assert responseBody.holds == 0

    #1001674

  Scenario: 02 Get Resource by Id in Sierra when the bib is deleted

    Given path '/api/gates-edge/gates/resource/1001674'
    And header Content-Type = 'application/json'
    And header Authorization = 'Bearer ' + accessToken
    And header iii-customer-domain = custDomain
    And header iii-customer-site-code = ilsSiteCode
    And header iii-customer-id = customerInfo.customerData.id
    And header Accept-Encoding =  '*'
    And header api-version = 1
    When method get
    Then status 404
    And match response.message == "Requested resource was not found."

    #1017409

  Scenario: 03 Get Resource by Id in Sierra when the bib is suppressed

    * def resourceStructure =
     """
    {
       itemId : '#string',
       locationCode : '#string',
       callNumber : '##string',
       status : {
          description : '##string',
          duedate" : '##regex [0-9]{4}[-]{1}[0-9]{2}[-]{1}[0-9]{2}[T]{1}[0-9]{2}[:]{1}[0-9]{2}[:]{1}[0-9]{2}[Z]{1}'
        }
    }
    """

    Given path '/api/gates-edge/gates/resource/1017409'
    And header Content-Type = 'application/json'
    And header Authorization = 'Bearer ' + accessToken
    And header iii-customer-domain = custDomain
    And header iii-customer-site-code = ilsSiteCode
    And header iii-customer-id = customerInfo.customerData.id
    And header Accept-Encoding =  '*'
    And header api-version = 1
    When method get
    Then status 200



  Scenario: Clean up after tests - Make customer inactive

    * def results = karate.call('../customer/make_licences_inactive.feature',customerInfo)
