Feature: Validate Patron

  Background:
    * def now = function(){ return java.lang.System.currentTimeMillis() }

   # * def envUrl = "ds-conv-bs-gates-polaris-pr-16.iii-inspire.dev"


    * def envUrl = "cf-gates.iii-conv.com"
    * def ilsSiteCode = "sierra" + now()
    * def description = "ZZZTestRail: devops-8868"

    * def custDomain = "admin.cf-gates.iii-conv.com"
    * def json = { url: '#(envUrl)', ilsSiteCode: '#(ilsSiteCode)', custdomain: '#(custDomain)', description: '#(description)' }



    * def res = callonce read('../customer/create_sierra_customer_k8s_1.feature') json


    * def json1 = { url: '#(envUrl)'}
    * def kc = karate.call('../keycloak/keycloak_token.feature',json1)
    * def access_token = kc.response.access_token

# the JSON returned from 'karate.info' has the following properties:
#   - featureDir
#   - featureFileName
#   - scenarioName
#   - scenarioType (either 'Scenario' or 'Scenario Outline')
#   - scenarioDescription
#   - errorMessage (will be not-null if the Scenario failed)


    * configure afterScenario =
    """
    function(){
    var info = karate.info;
    var vals = karate.tagValues;

    karate.log('suiteId', vals.suiteId[0]);

      karate.log('after type name', info.scenarioType + ':', info.scenarioName);
      karate.log('after scenarioDescription', info.scenarioDescription);
      karate.log('after errorMessage', info.errorMessage);
      karate.call('after-scenario.feature', { caller: info.featureFileName });

    }
    """

    * configure afterFeature = function(){ karate.call('after-feature.feature'); }

  @caseId=836373 @projectId=24 @suiteId=2192
  Scenario: Patron Validate - good barcode, good pin

    Given url 'https://' + envUrl + '/api/gates-edge/gates/patrons/validate'
    And header Content-Type = 'application/json'
    And header Authorization = 'Bearer ' + access_token
    And header iii-customer-domain = custDomain
    And header iii-customer-site-code = ilsSiteCode
    And header iii-customer-id = res.customerData.id
    And header Accept-Encoding =  '*'
    And header api-version = 1
    And request
    """
    {
    barcode:"george",
    pin:"deese1"
    }
    """
    When method post
    Then status 200
    And match response ==
    """
    {
    "valid": true,
    "patronId": "2183455"
    }
    """

  * def results = karate.call('../customer/make_licences_inactive.feature',res)

  * def runName = "Patron Validate on Sierra"
  * def testRailJson = {caseId: '#(caseId)', runName: '#(runName)'}
  * print karate.info
  #* def testRail = karate.call('../../testrail/testrail.feature',testRailJson)

