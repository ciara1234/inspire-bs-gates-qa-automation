Feature: Testing testail

  Background:


    * configure afterScenario =
    """
      function(){
        var info = karate.info;
        var caseId = karate.tagValues.caseId[0]

        karate.log(info);
        karate.log('after', info.scenarioType + ':', info.scenarioName);
        karate.call('after-scenario.feature', { caller: info.featureFileName, caseId: caseId });

      }
    """

  @caseId=836373
  Scenario: Check an url, see what happens
      Given url "https://news.ycombinator.com/news"
      When method get
      Then status 200

    * def vals = karate.tagValues
    * def caseId = vals.caseId[0]
    * def runName = "test a HTTP GET"
    * def testRailJson = {caseId: '#(caseId)', runName: '#(runName)'}
    * print karate.info

      * def testRail = karate.call('../../testrail/testrail.feature',testRailJson)
