Feature: Karate browser automation test

  Background:
    * def wait = read('wait-util.js')
    * configure driver = { type: 'chrome' }

  Scenario: open up docs, click around

    * driver 'https://staging.iii-conv.com/docs/api.html'
    * call wait 2
    #*  match driver.title == 'Convergence API'
    #* driver.highlight('#ulist')
    * driver.click('^Gates API')
    * call wait 2
    * driver.click('^Patrons')
    * call wait 2
    * driver.location = 'https://staging.iii-conv.com/docs/gates-api/docs/patrons.html'
    * call wait 2
    * driver.click('^Patron Validate (v.1)')
    * driver.pdf()
    #//*[@id="content"]/div[2]/div/div[3]/div[1]/div/pre/code
    ##content > div:nth-child(2) > div > div:nth-child(3) > div.listingblock > div > pre > code


