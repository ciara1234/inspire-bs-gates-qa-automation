Feature: Create customer on Customer API and assign connection and check connection test

  Background:
    * def now = function(){ return java.lang.System.currentTimeMillis() }


    * def envUrl = "ds-conv-bs-gates-sierra-pr-43.iii-inspire.dev"
    * def custDomain = "admin.ds-conv-bs-gates-sierra-pr-43.iii-inspire.dev"

    * def ilsSiteCode = "sierra" + now()
    * def description = "Sierra Gates QA Test: connection test"

    * def json = { url: '#(envUrl)', ilsSiteCode: '#(ilsSiteCode)', custdomain: '#(custDomain)',description: '#(description)' }
    * def res = callonce read('../customer/create_polaris_customer_k8s_1.feature') json

    * def envUrlAsJson = { url: '#(envUrl)'}
    * def kc = karate.call('../keycloak/keycloak_token.feature',envUrlAsJson)
    * def access_token = kc.response.access_token

  Scenario: Connection test - bad secret

    * def body =
    """
      {

      "type" : "polaris",
      "clientId" : "polarisdev",
      "clientSecret" : "4B3C1D34-7BA6-4D5E-8112-1C9A6BB7CD74AAAAA",
      "apiUrl" : "https://rd-polaris.polarislibrary.com/PAPIService/REST",
      "staffUsername": "polarisexec",
      "staffPassword": "P0laris!@",
      "staffDomain": "iii"
      }
    """

    Given url 'https://' + envUrl + '/api/gates-edge/gates/connection/test'
    And header Content-Type = 'application/json'
    And header Authorization = 'Bearer ' + access_token
    And header iii-customer-domain = custDomain
    And header iii-customer-site-code = ilsSiteCode
    And header iii-customer-id = res.customerData.id
    And header Accept-Encoding =  '*'
    And header api-version = 1
    And request body
    When method post
    Then status 401



  Scenario: Make customer inactive

    * def results = karate.call('../customer/make_licences_inactive.feature',res)
