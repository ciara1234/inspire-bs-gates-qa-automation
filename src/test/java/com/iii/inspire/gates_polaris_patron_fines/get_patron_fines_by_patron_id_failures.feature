Feature: Create customer on Customer API and assign connection and check Patron

  Background:

    * def now = function(){ return java.lang.System.currentTimeMillis() }

    * url envUrl
    * def ilsSiteCode = "patronfines" + now()
    * def ilsDescription = "Gates QA Test for Polaris: get patron fines by id"

    * def customerSiteDetails = { url: '#(envUrl)', ilsSiteCode: '#(ilsSiteCode)', custdomain: '#(custDomain)',description: '#(ilsDescription)' }
    * def customerInfo = callonce read('../customer/create_polaris_customer_k8s_1.feature') customerSiteDetails

    * def envUrlAsJson = { url: '#(envUrl)'}
    * def keycloak = karate.call('../keycloak/keycloak_token.feature', envUrlAsJson)
    * def accessToken = keycloak.response.access_token




  Scenario: 03 Get Patron Fines by Patron Id in Polaris : invalid barcode

    Given path '/api/gates-edge/gates/patrons/2183607/fines'
    And header Content-Type = 'application/json'
    And header Authorization = 'Bearer ' + accessToken
    And header iii-customer-domain = custDomain
    And header iii-customer-site-code = ilsSiteCode
    And header iii-customer-id = customerInfo.customerData.id
    And header Accept-Encoding =  '*'
    And header api-version = 1
    When method get
    Then status 500
    * def responseBody = response
    And assert responseBody.message == "Error retrieving patron ID. "

  Scenario: 04 Get Patron Fines by Patron Id in Polaris : fake site code, bad format for customer id

    Given path '/api/gates-edge/gates/patrons/20191234123412/fines'
    And header Content-Type = 'application/json'
    And header Authorization = 'Bearer ' + accessToken
    And header iii-customer-domain = custDomain
    And header iii-customer-site-code = 'fakey'
    And header iii-customer-id = 'fakefake'
    And header Accept-Encoding =  '*'
    And header api-version = 1
    When method get
    Then status 400
    * def responseBody = response
    And assert responseBody.message == "Invalid Customer ID format: fakefake"

  Scenario:05 Get Patron Fines by Patron Id in Polaris: good id format, not a customer

    Given path '/api/gates-edge/gates/patrons/20191234123412/fines'
    And header Content-Type = 'application/json'
    And header Authorization = 'Bearer ' + accessToken
    And header iii-customer-domain = custDomain
    And header iii-customer-site-code = 'fakey'
    And header iii-customer-id = '6343a48a-dead-c0de-d00d-0fbccb7a3b2f'
    And header Accept-Encoding =  '*'
    And header api-version = 1
    When method get
    Then status 400
    * def responseBody = response
    And assert responseBody.message == "Customer '6343a48a-dead-c0de-d00d-0fbccb7a3b2f' doesn't exist"

  Scenario: 06 Get Patron Fines by Patron Id in Polaris : good customer,no api header

    Given path '/api/gates-edge/gates/patrons/20191234123412/fines'
    And header Content-Type = 'application/json'
    And header Authorization = 'Bearer ' + accessToken
    And header iii-customer-domain = custDomain
    And header iii-customer-site-code = ilsSiteCode
    And header iii-customer-id = customerInfo.customerData.id
    And header Accept-Encoding =  '*'
    #And header api-version = 1
    When method get
    Then status 404
    And response.message == "Not Found"

  Scenario:07 Get Patron Fines by Patron Id in Polaris: good customer, bad api header

    Given path '/api/gates-edge/gates/patrons/20191234123412/fines'
    And header Content-Type = 'application/json'
    And header Authorization = 'Bearer ' + accessToken
    And header iii-customer-domain = custDomain
    And header iii-customer-site-code = ilsSiteCode
    And header iii-customer-id = customerInfo.customerData.id
    And header Accept-Encoding =  '*'
    And header api-version = 12
    When method get
    Then status 404
    And response.message == "Not Found"

  Scenario: Make customer inactive

    * def results = karate.call('../customer/make_licences_inactive.feature',customerInfo)
