Feature: Create customer on Customer API and assign connection and check Patron fines

  Background:

    * def now = function(){ return java.lang.System.currentTimeMillis() }
    * url envUrl

    * def ilsSiteCode = "gates" + now()
    * def ilsDescription = "Gates QA Test for Polaris: get patron fines by id"

    * def customerSiteDetails = { url: '#(envUrl)', ilsSiteCode: '#(ilsSiteCode)', custdomain: '#(custDomain)',description: '#(ilsDescription)' }
    * def customerInfo = callonce read('../customer/create_polaris_customer_k8s_1.feature') customerSiteDetails

    * def envUrlAsJson = { url: '#(envUrl)'}
    * def keycloak = karate.call('../keycloak/keycloak_token.feature', envUrlAsJson)
    * def accessToken = keycloak.response.access_token


  Scenario: 01 Get Patron fines by Id in Polaris where patron has no fines

    * def patronFinesStructure =
     """
    {
       fineId: '#string',
       itemId: '##string',
       fineDate : '##regex [0-9]{4}[-]{1}[0-9]{2}[-]{1}[0-9]{2}[T]{1}[0-9]{2}[:]{1}[0-9]{2}[:]{1}[0-9]{2}[Z]{1}',
       finePaidDate : '##regex [0-9]{4}[-]{1}[0-9]{2}[-]{1}[0-9]{2}[T]{1}[0-9]{2}[:]{1}[0-9]{2}[:]{1}[0-9]{2}[Z]{1}',
       fineType : '##string',
       fineDescription : '##string',
       locationCode :  '#string',
       paidAmount : '#number',
       outstandingAmount" : '##number'
    }
    """

    Given path '/api/gates-edge/gates/patrons/1000200040201/fines'
    And header Content-Type = 'application/json'
    And header Authorization = 'Bearer ' + accessToken
    And header iii-customer-domain = custDomain
    And header iii-customer-site-code = ilsSiteCode
    And header iii-customer-id = customerInfo.customerData.id
    And header Accept-Encoding =  '*'
    And header api-version = 1
    When method get
    Then status 200
    And match each response.fines contains patronFinesStructure


  Scenario:02 Get Patron fines by Id in Polaris where patron has some fines

    * def patronFinesStructure =
     """
    {
       fineId: '#string',
       itemId: '##string',
       fineDate : '##regex [0-9]{4}[-]{1}[0-9]{2}[-]{1}[0-9]{2}[T]{1}[0-9]{2}[:]{1}[0-9]{2}[:]{1}[0-9]{2}[Z]{1}',
       finePaidDate : '##regex [0-9]{4}[-]{1}[0-9]{2}[-]{1}[0-9]{2}[T]{1}[0-9]{2}[:]{1}[0-9]{2}[:]{1}[0-9]{2}[Z]{1}',
       fineType : '##string',
       fineDescription : '##string',
       locationCode :  '#string',
       paidAmount : '#number',
       outstandingAmount" : '##number'
    }
    """

    Given path '/api/gates-edge/gates/patrons/20191234123412/fines'
    And header Content-Type = 'application/json'
    And header Authorization = 'Bearer ' + accessToken
    And header iii-customer-domain = custDomain
    And header iii-customer-site-code = ilsSiteCode
    And header iii-customer-id = customerInfo.customerData.id
    And header Accept-Encoding =  '*'
    And header api-version = 1
    When method get
    Then status 200
    And match each response.fines contains patronFinesStructure


  Scenario: After tests - Make customer Archived

   * def results = karate.call('../customer/make_licences_inactive.feature',customerInfo)
