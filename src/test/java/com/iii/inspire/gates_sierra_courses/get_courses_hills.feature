Feature: Create customer on Customer API and assign connection and check courses

  #good test data
  #https://devops-8985-app.iii-lab.eu:443/iii/sierra-api/v5/courses/?limit=1&offset=66
  Background:
    * def now = function(){ return java.lang.System.currentTimeMillis() }

    * def envUrl = "https://sandbox-stargates.iii-inspire.dev"
    * def custDomain = "admin.sandbox-stargates.iii-inspire.dev"

    * def ilsSiteCode = "gates" + now()
    * def description = "Gates QA Test: Courses"

    * def json = { url: '#(envUrl)', ilsSiteCode: '#(ilsSiteCode)', custdomain: '#(custDomain)',description: '#(description)' }
   #* def res = callonce read('../customer/create_sierra_five_nitely_customer_k8s_1.feature') json
   #* def res = callonce read('../customer/create_sierra_6916_customer_k8s_1.feature') json
    * def res = callonce read('../customer/create_sierra_customer_hills.feature') json

    * def siteAsJson = { url: '#(envUrl)'}
    * def kc = karate.call('../keycloak/keycloak_token.feature',siteAsJson)
    * def access_token = kc.response.access_token


  Scenario: Get courses in sierra - no params

    Given url 'https://' + envUrl + '/api/gates-edge/gates/courses'
    And header Content-Type = 'application/json'
    And header Authorization = 'Bearer ' + access_token
    And header iii-customer-domain = custDomain
    And header iii-customer-site-code = ilsSiteCode
    And header iii-customer-id = res.customerData.id
    And header Accept-Encoding =  '*'
    And header api-version = 1
    When method get
    Then status 200



  Scenario: Make customer inactive

    * def results = karate.call('../customer/make_licences_inactive.feature',res)
