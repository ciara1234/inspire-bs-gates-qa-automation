Feature: Validate Patron

  Background:
    * def now = function(){ return java.lang.System.currentTimeMillis() }

    * def envUrl = "https://prodlite.iii-conv.com"
    * def ilsSiteCode = "patronvalidate" + now()
    * def ilsDescription = "Sierra 4.3 Gates QA Test: patron validate"

    * def custDomain = "admin.prodlite.iii-conv.com"

    * def customerSiteDetails = { url: '#(envUrl)', ilsSiteCode: '#(ilsSiteCode)', custdomain: '#(custDomain)',description: '#(ilsDescription)' }
    * def customerInfo = callonce read('../customer/create_sierra_customer_k8s_1.feature') customerSiteDetails

    * def envUrlAsJson = { url: '#(envUrl)'}
    * def keycloak = karate.call('../keycloak/keycloak_token.feature', envUrlAsJson)
    * def accessToken = keycloak.response.access_token

    * url envUrl

  Scenario: Patron Validate - good barcode, good pin

    Given path '/api/gates-edge/gates/patrons/validate'
    And header Content-Type = 'application/json'
    And header Authorization = 'Bearer ' + accessToken
    And header iii-customer-domain = custDomain
    And header iii-customer-site-code = ilsSiteCode
    And header iii-customer-id = customerInfo.customerData.id
    And header Accept-Encoding =  '*'
    And header api-version = 1
    And request
    """
    {
    barcode:"george",
    pin:"deese1"
    }
    """
    When method post
    Then status 200
    And match response ==
    """
    {
    "valid": true,
    "patronId": "2183455"
    }
    """


  Scenario: clean up and archive customer
    * def results = karate.call('../customer/make_licences_inactive.feature',customerInfo)


