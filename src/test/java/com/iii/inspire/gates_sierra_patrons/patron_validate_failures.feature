Feature: Validate Patron

  Background:
    * def now = function(){ return java.lang.System.currentTimeMillis() }

    * def envUrl = "https://prodlite.iii-conv.com"
    * def ilsSiteCode = "patronvalidate" + now()
    * def ilsDescription = "Sierra 4.3 Gates QA Test: patron validate"

    * def custDomain = "admin.prodlite.iii-conv.com"

    * def customerSiteDetails = { url: '#(envUrl)', ilsSiteCode: '#(ilsSiteCode)', custdomain: '#(custDomain)',description: '#(ilsDescription)' }
    * def customerInfo = callonce read('../customer/create_sierra_customer_k8s_1.feature') customerSiteDetails

    * def envUrlAsJson = { url: '#(envUrl)'}
    * def keycloak = karate.call('../keycloak/keycloak_token.feature', envUrlAsJson)
    * def accessToken = keycloak.response.access_token

    * url envUrl

  Scenario: Patron Validate - good barcode no pin

    Given path '/api/gates-edge/gates/patrons/validate'
    And header Content-Type = 'application/json'
    And header Authorization = 'Bearer ' + accessToken
    And header iii-customer-domain = custDomain
    And header iii-customer-site-code = ilsSiteCode
    And header iii-customer-id = customerInfo.customerData.id
    And header Accept-Encoding =  '*'
    And header api-version = 1
    And request
    """
    {
          barcode:"george",
          pin:""
    }
   """
    When method post
    Then status 400
    And match response ==
    """
    {
       "path": "/patrons/validate",
        "error": "Bad Request",
        "message": "Invalid parameter : Invalid barcode or PIN",
        "timestamp": "#string",
        "status": 400
    }
    """

  Scenario: Patron Validate - good barcode bad pin
    Given path '/api/gates-edge/gates/patrons/validate'
    And header Content-Type = 'application/json'
    And header Authorization = 'Bearer ' + accessToken
    And header iii-customer-domain = custDomain
    And header iii-customer-site-code = ilsSiteCode
    And header iii-customer-id = customerInfo.customerData.id
    And header Accept-Encoding =  '*'
    And header api-version = 1
    And request
    """
    {
          barcode:"george",
          pin:"deese1XXX"
    }
   """
    When method post
    Then status 400
    And match response ==
    """
    {
      "path": "/patrons/validate",
      "error": "Bad Request",
      "message": "Invalid parameter : Invalid barcode or PIN",
      "timestamp": "#string",
      "status": 400
    }
    """

  Scenario: Patron Validate - bad barcode bad pin

    Given path '/api/gates-edge/gates/patrons/validate'
    And header Content-Type = 'application/json'
    And header Authorization = 'Bearer ' + accessToken
    And header iii-customer-domain = custDomain
    And header iii-customer-site-code = ilsSiteCode
    And header iii-customer-id = customerInfo.customerData.id
    And header X-III-POLARIS-ORG-ID = 3
    And header Accept-Encoding =  '*'
    And header api-version = 1
    And request
    """
    {
          barcode:"georgeXXX",
          pin:"1234XXX"
    }
   """
    When method post
    Then status 400
    And match response ==
    """
  {
      "path": "/patrons/validate",
      "error": "Bad Request",
      "message": "Invalid parameter : Invalid barcode or PIN",
      "timestamp": "#string",
      "status": 400
  }
    """

  Scenario: Patron Validate - bad barcode no pin


    Given path '/api/gates-edge/gates/patrons/validate'
    And header Content-Type = 'application/json'
    And header Authorization = 'Bearer ' + accessToken
    And header iii-customer-domain = custDomain
    And header iii-customer-site-code = ilsSiteCode
    And header iii-customer-id = customerInfo.customerData.id
    And header Accept-Encoding =  '*'
    And header api-version = 1
    And request
    """
    {
          barcode:"georgeXXX",
          pin:""
    }
   """
    When method post
    Then status 400
    And match response ==
    """
    {
       "path": "/patrons/validate",
        "error": "Bad Request",
        "message": "Invalid parameter : Invalid barcode or PIN",
        "timestamp": "#string",
        "status": 400
    }
    """

  Scenario: Patron Validate - no barcode no pin


    Given path '/api/gates-edge/gates/patrons/validate'
    And header Content-Type = 'application/json'
    And header Authorization = 'Bearer ' + accessToken
    And header iii-customer-domain = custDomain
    And header iii-customer-site-code = ilsSiteCode
    And header iii-customer-id = customerInfo.customerData.id
    And header Accept-Encoding =  '*'
    And header api-version = 1
    And request
    """
    {
          barcode:"",
          pin:""
    }
   """
    When method post
    Then status 400
    And match response ==
    """
    {
      "path": "/patrons/validate",
      "error": "Bad Request",
      "message": "Invalid parameter : Invalid barcode or PIN",
      "timestamp": "#string",
      "status": 400
    }
    """


  Scenario: Patron Validate - no body just braces


    Given path  '/api/gates-edge/gates/patrons/validate'
    And header Content-Type = 'application/json'
    And header Authorization = 'Bearer ' + accessToken
    And header iii-customer-domain = custDomain
    And header iii-customer-site-code = ilsSiteCode
    And header iii-customer-id = customerInfo.customerData.id
    And header Accept-Encoding =  '*'
    And header api-version = 1
    And request
    """
    {
    }
   """
    When method post
    Then status 400
    And match response ==
    """
          {

          "path":"/patrons/validate",
          "error":"Bad Request",
          "message":"Illegal request parameter received.",
          "timestamp":"#string",
          "status":400

          }
    """

  Scenario: Patron Validate - no body at all

    Given path '/api/gates-edge/gates/patrons/validate'
    And header Content-Type = 'application/json'
    And header Authorization = 'Bearer ' + accessToken
    And header iii-customer-domain = custDomain
    And header iii-customer-site-code = ilsSiteCode
    And header iii-customer-id = customerInfo.customerData.id
    And header Accept-Encoding =  '*'
    And header api-version = 1
    And request ""
    When method post
    Then status 400
    And match response ==
    """
  {
      "path": "/patrons/validate",
      "error": "Bad Request",
      "message": "Illegal request parameter received. (class org.springframework.http.converter.HttpMessageNotReadableException)",
      "timestamp": "#string",
      "status": 400
  }
    """

  Scenario: clean up and archive customer
    * def results = karate.call('../customer/make_licences_inactive.feature',customerInfo)
