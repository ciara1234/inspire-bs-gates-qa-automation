Feature: Create customer on Customer API and assign connection
  Background:

    * def now = function(){ return java.lang.System.currentTimeMillis() }

    * def envUrl = "https://sandbox-stargates.iii-inspire.dev"
    * def ilsSiteCode = "gates" + now()
   # * def ilsSiteCode = "streaming"
    * def ilsDescription = "locations stuff"

    * def custDomain = "admin.sandbox-stargates.iii-inspire.dev"

    * def customerSiteDetails = { url: '#(envUrl)', ilsSiteCode: '#(ilsSiteCode)', custdomain: '#(custDomain)',description: '#(ilsDescription)' }
    * def customerInfo = callonce read('../customer/create_sierra_customer_k8s_1.feature') customerSiteDetails

    * def envUrlAsJson = { url: '#(envUrl)'}
    * def keycloak = karate.call('../keycloak/keycloak_token.feature', envUrlAsJson)
    * def accessToken = keycloak.response.access_token

    * url envUrl

  Scenario: Get locations

     Given path '/api/gates-edge/gates/locations'
    And header Content-Type = 'application/json'
    And header Authorization = 'Bearer ' + accessToken
    And header iii-customer-domain = custDomain
    And header iii-customer-site-code = ilsSiteCode
    And header iii-customer-id = customerInfo.customerData.id
    And header Accept-Encoding =  '*'
    And header api-version = 2
    When method get
    Then status 200



  Scenario: After tests - Make customer Archived

   * def results = karate.call('../customer/make_licences_inactive.feature',customerInfo)
