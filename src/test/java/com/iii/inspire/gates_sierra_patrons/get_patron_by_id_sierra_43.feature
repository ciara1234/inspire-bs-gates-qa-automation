Feature: Create customer on Customer API and assign connection and check Patron

  Background:

    * def now = function(){ return java.lang.System.currentTimeMillis() }

    * def envUrl = "https://dis-17403.iii-conv.com"
   # * def ilsSiteCode = "gates" + now()
    * def ilsSiteCode = "rabbitcf"
    * def ilsDescription = "sierra 4.3 customer"

    * def custDomain = "admin.dis-17403.iii-conv.com"

    * def customerSiteDetails = { url: '#(envUrl)', ilsSiteCode: '#(ilsSiteCode)', custdomain: '#(custDomain)',description: '#(ilsDescription)' }
    * def customerInfo = callonce read('../customer/create_sierra_customer_k8s_1.feature') customerSiteDetails

    * def envUrlAsJson = { url: '#(envUrl)'}
    * def keycloak = karate.call('../keycloak/keycloak_token.feature', envUrlAsJson)
    * def accessToken = keycloak.response.access_token

    * url envUrl

  Scenario: Get Patron by Id in Sierra - v1 Api

    * def patronStructure =
     """
    {
        id: '#number',
        barcodes: '##array',
        patronName: '##array',
        emailAddress: '##array',
        birthDate: '##regex [0-9]{4}[-]{1}[0-9]{2}[-]{1}[0-9]{2}',
        homeLibraryCode: '##string',
        messages:'##array',
        blocks: '##array',
        address: '##array',
        phoneNumber: '##array',
        expirationDate: '#regex [0-9]{4}[-]{1}[0-9]{2}[-]{1}[0-9]{2}',
        moneyOwed: '#number'
    }
    """

    Given path '/api/gates-edge/gates/patrons/2183607'
    And header Content-Type = 'application/json'
    And header Authorization = 'Bearer ' + accessToken
    And header iii-customer-domain = custDomain
    And header iii-customer-site-code = ilsSiteCode
    And header iii-customer-id = customerInfo.customerData.id
    And header Accept-Encoding =  '*'
    And header api-version = 1
    When method get
    Then status 200
    And match response contains patronStructure



#  Scenario: After tests - Make customer Archived

 #   * def results = karate.call('../customer/make_licences_inactive.feature',customerInfo)
