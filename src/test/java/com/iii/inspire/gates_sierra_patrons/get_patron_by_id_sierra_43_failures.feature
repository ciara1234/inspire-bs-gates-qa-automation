Feature: Create customer on Customer API and assign connection and check Patron

  Background:

    * def now = function(){ return java.lang.System.currentTimeMillis() }

    * def envUrl = "https://ds-conv-is-gates-edge-pr-39.iii-inspire.dev"
    * def ilsSiteCode = "gates" + now()
    * def ilsDescription = "Gates QA Test for Sierra: get patron by id"

    * def custDomain = "admin.ds-conv-is-gates-edge-pr-39.iii-inspire.dev"

    * def customerSiteDetails = { url: '#(envUrl)', ilsSiteCode: '#(ilsSiteCode)', custdomain: '#(custDomain)',description: '#(ilsDescription)' }
    * def customerInfo = callonce read('../customer/create_sierra_customer_k8s_1.feature') customerSiteDetails

    * def envUrlAsJson = { url: '#(envUrl)'}
    * def keycloak = karate.call('../keycloak/keycloak_token.feature', envUrlAsJson)
    * def accessToken = keycloak.response.access_token

    * url envUrl

  Scenario: Get Patron by Id in Sierra - no api version



    Given path '/api/gates-edge/gates/patrons/2183478'
    And header Content-Type = 'application/json'
    And header Authorization = 'Bearer ' + accessToken
    And header iii-customer-domain = custDomain
    And header iii-customer-site-code = ilsSiteCode
    And header iii-customer-id = customerInfo.customerData.id
    And header Accept-Encoding =  '*'
   # And header api-version = 1
    When method get
    Then status 404


  Scenario: Get Patron by Id in Sierra - wrong api version


    Given path '/api/gates-edge/gates/patrons/2183478'
    And header Content-Type = 'application/json'
    And header Authorization = 'Bearer ' + accessToken
    And header iii-customer-domain = custDomain
    And header iii-customer-site-code = ilsSiteCode
    And header iii-customer-id = customerInfo.customerData.id
    And header Accept-Encoding =  '*'
    And header api-version = 3
    When method get
    Then status 404

    Scenario: Bad input - v1 - invalid number

      Given path '/api/gates-edge/gates/patrons/0'
      And header Content-Type = 'application/json'
      And header Authorization = 'Bearer ' + accessToken
      And header iii-customer-domain = custDomain
      And header iii-customer-site-code = ilsSiteCode
      And header iii-customer-id = customerInfo.customerData.id
      And header Accept-Encoding =  '*'
      And header api-version = 1
      When method get
      Then status 401


  Scenario: Bad input - v2 - invalid number
      Given path '/api/gates-edge/gates/patrons/0'
      And header Content-Type = 'application/json'
      And header Authorization = 'Bearer ' + accessToken
      And header iii-customer-domain = custDomain
      And header iii-customer-site-code = ilsSiteCode
      And header iii-customer-id = customerInfo.customerData.id
      And header Accept-Encoding =  '*'
      And header api-version = 2
      When method get
      Then status 401



  Scenario: Bad input - v1 - bad characters

    Given path '/api/gates-edge/gates/patrons/%00'
    And header Content-Type = 'application/json'
    And header Authorization = 'Bearer ' + accessToken
    And header iii-customer-domain = custDomain
    And header iii-customer-site-code = ilsSiteCode
    And header iii-customer-id = customerInfo.customerData.id
    And header Accept-Encoding =  '*'
    And header api-version = 1
    When method get
    Then status 403
    And response.message = "The request was rejected because the URL contained a potentially malicious String \"%25\""


  Scenario: Bad input - v2 - bad characters
    Given path '/api/gates-edge/gates/patrons/%00'
    And header Content-Type = 'application/json'
    And header Authorization = 'Bearer ' + accessToken
    And header iii-customer-domain = custDomain
    And header iii-customer-site-code = ilsSiteCode
    And header iii-customer-id = customerInfo.customerData.id
    And header Accept-Encoding =  '*'
    And header api-version = 2
    When method get
    Then status 403
    And response.message = "The request was rejected because the URL contained a potentially malicious String \"%25\""


  Scenario: Bad input - v1 - bad characters - 1001300A

    Given path '/api/gates-edge/gates/patrons/1001300A'
    And header Content-Type = 'application/json'
    And header Authorization = 'Bearer ' + accessToken
    And header iii-customer-domain = custDomain
    And header iii-customer-site-code = ilsSiteCode
    And header iii-customer-id = customerInfo.customerData.id
    And header Accept-Encoding =  '*'
    And header api-version = 1
    When method get
    Then status 400

  Scenario: Bad input - v2 - bad characters - 1001300A

    Given path '/api/gates-edge/gates/patrons/1001300A'
    And header Content-Type = 'application/json'
    And header Authorization = 'Bearer ' + accessToken
    And header iii-customer-domain = custDomain
    And header iii-customer-site-code = ilsSiteCode
    And header iii-customer-id = customerInfo.customerData.id
    And header Accept-Encoding =  '*'
    And header api-version = 2
    When method get
    Then status 400

  Scenario: Bad input - v1 - invalid number - 2345834736252

    Given path '/api/gates-edge/gates/patrons/2345834736252'
    And header Content-Type = 'application/json'
    And header Authorization = 'Bearer ' + accessToken
    And header iii-customer-domain = custDomain
    And header iii-customer-site-code = ilsSiteCode
    And header iii-customer-id = customerInfo.customerData.id
    And header Accept-Encoding =  '*'
    And header api-version = 1
    When method get
    Then status 500
    And response.message = "Backend service response error"

  Scenario: Bad input - v2 - invalid number - 2345834736252

    Given path '/api/gates-edge/gates/patrons/2345834736252'
    And header Content-Type = 'application/json'
    And header Authorization = 'Bearer ' + accessToken
    And header iii-customer-domain = custDomain
    And header iii-customer-site-code = ilsSiteCode
    And header iii-customer-id = customerInfo.customerData.id
    And header Accept-Encoding =  '*'
    And header api-version = 2
    When method get
    Then status 500
    And response.message = "Backend service response error"



  Scenario: After tests - Make customer Archived


    * def results = karate.call('../customer/make_licences_inactive.feature',customerInfo)
