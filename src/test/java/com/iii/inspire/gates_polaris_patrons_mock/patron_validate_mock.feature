Feature: Validate Patron

  Background:
    * def now = function(){ return java.lang.System.currentTimeMillis() }

    * def envUrl = "https://ds-conv-bs-gates-polaris-pr-19.iii-inspire.dev"
    * def ilsSiteCode = "polaris" + now()
    * def ilsDescription = "aaaaGates QA - Polaris: Mock server test"

    * def custDomain = "admin.ds-conv-bs-gates-polaris-pr-19.iii-inspire.dev"

    * def customerSiteDetails = { url: '#(envUrl)', ilsSiteCode: '#(ilsSiteCode)', custdomain: '#(custDomain)',description: '#(ilsDescription)' }
    * def customerInfo = callonce read('../customer/create_mock_polaris_customer_k8s_1.feature') customerSiteDetails

    * def envUrlAsJson = { url: '#(envUrl)'}
    * def keycloak = karate.call('../keycloak/keycloak_token.feature', envUrlAsJson)
    * def accessToken = keycloak.response.access_token

    * url envUrl


  Scenario: Patron Validate - good barcode, good pin

    Given path '/api/gates-edge/gates/patrons/validate'
    And header Content-Type = 'application/json'
    And header Authorization = 'Bearer ' + accessToken
    And header iii-customer-domain = custDomain
    And header iii-customer-site-code = ilsSiteCode
    And header iii-customer-id = customerInfo.customerData.id
    And header X-III-POLARIS-ORG-ID = 3
    And header Accept-Encoding =  '*'
    And header api-version = 1
    And request
  """
{"barcode": "good_barcode", "pin": "good_password"}
  """
    When method post
    Then status 200
    And match response ==
  """
  {
  "valid": true,
  "patronId": "good_barcode"
  }
  """

  Scenario: clean up after - make customer inactive after tests

    * def results = karate.call('../customer/make_licences_inactive.feature',customerInfo)
