@ignore
Feature: Create customer on Customer API and assign connection - using devops 6916 Sierra 5.0 Nightly always on server
  Background:

    * def resJson = __arg
    #* print resJson
    #* print resJson.url
    #* print resJson.ilsSiteCode
    #* print resJson.custdomain

    * def json = { url: '#(resJson.url)'}
    * def kc = karate.call('../keycloak/keycloak_token.feature',json)
    * def access_token = kc.response.access_token
    #* def customer_id = ""
    * url resJson.url

  Scenario: Create Customer with Connection

    Given path '/api/customer/customers/'
    And header 'Content-Type' = 'application/json'
    And header Authorization = 'Bearer ' + access_token
    And header iii-customer-domain = resJson.custdomain
    And header api-version = 1
    And request
      """
      {
      name : "#(resJson.description)",
      siteCode :'#(resJson.ilsSiteCode)',
      types : [ {
      "type" : "LIBRARY" }]
      }
      """
    When method post
    Then status 201
    And def customerData = response
    And set customerData.envK8s = resJson.url
    And set customerData.custDomain = resJson.custdomain

    * def customer_id = response.id

    Given path  '/api/customer/dictionaries/license/names'
    And header 'Content-Type' = 'application/json'
    And header Authorization = 'Bearer ' + access_token
    And header iii-customer-domain = resJson.custdomain
    And header api-version = 1
    When method get
    Then status 200

    * def discovery = response[1].id
    * def context_engine = response[0].id

    Given path '/api/customer/customers/' + customer_id + '/licenses'
    And header 'Content-Type' = 'application/json'
    And header Authorization = 'Bearer ' + access_token
    And header iii-customer-domain = resJson.custdomain
    And header api-version = 2
    And request
    """
    {
    licenseName :"Context Engine",
    status : "ACTIVE",
    expirationDate : "2020-10-05"
    }
    """
    When method post
    Then status 201
    And set customerData.contextLicenseId = response.licenseId
    * def contextLicense = response.licenseId

    Given path  '/api/customer/customers/' + customer_id + '/licenses'
    And header 'Content-Type' = 'application/json'
    And header Authorization = 'Bearer ' + access_token
    And header iii-customer-domain = resJson.custdomain
    And header api-version = 2
    And request
    """
    {
    licenseName :"Discovery",
    status : "ACTIVE",
    expirationDate : "2020-10-05"
    }
    """
    When method post
    Then status 201
    And set customerData.disLicenseId = response.licenseId
    * def disLicense = response.licenseId

    Given path '/api/customer/customers/' + customer_id + '/connection'
    And header 'Content-Type' = 'application/json'
    And header api-version = 3
    And header Authorization = 'Bearer ' + access_token
    And header iii-customer-domain = resJson.custdomain
    And request
    """
        {
        "type" : "sierra",
        "casUrl" : "https://devops-8225-app.iii-lab.eu/iii/cas",
        "clientId" : "F+JnJGwYVWkexeZt2ZzWe9PuRdct",
        "clientSecret" : "goodsecret",
        "apiUrl" : "https://devops-8225-app.iii-lab.eu:443/iii/sierra-api",
        "stompLoginHeader" : "aaaa",
        "stompPasscodeHeader" : "2111",
        "stompUrl" : "aaaaa"
        }
    """
    When method post
    Then status 201
    And set customerData.connectionId = response.connectionId

    Given url resJson.url + '/api/customer/customers/' + customer_id + '/connection?type=sierra'
    And header 'Content-Type' = 'application/json'
    And header api-version = 3
    And header Authorization = 'Bearer ' + access_token
    And header iii-customer-domain = resJson.custdomain
    When method get
    Then status 200

    * def customer_id = response.customerId

    * print customerData


  #### returns this object ###
  ### customerData
  ###
  ### customerId
  ### ilsSiteCode
  ### custDomain
  ### contextLicenseId
  ### disLicenseId
  ##  envK8s
  ###########################
