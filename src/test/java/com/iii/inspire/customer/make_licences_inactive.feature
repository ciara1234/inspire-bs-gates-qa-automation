@ignore
Feature: Make a customer inactive after a test - makes the two licenses for Discover and Context Engine Archived
  Background:

    * def resJson = __arg
  #* print resJson
  # * print resJson.customerData.disLicenseId
  # * print resJson.customerData.contextLicenseId
  # * print resJson.customerData.custDomain
  # * print resJson.customerData.envK8s

    * def json1 = { url: '#(resJson.customerData.envK8s)'}
    * def kc = karate.call('../keycloak/keycloak_token.feature',json1)
    * def access_token = kc.response.access_token

  Scenario: Assuming customer only has Discovery + Context Engine Licenses: Deactivate the licenses

    Given url resJson.customerData.envK8s + '/api/customer/customers/' + resJson.customerData.id + '/licenses/' + resJson.customerData.disLicenseId
    And header 'Content-Type' = 'application/json'
    And header Authorization = 'Bearer ' + access_token
    And header iii-customer-domain = resJson.customerData.custDomain
    And header iii-customer-site-code = resJson.customerData.ilsSiteCode
    And header api-version = 2
    And request
      """
      {
      licenseName :"Discovery",
      status : "ARCHIVED",
      expirationDate : null
      }
      """
    When method put
    Then status 200

    Given url  resJson.customerData.envK8s + '/api/customer/customers/' + resJson.customerData.id + '/licenses/' + resJson.customerData.contextLicenseId
    And header 'Content-Type' = 'application/json'
    And header Authorization = 'Bearer ' + access_token
    And header iii-customer-domain = resJson.customerData.custDomain
    And header iii-customer-site-code = resJson.customerData.ilsSiteCode
    And header api-version = 2
    And request
      """
      {
      licenseName :"Context Engine",
      status : "ARCHIVED",
      expirationDate : null
      }
      """
    When method put
    Then status 200


