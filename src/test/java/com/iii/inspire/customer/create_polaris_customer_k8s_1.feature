@ignore
Feature: Create customer on Customer API and assign connection to rd-polaris.polarislibrary.com Polaris Server
  Background:
    * def now = function(){ return java.lang.System.currentTimeMillis() }

    #* def envUrl = "ds-conv-bs-gates-polaris-pr-15.iii-inspire.dev"
    * def site_code = "polaris" + now()
    #* def cust_domain = "admin.ds-conv-bs-gates-polaris-pr-15.iii-inspire.dev"

    * def resJson = __arg
    * print resJson
    * print resJson.url
    * print resJson.ilsSiteCode
    * print resJson.custdomain

    * def json = { url: '#(resJson.url)'}
    * def kc = karate.call('../keycloak/keycloak_token.feature',json)
    * def access_token = kc.response.access_token

    * url resJson.url
    #* def customer_id = ""

  #### returns this object ###
  ### customerData
  ###
  ### customerId
  ### ilsSiteCode
  ### custDomain
  ### contextLicenseId
  ### disLicenseId
  ##  envK8s
  ##################

  Scenario: Create Customer with Connection and check locations

    Given path '/api/customer/customers/'
    And header 'Content-Type' = 'application/json'
    And header Authorization = 'Bearer ' + access_token
    And header iii-customer-domain = resJson.custdomain
    And header api-version = 1
    And request
  """
  {
  name : "#(resJson.description)",
  siteCode :'#(resJson.ilsSiteCode)',
  types : [ {
  "type" : "LIBRARY" }]
  }
  """
    When method post
    Then status 201
    And def customerData = response
    And set customerData.envK8s = resJson.url
    And set customerData.custDomain = resJson.custdomain

    * def customer_id = response.id

    Given path '/api/customer/dictionaries/license/names'
    And header 'Content-Type' = 'application/json'
    And header Authorization = 'Bearer ' + access_token
    And header iii-customer-domain = resJson.custdomain
    And header api-version = 1
    When method get
    Then status 200

    * def discovery = response[1].id
    * def context_engine = response[0].id

    Given path '/api/customer/customers/' + customer_id + '/licenses'
    And header 'Content-Type' = 'application/json'
    And header Authorization = 'Bearer ' + access_token
    And header iii-customer-domain = resJson.custdomain
    And header api-version = 2
    And request
  """
  {
  licenseName :"Context Engine",
  status : "ACTIVE",
  expirationDate : "2020-10-05"
  }
  """
    When method post
    Then status 201
    And set customerData.contextLicenseId = response.licenseId
    * def contextLicense = response.licenseId

    Given path  '/api/customer/customers/' + customer_id + '/licenses'
    And header 'Content-Type' = 'application/json'
    And header Authorization = 'Bearer ' + access_token
    And header iii-customer-domain = resJson.custdomain
    And header api-version = 2
    And request
  """
  {
  licenseName :"Discovery",
  status : "ACTIVE",
  expirationDate : "2020-10-05"
  }
  """
    When method post
    Then status 201
    And set customerData.disLicenseId = response.licenseId
    * def disLicense = response.licenseId

    Given path '/api/customer/customers/' + customer_id + '/connection'
    And header 'Content-Type' = 'application/json'
    And header api-version = 3
    And header Authorization = 'Bearer ' + access_token
    And header iii-customer-domain = resJson.custdomain
    And request
  """
  {
      "type" : "polaris",
      "clientId" : "polarisdev",
      "clientSecret" : "4B3C1D34-7BA6-4D5E-8112-1C9A6BB7CD74",
      "apiUrl" : "https://rd-polaris.polarislibrary.com/PAPIService/REST",
      "staffUsername": "polarisexec",
      "staffPassword": "P0laris!@",
      "staffDomain": "iii"
  }
  """
    When method post
    Then status 201
    And set customerData.connectionId = response.connectionId

    Given url resJson.url +  '/api/customer/customers/' + customer_id + '/connection?type=polaris'
    And header 'Content-Type' = 'application/json'
    And header api-version = 3
    And header Authorization = 'Bearer ' + access_token
    And header iii-customer-domain = resJson.custdomain
    When method get
    Then status 200

    * def customer_id = response.customerId

    * print customerData

