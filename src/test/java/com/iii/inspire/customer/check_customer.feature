@ignore
Feature:
  Background:


    * def envUrl = 'cf-gates.iii-conv.com'
    * def json = { url: '#(envUrl)'}
    * def kc = karate.call('../keycloak/keycloak_token.feature',json)
    * def access_token = kc.response.access_token
    * def custDomain = "admin.cf-gates.iii-conv.com"


  Scenario: check customers 1

    Given url 'https://' + envUrl + '/api/customer/customers/?status=ACTIVE&size=9999'
    And header 'Content-Type' = 'application/json'
    And header api-version = 1
    And header Authorization = 'Bearer ' + access_token
    And header iii-customer-domain = custDomain
    When method get
    Then status 200


  Scenario: check customers 2

      Given url 'https://' + envUrl + '/api/customer/customers/search?status=ACTIVE&size=9999'
      And header 'Content-Type' = 'application/json'
      And header api-version = 1
      And header Authorization = 'Bearer ' + access_token
      And header iii-customer-domain = custDomain
      When method get
      Then status 200

  Scenario: delete connection


    Given url 'https://' + envUrl + '/api/customer/customers/fa3b3457-d86c-4b08-a0f1-3fea1fb5bc0a/connection/a1bd4321-817d-403c-9158-e69c64cf2ff9?type=sierra
    And header 'Content-Type' = 'application/json'
    And header api-version = 1
    And header Authorization = 'Bearer ' + access_token
    And header iii-customer-domain = custDomain
    When method delete
    Then status 200
