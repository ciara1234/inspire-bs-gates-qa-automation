Feature: Create customer on Customer API and assign connection and check Patron Holds on Polaris

  #https://rd-polaris.polarislibrary.com/LeapWebApp/circulation/default.aspx#patrons/367068/record

  Background:
    * def now = function(){ return java.lang.System.currentTimeMillis() }


    * url envUrl
    * def ilsSiteCode = "polaris" + now()
   # * def ilsSiteCode = "thirtyfiftyhogs"
    * def ilsDescription = "Gates TEST 👍👍👍"

    * def customerSiteDetails = { url: '#(envUrl)', ilsSiteCode: '#(ilsSiteCode)', custdomain: '#(custDomain)',description: '#(ilsDescription)' }
    * def customerInfo = callonce read('../customer/create_polaris_customer_k8s_1.feature') customerSiteDetails

    * def envUrlAsJson = { url: '#(envUrl)'}
    * def keycloak = karate.call('../keycloak/keycloak_token.feature',envUrlAsJson)
    * def accessToken = keycloak.response.access_token

  Scenario: 02 Get Patron holds in Polaris



    Given path '/api/gates-edge/gates/patrons/1004300089725/holds'
    And header Content-Type = 'application/json'
    And header Authorization = 'Bearer ' + accessToken
    And header iii-customer-domain = custDomain
    And header iii-customer-site-code = ilsSiteCode
    And header iii-customer-id = customerInfo.customerData.id
    And header Accept-Encoding =  '*'
    And header api-version = 1
    When method get
    Then status 200
   # And match each response.fines contains fineStructure

  Scenario: clean up after - archive licences for customer

   * def results = karate.call('../customer/make_licences_inactive.feature',customerInfo)
