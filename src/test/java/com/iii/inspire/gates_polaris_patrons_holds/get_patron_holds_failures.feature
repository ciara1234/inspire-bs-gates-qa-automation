Feature: Create customer on Customer API and assign connection and check Patron Basic Data on Polaris

  #https://rd-polaris.polarislibrary.com/LeapWebApp/circulation/default.aspx#patrons/367068/record

  Background:
    * def now = function(){ return java.lang.System.currentTimeMillis() }


    * url envUrl
    * def ilsSiteCode = "polaris" + now()
   # * def ilsSiteCode = "thirtyfiftyhogs"
    * def ilsDescription = "Gates TEST 👍👍👍"

    * def customerSiteDetails = { url: '#(envUrl)', ilsSiteCode: '#(ilsSiteCode)', custdomain: '#(custDomain)',description: '#(ilsDescription)' }
    * def customerInfo = callonce read('../customer/create_polaris_customer_k8s_1.feature') customerSiteDetails

    * def envUrlAsJson = { url: '#(envUrl)'}
    * def keycloak = karate.call('../keycloak/keycloak_token.feature',envUrlAsJson)
    * def accessToken = keycloak.response.access_token

  Scenario: 01 Get Patron Holds by Patron Id in Polaris : fake site code, bad format for customer id

    Given path '/api/gates-edge/gates/patrons/2183607/holds'
    And header Content-Type = 'application/json'
    And header Authorization = 'Bearer ' + accessToken
    And header iii-customer-domain = custDomain
    And header iii-customer-site-code = 'fakey'
    And header iii-customer-id = 'fakefake'
    And header Accept-Encoding =  '*'
    And header api-version = 1
    When method get
    Then status 400
    * def responseBody = response
    And assert responseBody.message == "Invalid Customer ID format: fakefake"

  Scenario: 02 Get Patron Holds by Patron Id in Polaris: good id format, not a customer


    Given path '/api/gates-edge/gates/patrons/20191234123412/holds'
    And header Content-Type = 'application/json'
    And header Authorization = 'Bearer ' + accessToken
    And header iii-customer-domain = custDomain
    And header iii-customer-site-code = 'fakey'
    And header iii-customer-id = '6343a48a-dead-c0de-d00d-0fbccb7a3b2f'
    And header Accept-Encoding =  '*'
    And header api-version = 1
    When method get
    Then status 400
    * def responseBody = response
    And assert responseBody.message == "Customer '6343a48a-dead-c0de-d00d-0fbccb7a3b2f' doesn't exist"

  Scenario: 03 Get Patron Holds by Patron Id in Polaris : good customer,no api header

    Given path '/api/gates-edge/gates/patrons/20191234123412/holds'
    And header Content-Type = 'application/json'
    And header Authorization = 'Bearer ' + accessToken
    And header iii-customer-domain = custDomain
    And header iii-customer-site-code = ilsSiteCode
    And header iii-customer-id = customerInfo.customerData.id
    And header Accept-Encoding =  '*'
    #And header api-version = 1
    When method get
    Then status 404

  Scenario: 04 Get Patron Holds by Patron Id in Polaris: good customer, bad api header

    Given path '/api/gates-edge/gates/patrons/20191234123412/holds'
    And header Content-Type = 'application/json'
    And header Authorization = 'Bearer ' + accessToken
    And header iii-customer-domain = custDomain
    And header iii-customer-site-code = ilsSiteCode
    And header iii-customer-id = customerInfo.customerData.id
    And header Accept-Encoding =  '*'
    And header api-version = 12
    When method get
    Then status 404

  Scenario: Make customer inactive

    * def results = karate.call('../customer/make_licences_inactive.feature',customerInfo)


