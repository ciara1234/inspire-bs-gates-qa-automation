Feature: Get it

  Background:

    * def now = function(){ return java.lang.System.currentTimeMillis() }

    * def envUrl = "https://ds-inspire-bs-get-it-pr-7.iii-inspire.dev"
    * def ilsSiteCode = "getitholds" + now()
    * def ilsDescription = "Sierra Gates QA Test: get patron hold form"

    * def custDomain = "admin.ds-inspire-bs-get-it-pr-7.iii-inspire.dev"

    * def customerSiteDetails = { url: '#(envUrl)', ilsSiteCode: '#(ilsSiteCode)', custdomain: '#(custDomain)',description: '#(ilsDescription)' }
    * def customerInfo = callonce read('../customer/create_sierra_customer_8020.feature') customerSiteDetails

    * def envUrlAsJson = { url: '#(envUrl)'}
    * def keycloak = karate.call('../keycloak/keycloak_token.feature', envUrlAsJson)
    * def accessToken = keycloak.response.access_token

    * def wait = read('classpath:wait-util.js')
    * call wait 5

    * url envUrl

  Scenario: Hold form - v1 - correct params - success

    Given path '/api/get-it/gates/request-form'
    And header Content-Type = 'application/json'
    And header Authorization = 'Bearer ' + accessToken
    And header iii-customer-domain = custDomain
    And header iii-customer-site-code = ilsSiteCode
    And header iii-customer-id = customerInfo.customerData.id
    And header Accept-Encoding =  '*'
    And header api-version = 1
    And request {"patronId":2184258,"recordNumber":"6390547","recordType":"b"}
    When method post
    Then status 200
    And match response.pickupLocations[0].code == "ap"

  #Scenario: After tests - Make customer Archived

   #* def results = karate.call('../customer/make_licences_inactive.feature',customerInfo)
