Feature: Patron Auth

  Background:

    Scenario: Get Overdrive Oauth token

    Given url 'https://oauth-patron.overdrive.com/patrontoken'
    And header Content-Type = 'application/x-www-form-urlencoded'
    And header Authorization = 'Basic SU5OT1ZBVElWRUlOVEVSRkFDRVM6eXNkOVliZ2R1UW8zYndjWWQ3a3dwektvaHQ0MVBVYzc='
    And form field grant_type = 'password'
    And form field username = '.p1276649'
    And form field password = '[ignore]'
    And form field scope = 'websiteid:100300 authorizationname:innovmobileqa'
    And form field password_required = false
    And request {}
    When method post
    Then status 200

   * def OverDrivePatronToken = response.access_token

#Patron holds
      Given url 'https://patron.api.overdrive.com/v1/patrons/me/holds'
      And header Authorization = 'Bearer ' + OverDrivePatronToken
      When method get
      Then status 200

#metadata
      Given url 'https://api.overdrive.com/v1/collections/v1P1BBQ0AAA2N/products/EBFF8D7B-C9A0-4478-A1ED-63B64E8386C5/metadata'
      And header Authorization = 'Bearer ' + OverDrivePatronToken
      When method get
      Then status 200



