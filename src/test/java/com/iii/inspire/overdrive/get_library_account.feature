Feature: Library Auth

  Background:

    Scenario: Get Overdrive Oauth token

    Given url 'https://oauth.overdrive.com/token'
    And header Content-Type = 'application/x-www-form-urlencoded'
    And header Authorization = 'Basic SU5OT1ZBVElWRUlOVEVSRkFDRVM6eXNkOVliZ2R1UW8zYndjWWQ3a3dwektvaHQ0MVBVYzc='
    And form field grant_type = 'client_credentials'
    And request {}
    When method post
    Then status 200

   * def OverDriveToken = response.access_token

#Library Account
      Given url 'https://api.overdrive.com/v1/libraries/4425'
      And header Authorization = 'Bearer ' + OverDriveToken
      When method get
      Then status 200

#search
      Given url 'https://api.overdrive.com/v1/collections/v1L1BBQ0AAA2_/products'
      And header Authorization = 'Bearer ' + OverDriveToken
      When method get
      Then status 200



