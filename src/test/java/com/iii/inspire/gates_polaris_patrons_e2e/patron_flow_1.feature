Feature: Create customer on Customer API and assign connection and simulate patron flow

  #https://rd-polaris.polarislibrary.com/LeapWebApp/circulation/default.aspx#patrons/367068/record

  Background:
    * def now = function(){ return java.lang.System.currentTimeMillis() }

    * def envUrl = "https://sandbox-stargates.iii-inspire.dev"
    * def ilsSiteCode = "polaris" + now()
    * def ilsDescription = "Gates QA - Polaris: Patron E2E"

    * def custDomain = "admin.sandbox-stargates.iii-inspire.dev"

    * def customerSiteDetails = { url: '#(envUrl)', ilsSiteCode: '#(ilsSiteCode)', custdomain: '#(custDomain)',description: '#(ilsDescription)' }
    * def customerInfo = callonce read('../customer/create_polaris_customer_k8s_1.feature') customerSiteDetails

    * def envUrlAsJson = { url: '#(envUrl)'}
    * def keycloak = karate.call('../keycloak/keycloak_token.feature', envUrlAsJson)
    * def accessToken = keycloak.response.access_token

    * url envUrl

  Scenario: E2E Flow: Patron Validate, Get Patron Data, and then get list of Pickuplocations

    Given path '/api/gates-edge/gates/patrons/validate'
    And header Content-Type = 'application/json'
    And header Authorization = 'Bearer ' + accessToken
    And header iii-customer-domain = custDomain
    And header iii-customer-site-code = ilsSiteCode
    And header iii-customer-id = customerInfo.customerData.id
    And header X-III-POLARIS-ORG-ID = 3
    And header Accept-Encoding =  '*'
    And header api-version = 1
    And request
  """
  {
  barcode:"1004300089725",
  pin:"1234"
  }
  """
    When method post
    Then status 200
    And match response ==
  """
  {
  "valid": true,
  "patronId": "1004300089725"
  }
  """

 * def patronStructure =
  """
    {
        id: '#number',
        barcodes: '##array',
        patronName: '##array',
        emailAddress: '##array',
        birthDate: '#regex [0-9]{4}[-]{1}[0-9]{2}[-]{1}[0-9]{2}',
        homeLibraryCode: '##string',
        messages:'##array',
        blocks: '##array',
        address: '##array',
        phoneNumber: '##array',
        expirationDate: '#regex [0-9]{4}[-]{1}[0-9]{2}[-]{1}[0-9]{2}',
        moneyOwed: '#number'
    }
    """

    Given path '/api/gates-edge/gates/patrons/1004300089725'
    And header Content-Type = 'application/json'
    And header Authorization = 'Bearer ' + accessToken
    And header iii-customer-domain = custDomain
    And header iii-customer-site-code = ilsSiteCode
    And header iii-customer-id = customerInfo.customerData.id
    And header Accept-Encoding =  '*'
    And header api-version = 2
    When method get
    Then status 200



    * def patronId = "1004300089725"
    * def patronHomeLibraryCode = response.homeLibraryCode

    Given path '/api/gates-edge/gates/patrons/' + patronId + '/holds/request-form'
    And header Content-Type = 'application/json'
    And header Authorization = 'Bearer ' + accessToken
    And header iii-customer-domain = custDomain
    And header iii-customer-site-code = ilsSiteCode
    And header iii-customer-id = customerInfo.customerData.id
    And header Accept-Encoding =  '*'
    And header api-version = 2
    And request {"homeLibraryCode": '#(patronHomeLibraryCode)',"recordNumber" : 1234,"recordType" : "b"}
    When method post
    Then status 200
    And match response.pickupLocations[0].code == "3"
    And match response.pickupLocations[1].code == "5"


  Scenario: 01 Patron place hold - book -  bib record can be requested

    Given path  '/api/gates-edge/gates/patrons/1004300089725/holds'
    And header Content-Type = 'application/json'
    And header Authorization = 'Bearer ' + accessToken
    And header iii-customer-domain = custDomain
    And header iii-customer-site-code = ilsSiteCode
    And header iii-customer-id = customerInfo.customerData.id
    And header Accept-Encoding =  '*'
    And header api-version = 1
    And request
  """
  {
    "recordNumber":506986,
    "pickupLocation":"23",
    "recordType":"b"
  }
  """
    When method post
    Then status 200
    And match response contains
    """
    {
      "holdId":#number
    }
    """




  Scenario: clean up after - make customer inactive after tests

    * def results = karate.call('../customer/make_licences_inactive.feature',customerInfo)
