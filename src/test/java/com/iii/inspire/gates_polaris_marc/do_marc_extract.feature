Feature: Create customer on Customer API and assign connection and check items status

  Background:
    * def now = function(){ return java.lang.System.currentTimeMillis() }

    * def envUrl = "https://sandbox-stargates.iii-inspire.dev"
    * def ilsSiteCode = "polaris" + now()
    * def ilsDescription = "Gates QA : Marc Extract"

    * def custDomain = "admin.sandbox-stargates.iii-inspire.dev"

    * def customerSiteDetails = { url: '#(envUrl)', ilsSiteCode: '#(ilsSiteCode)', custdomain: '#(custDomain)',description: '#(ilsDescription)' }
    * def customerInfo = callonce read('../customer/create_sierra_customer_k8s_1.feature') customerSiteDetails

    * def envUrlAsJson = { url: '#(envUrl)'}
    * def keycloak = karate.call('../keycloak/keycloak_token.feature',envUrlAsJson)
    * def accessToken = keycloak.response.access_token

    * url envUrl

  Scenario: Trigger Marc Extract

    * def flowId0 = "flow-all" + now()
    * def flowId1 = "flow-created" + now()
    * def flowId2 = "flow-created-updated" + now()
    * def flowId3 = "flow-updated" + now()

    Given path  '/api/gates-edge/gates/bibs-all'
    And header Content-Type = 'application/json'
    And header Authorization = 'Bearer ' + accessToken
    And header iii-customer-domain = custDomain
    And header iii-customer-site-code = ilsSiteCode
    And header iii-customer-id = customerInfo.customerData.id
    And header Accept-Encoding =  '*'
    And header api-version = 1
   # And request {"flowId": "#(flowId0)"}
  # And request {"flowId": "#(flowId2)", "created": "2013-01-01,2019-06-01", "updated": "2013-01-01,2019-06-01"}
    And request {"flowId": "#(flowId1)", "created": "2019-01-01,2019-08-01"}
 # And request {"flowId": "#(flowId3)", "updated": "2016-05-01,2019-06-01"}
    When method post
    Then status 200


 # Scenario: clean up after - make customer licenses inactive

  #  * def results = karate.call('../customer/make_licences_inactive.feature',customerInfo)

