Feature: Create customer on Customer API and assign connection and check hold status

  Background:

    * def now = function(){ return java.lang.System.currentTimeMillis() }

    * def envUrl = "https://staging.iii-conv.com"
    * def ilsSiteCode = "staging" + now()
    * def ilsDescription = "staging test"

    * def custDomain = "admin.staging.iii-conv.com"

    * def customerSiteDetails = { url: '#(envUrl)', ilsSiteCode: '#(ilsSiteCode)', custdomain: '#(custDomain)',description: '#(ilsDescription)' }
    * def customerInfo = callonce read('../customer/create_polaris_customer_k8s_1.feature') customerSiteDetails

    * def envUrlAsJson = { url: '#(envUrl)'}
    * def keycloak = karate.call('../keycloak/keycloak_token.feature', envUrlAsJson)
    * def accessToken = keycloak.response.access_token

    * url envUrl

  Scenario: Get material types

    Given path '/api/gates-edge/gates/materialtypes'
    And header Content-Type = 'application/json'
    And header Authorization = 'Bearer ' + accessToken
    And header iii-customer-domain = custDomain
    And header iii-customer-site-code = ilsSiteCode
    And header iii-customer-id = customerInfo.customerData.id
    And header Accept-Encoding =  '*'
    And header api-version = 1
    When method get
    Then status 200


  Scenario: Clean up after tests - Make customer inactive

    * def results = karate.call('../customer/make_licences_inactive.feature',customerInfo)
