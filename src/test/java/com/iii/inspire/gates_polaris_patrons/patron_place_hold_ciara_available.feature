Feature: Place hold for Polaris Patron

  Background:

    * def now = function(){ return java.lang.System.currentTimeMillis() }

    * def envUrl = "https://ds-conv-bs-gates-polaris-pr-21.iii-inspire.dev"
    * def ilsSiteCode = "polaris" + now()
    * def ilsDescription = "Gates QA - Polaris: Patron Place Hold"

    * def custDomain = "admin.ds-conv-bs-gates-polaris-pr-21.iii-inspire.dev"

    * def customerSiteDetails = { url: '#(envUrl)', ilsSiteCode: '#(ilsSiteCode)', custdomain: '#(custDomain)',description: '#(ilsDescription)' }
    * def customerInfo = callonce read('../customer/create_polaris_customer_k8s_1.feature') customerSiteDetails

    * def envUrlAsJson = { url: '#(envUrl)'}
    * def keycloak = karate.call('../keycloak/keycloak_token.feature', envUrlAsJson)
    * def accessToken = keycloak.response.access_token

    * url envUrl


  Scenario: 06. Patron place hold - Bib available at library - You can obtain the item at the library right now, you know

    Given path  '/api/gates-edge/gates/patrons/1004300089725/holds'
    And header Content-Type = 'application/json'
    And header Authorization = 'Bearer ' + accessToken
    And header iii-customer-domain = custDomain
    And header iii-customer-site-code = ilsSiteCode
    And header iii-customer-id = customerInfo.customerData.id
    And header Accept-Encoding =  '*'
    And header api-version = 1
    And request
  """
  {
    "recordNumber":404198,
    "pickupLocation":"23",
    "recordType":"b"
  }
  """
    When method post
    Then status 500
    And response.message == "The request cannot be placed because a copy of this title is currently available at the library. You can obtain the item at the library right now, you know. "

  Scenario: Clean up after - make customer Archived after tests

    * def results = karate.call('../customer/make_licences_inactive.feature',customerInfo)

