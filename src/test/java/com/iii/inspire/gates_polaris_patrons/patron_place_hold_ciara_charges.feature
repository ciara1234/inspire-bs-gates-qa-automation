Feature: Place hold for Polaris Patron

  Background:

    * def now = function(){ return java.lang.System.currentTimeMillis() }

    * def envUrl = "https://ds-conv-bs-gates-polaris-pr-23.iii-inspire.dev"
    * def ilsSiteCode = "polaris" + now()
    * def ilsDescription = "Gates QA - Polaris: Patron Place Hold"

    * def custDomain = "admin.ds-conv-bs-gates-polaris-pr-23.iii-inspire.dev"

    * def customerSiteDetails = { url: '#(envUrl)', ilsSiteCode: '#(ilsSiteCode)', custdomain: '#(custDomain)',description: '#(ilsDescription)' }
    * def customerInfo = callonce read('../customer/create_polaris_customer_k8s_1.feature') customerSiteDetails

    * def envUrlAsJson = { url: '#(envUrl)'}
    * def keycloak = karate.call('../keycloak/keycloak_token.feature', envUrlAsJson)
    * def accessToken = keycloak.response.access_token

    * url envUrl


  Scenario: 15. Patron place hold - it has a charge so it should fail

    Given path  '/api/gates-edge/gates/patrons/1004300089725/holds'
    And header Content-Type = 'application/json'
    And header Authorization = 'Bearer ' + accessToken
    And header iii-customer-domain = custDomain
    And header iii-customer-site-code = ilsSiteCode
    And header iii-customer-id = customerInfo.customerData.id
    And header Accept-Encoding =  '*'
    And header api-version = 1
    And request
  """
  {
    "recordNumber":748744,
    "pickupLocation":"11",
    "recordType":"b",
    "note" : "charged"
  }
  """
    When method post
    Then status 500
    #And response.message == "The request cannot be placed because your account has exceeded the maximum number of requests for this material type.  You can cancel some requests for this material type or wait for a request to be filled, then submit this request again."

  Scenario: Clean up after - make customer Archived after tests

    * def results = karate.call('../customer/make_licences_inactive.feature',customerInfo)

