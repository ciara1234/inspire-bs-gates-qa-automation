Feature: Place hold for Polaris Patron

  Background:

    * def now = function(){ return java.lang.System.currentTimeMillis() }

    * def envUrl = "https://ds-conv-bs-gates-polaris-pr-21.iii-inspire.dev"
    * def ilsSiteCode = "polaris" + now()
    * def ilsDescription = "Gates QA - Polaris: Patron Place Hold"

    * def custDomain = "admin.ds-conv-bs-gates-polaris-pr-21.iii-inspire.dev"

    * def customerSiteDetails = { url: '#(envUrl)', ilsSiteCode: '#(ilsSiteCode)', custdomain: '#(custDomain)',description: '#(ilsDescription)' }
    * def customerInfo = callonce read('../customer/create_polaris_customer_k8s_1.feature') customerSiteDetails

    * def envUrlAsJson = { url: '#(envUrl)'}
    * def keycloak = karate.call('../keycloak/keycloak_token.feature', envUrlAsJson)
    * def accessToken = keycloak.response.access_token

    * url envUrl

  Scenario: 01 Patron place hold - book - "Down and out in the Magic Kindgom" home library -  bib record can be requested

    Given path  '/api/gates-edge/gates/patrons/1004300089725/holds'
    And header Content-Type = 'application/json'
    And header Authorization = 'Bearer ' + accessToken
    And header iii-customer-domain = custDomain
    And header iii-customer-site-code = ilsSiteCode
    And header iii-customer-id = customerInfo.customerData.id
    And header Accept-Encoding =  '*'
    And header api-version = 1
    And request
  """
  {
    "recordNumber":506984,
    "pickupLocation":"23",
    "recordType":"b"
  }
  """
    When method post
    Then status 200
    And match response contains
    """
    {
      "holdId":#number
    }
    """

  Scenario: 02 Patron place hold - book - "Down and out in the Magic Kindgom" - home library -  bib record can't be requested because it is already on hold

    Given path  '/api/gates-edge/gates/patrons/1004300089725/holds'
    And header Content-Type = 'application/json'
    And header Authorization = 'Bearer ' + accessToken
    And header iii-customer-domain = custDomain
    And header iii-customer-site-code = ilsSiteCode
    And header iii-customer-id = customerInfo.customerData.id
    And header Accept-Encoding =  '*'
    And header api-version = 1
    And request
  """
  {
    "recordNumber":506984,
    "pickupLocation":"23",
    "recordType":"b"
  }
  """
    When method post
    Then status 500
    And match response.message == "The request cannot be placed because a duplicate request has been found.  Please check your account."



  Scenario: 03 Patron place hold - with patron home lib code - bib record can't be requested

    Given path  '/api/gates-edge/gates/patrons/1004300089725/holds'
    And header Content-Type = 'application/json'
    And header Authorization = 'Bearer ' + accessToken
    And header iii-customer-domain = custDomain
    And header iii-customer-site-code = ilsSiteCode
    And header iii-customer-id = customerInfo.customerData.id
    And header Accept-Encoding =  '*'
    And header api-version = 1
    And request
  """
  {
    "recordNumber":291669,
    "pickupLocation":"23",
    "recordType":"b"
  }
  """
    When method post
    Then status 500
    And match response.message == "Requests are not permitted for this title.  Contact the library for assistance."



  Scenario: 04 Patron place hold - e resource - "Down and out in the Magic Kindgom", restricted - non home library -  bib record can't be requested

    Given path  '/api/gates-edge/gates/patrons/1004300089725/holds'
    And header Content-Type = 'application/json'
    And header Authorization = 'Bearer ' + accessToken
    And header iii-customer-domain = custDomain
    And header iii-customer-site-code = ilsSiteCode
    And header iii-customer-id = customerInfo.customerData.id
    And header Accept-Encoding =  '*'
    And header api-version = 1
    And request
  """
  {
    "recordNumber":1092291,
    "pickupLocation":"110",
    "recordType":"b"
  }
  """
    When method post
    Then status 500
    And match response.message == "Requests are not permitted for this title.  Contact the library for assistance."


  Scenario: 05 Patron place hold - book - 	"Rewired : the post-cyberpunk anthology"- home library -  bib record can't be requested because it is not holdable

    Given path  '/api/gates-edge/gates/patrons/1004300089725/holds'
    And header Content-Type = 'application/json'
    And header Authorization = 'Bearer ' + accessToken
    And header iii-customer-domain = custDomain
    And header iii-customer-site-code = ilsSiteCode
    And header iii-customer-id = customerInfo.customerData.id
    And header Accept-Encoding =  '*'
    And header api-version = 1
    And request
  """
  {
    "recordNumber":741917,
    "pickupLocation":"90",
    "recordType":"b"
  }
  """
    When method post
    Then status 500
    And match response.message == "Requests are not permitted for this title.  Contact the library for assistance."





  Scenario: Clean up after - make customer Archived after tests

    * def results = karate.call('../customer/make_licences_inactive.feature',customerInfo)

