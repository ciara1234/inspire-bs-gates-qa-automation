Feature: Validate Patron

  Background:
    * def now = function(){ return java.lang.System.currentTimeMillis() }

    * def envUrl = "https://ds-conv-bs-gates-polaris-pr-19.iii-inspire.dev"
    * def ilsSiteCode = "polaris" + now()
    * def ilsDescription = "Gates QA - Polaris: Patron Validate"

    * def custDomain = "admin.ds-conv-bs-gates-polaris-pr-19.iii-inspire.dev"

    * def customerSiteDetails = { url: '#(envUrl)', ilsSiteCode: '#(ilsSiteCode)', custdomain: '#(custDomain)',description: '#(ilsDescription)' }
    * def customerInfo = callonce read('../customer/create_polaris_customer_k8s_1.feature') customerSiteDetails

    * def envUrlAsJson = { url: '#(envUrl)'}
    * def keycloak = karate.call('../keycloak/keycloak_token.feature', envUrlAsJson)
    * def accessToken = keycloak.response.access_token

    * url envUrl

  Scenario: Patron Validate - good barcode no pin

    Given path  '/api/gates-edge/gates/patrons/validate'
    And header Content-Type = 'application/json'
    And header Authorization = 'Bearer ' + accessToken
    And header iii-customer-domain = custDomain
    And header iii-customer-site-code = ilsSiteCode
    And header iii-customer-id = customerInfo.customerData.id
    And header X-III-POLARIS-ORG-ID = 3
    And header Accept-Encoding =  '*'
    And header api-version = 1
    And request
    """
    {
          barcode:"20191234123412",
          pin:""
    }
   """
    When method post
    Then status 500
    And match response ==
    """
    {
       "path": "/patrons/validate",
        "error": "Internal Server Error",
        "message": "Password is invalid",
        "timestamp": "#string",
        "status": 500
    }
    """

  Scenario: Patron Validate - good barcode bad pin


    Given path '/api/gates-edge/gates/patrons/validate'
    And header Content-Type = 'application/json'
    And header Authorization = 'Bearer ' + accessToken
    And header iii-customer-domain = custDomain
    And header iii-customer-site-code = ilsSiteCode
    And header iii-customer-id = customerInfo.customerData.id
    And header X-III-POLARIS-ORG-ID = 3
    And header Accept-Encoding =  '*'
    And header api-version = 1
    And request
    """
    {
          barcode:"20191234123412",
          pin:"1234XXX"
    }
   """
    When method post
    Then status 500
    And match response ==
    """
    {
      "path": "/patrons/validate",
      "error": "Internal Server Error",
      "message": "Unable to authenticate the patron credentials.",
      "timestamp": "#string",
      "status": 500
    }
    """

  Scenario: Patron Validate - bad barcode bad pin

    Given path '/api/gates-edge/gates/patrons/validate'
    And header Content-Type = 'application/json'
    And header Authorization = 'Bearer ' + accessToken
    And header iii-customer-domain = custDomain
    And header iii-customer-site-code = ilsSiteCode
    And header iii-customer-id = customerInfo.customerData.id
    And header X-III-POLARIS-ORG-ID = 3
    And header Accept-Encoding =  '*'
    And header api-version = 1
    And request
    """
    {
          barcode:"20191234123412XXX",
          pin:"1234XXX"
    }
   """
    When method post
    Then status 500
    And match response ==
    """
  {
      "path": "/patrons/validate",
      "error": "Internal Server Error",
      "message": "Unable to authenticate the patron credentials.",
      "timestamp": "#string",
      "status": 500
  }
    """

  Scenario: Patron Validate - bad barcode no pin


    Given path '/api/gates-edge/gates/patrons/validate'
    And header Content-Type = 'application/json'
    And header Authorization = 'Bearer ' + accessToken
    And header iii-customer-domain = custDomain
    And header iii-customer-site-code = ilsSiteCode
    And header iii-customer-id = customerInfo.customerData.id
    And header X-III-POLARIS-ORG-ID = 3
    And header Accept-Encoding =  '*'
    And header api-version = 1
    And request
    """
    {
          barcode:"20191234123412XXX",
          pin:""
    }
   """
    When method post
    Then status 500
    And match response ==
    """
    {
       "path": "/patrons/validate",
        "error": "Internal Server Error",
        "message": "Password is invalid",
        "timestamp": "#string",
        "status": 500
    }
    """

  Scenario: Patron Validate - no barcode no pin


    Given path '/api/gates-edge/gates/patrons/validate'
    And header Content-Type = 'application/json'
    And header Authorization = 'Bearer ' + accessToken
    And header iii-customer-domain = custDomain
    And header iii-customer-site-code = ilsSiteCode
    And header iii-customer-id = customerInfo.customerData.id
    And header X-III-POLARIS-ORG-ID = 3
    And header Accept-Encoding =  '*'
    And header api-version = 1
    And request
    """
    {
          barcode:"",
          pin:""
    }
   """
    When method post
    Then status 500
    And match response ==
    """
    {
      "path": "/patrons/validate",
      "error": "Internal Server Error",
      "message": "Barcode is invalid, Password is invalid",
      "timestamp": "#string",
      "status": 500
    }
    """

  Scenario: Patron Validate - no body just braces


    Given path '/api/gates-edge/gates/patrons/validate'
    And header Content-Type = 'application/json'
    And header Authorization = 'Bearer ' + accessToken
    And header iii-customer-domain = custDomain
    And header iii-customer-site-code = ilsSiteCode
    And header iii-customer-id = customerInfo.customerData.id
    And header X-III-POLARIS-ORG-ID = 3
    And header Accept-Encoding =  '*'
    And header api-version = 1
    And request
    """
    {
    }
   """
    When method post
    Then status 400
    And match response ==
    """
          {

          "path":"/patrons/validate",
          "error":"Bad Request",
          "message":"Validation failure",
          "timestamp":"#string",
          "status":400

          }
    """

  Scenario: Patron Validate - no body at all

    Given path '/api/gates-edge/gates/patrons/validate'
    And header Content-Type = 'application/json'
    And header Authorization = 'Bearer ' + accessToken
    And header iii-customer-domain = custDomain
    And header iii-customer-site-code = ilsSiteCode
    And header iii-customer-id = customerInfo.customerData.id
    And header X-III-POLARIS-ORG-ID = 3
    And header Accept-Encoding =  '*'
    And header api-version = 1
    And request ""
    When method post
    Then status 400
    And match response ==
    """
  {
      "path": "/patrons/validate",
      "error": "Bad Request",
      "message": "Request body is missing: public reactor.core.publisher.Mono<com.iii.inspire.gates.polaris.payload.patron.PatronValidateResponse> com.iii.inspire.gates.polaris.controller.PatronController.validatePatron(org.springframework.web.server.ServerWebExchange,com.iii.inspire.gates.polaris.payload.patron.PatronValidateRequestBody)",
      "timestamp": "#string",
      "status": 400
  }
    """

  Scenario: Make customer inactive
    * def results = karate.call('../customer/make_licences_inactive.feature',customerInfo)
