Feature: Create customer on Customer API and assign valid Connection and check patron hold form

  #https://rd-polaris.polarislibrary.com/LeapWebApp/circulation/default.aspx#patrons/367068/record

  Background:
    * def now = function(){ return java.lang.System.currentTimeMillis() }

    * def envUrl = "https://ds-conv-bs-gates-polaris-pr-19.iii-inspire.dev"
    * def ilsSiteCode = "polaris" + now()
    * def ilsDescription = "Gates QA - Polaris: Patron Hold Form"

    * def custDomain = "admin.ds-conv-bs-gates-polaris-pr-19.iii-inspire.dev"

    * def customerSiteDetails = { url: '#(envUrl)', ilsSiteCode: '#(ilsSiteCode)', custdomain: '#(custDomain)',description: '#(ilsDescription)' }
    * def customerInfo = callonce read('../customer/create_polaris_customer_k8s_1.feature') customerSiteDetails

    * def envUrlAsJson = { url: '#(envUrl)'}
    * def keycloak = karate.call('../keycloak/keycloak_token.feature', envUrlAsJson)
    * def accessToken = keycloak.response.access_token

    * url envUrl

  Scenario: 02 Get valid pickup locations with a valid patron Id and Valid HomeLibrary Code, bad api version, and adding placeholder values for recordNumber and RecordType

    Given path  '/api/gates-edge/gates/patrons/367068/holds/request-form'
    And header Content-Type = 'application/json'
    And header Authorization = 'Bearer ' + accessToken
    And header iii-customer-domain = custDomain
    And header iii-customer-site-code = ilsSiteCode
    And header iii-customer-id = customerInfo.customerData.id
    And header Accept-Encoding =  '*'
    And header api-version = 3
    And request {"homeLibraryCode": "105","recordNumber" : 1234,"recordType" : "b",}
    When method post
    Then status 404


  Scenario: 03 Get valid pickup locations with a valid patron Id and Valid HomeLibrary Code, fake customer, and adding placeholder values for recordNumber and RecordType

    Given path '/api/gates-edge/gates/patrons/367068/holds/request-form'
    And header Content-Type = 'application/json'
    And header Authorization = 'Bearer ' + accessToken
    And header iii-customer-domain = "fakedomain"
    And header iii-customer-site-code = "fakesite"
    And header iii-customer-id = "6343a48a-dead-c0de-d00d-0fbccb7a3b2f"
    And header Accept-Encoding =  '*'
    And header api-version = 1
    And request {"homeLibraryCode": "105","recordNumber" : 1234,"recordType" : "b",}
    When method post
    Then status 403
    * def responseBody = response
    And assert responseBody.message == "Requests without siteCode is not permitted"

  Scenario: 04 Get valid pickup locations with a valid patron Id and Invalid HomeLibrary Code, adding placeholder values for recordNumber and RecordType

    Given path '/api/gates-edge/gates/patrons/367068/holds/request-form'
    And header Content-Type = 'application/json'
    And header Authorization = 'Bearer ' + accessToken
    And header iii-customer-domain = custDomain
    And header iii-customer-site-code = ilsSiteCode
    And header iii-customer-id = customerInfo.customerData.id
    And header Accept-Encoding =  '*'
    And header api-version = 2
    And request {"homeLibraryCode": "10544","recordNumber" : 1234,"recordType" : "b",}
    When method post
    Then status 500
    * def responseBody = response
    And assert responseBody.message == "Invalid home library code supplied"


  Scenario: 05 Get valid pickup locations with a Invalid patron Id and valid HomeLibrary Code, adding placeholder values for recordNumber and RecordType

    Given path '/api/gates-edge/gates/patrons/367068999111111111/holds/request-form'
    And header Content-Type = 'application/json'
    And header Authorization = 'Bearer ' + accessToken
    And header iii-customer-domain = custDomain
    And header iii-customer-site-code = ilsSiteCode
    And header iii-customer-id = customerInfo.customerData.id
    And header Accept-Encoding =  '*'
    And header api-version = 2
    And request {"homeLibraryCode": "105","recordNumber" : 1234,"recordType" : "b",}
    When method post
    Then status 200


  Scenario: 06 Get valid pickup locations with a Invalid patron Id and Invalid HomeLibrary Code, adding placeholder values for recordNumber and RecordType

    Given path '/api/gates-edge/gates/patrons/3670689991111/holds/request-form'
    And header Content-Type = 'application/json'
    And header Authorization = 'Bearer ' + accessToken
    And header iii-customer-domain = custDomain
    And header iii-customer-site-code = ilsSiteCode
    And header iii-customer-id = customerInfo.customerData.id
    And header Accept-Encoding =  '*'
    And header api-version = 2
    And request {"homeLibraryCode": "1054445","recordNumber" : 1234,"recordType" : "b",}
    When method post
    Then status 500
    * def responseBody = response
    And assert responseBody.message == "Invalid home library code supplied"


  Scenario: clean up after - make customer licenses inactive

    * def results = karate.call('../customer/make_licences_inactive.feature',customerInfo)
