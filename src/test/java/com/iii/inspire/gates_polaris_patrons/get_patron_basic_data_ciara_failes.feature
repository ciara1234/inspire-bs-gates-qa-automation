Feature: Create customer on Customer API and assign connection and check Patron Basic Data on Polaris

  #https://rd-polaris.polarislibrary.com/LeapWebApp/circulation/default.aspx#patrons/367068/record

  Background:
    * def now = function(){ return java.lang.System.currentTimeMillis() }

    * def envUrl = "https://ds-conv-bs-gates-polaris-pr-21.iii-inspire.dev"
    * def ilsSiteCode = "polaris" + now()
    * def ilsDescription = "Gates QA - Polaris: Patron Basic Data"

    * def custDomain = "admin.ds-conv-bs-gates-polaris-pr-21.iii-inspire.dev"

    * def customerSiteDetails = { url: '#(envUrl)', ilsSiteCode: '#(ilsSiteCode)', custdomain: '#(custDomain)',description: '#(ilsDescription)' }
    * def customerInfo = callonce read('../customer/create_polaris_customer_k8s_1.feature') customerSiteDetails

    * def envUrlAsJson = { url: '#(envUrl)'}
    * def keycloak = karate.call('../keycloak/keycloak_token.feature',envUrlAsJson)
    * def accessToken = keycloak.response.access_token

    * url envUrl
  Scenario: 01 Get Patron basic data in Polaris

    * def patronStructure =
     """
    {
        id: '#number',
        barcodes: '##array',
        patronName: '##array',
        emailAddress: '##array',
        birthDate: '#regex [0-9]{4}[-]{1}[0-9]{2}[-]{1}[0-9]{2}',
        homeLibraryCode: '##string',
        messages:'##array',
        blocks: '##array',
        address: '##array',
        phoneNumber: '##array',
        expirationDate: '#regex [0-9]{4}[-]{1}[0-9]{2}[-]{1}[0-9]{2}',
        moneyOwed: '#number',
        credit: '#number',
        deposit: '#number'
    }
    """

    Given path '/api/gates-edge/gates/patrons/1004300089725'
    And header Content-Type = 'application/json'
    And header Authorization = 'Bearer ' + accessToken
    And header iii-customer-domain = custDomain
    And header iii-customer-site-code = ilsSiteCode
    And header iii-customer-id = customerInfo.customerData.id
    And header Accept-Encoding =  '*'
    And header api-version = 1
    When method get
    Then status 200
    And match response contains patronStructure

  Scenario: clean up after - archive licences for customer

    * def results = karate.call('../customer/make_licences_inactive.feature',customerInfo)
