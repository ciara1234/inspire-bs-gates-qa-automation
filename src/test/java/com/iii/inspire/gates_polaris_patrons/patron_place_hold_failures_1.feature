Feature: Create customer on Customer API and assign connection and check Patron Place hold on polaris
  #https://rd-polaris.polarislibrary.com/LeapWebApp/circulation/default.aspx#patrons/367068/record

  Background:

    * def now = function(){ return java.lang.System.currentTimeMillis() }

    * def envUrl = "https://ds-conv-bs-gates-polaris-pr-21.iii-inspire.dev"
    * def ilsSiteCode = "polaris" + now()
    * def ilsDescription = "Gates QA: Patron Polaris place hold errors"

    * def custDomain = "admin.ds-conv-bs-gates-polaris-pr-21.iii-inspire.dev"

    * def customerSiteDetails = { url: '#(envUrl)', ilsSiteCode: '#(ilsSiteCode)', custdomain: '#(custDomain)',description: '#(ilsDescription)' }
    * def customerInfo = callonce read('../customer/create_polaris_customer_k8s_1.feature') customerSiteDetails

    * def envUrlAsJson = { url: '#(envUrl)'}
    * def keycloak = karate.call('../keycloak/keycloak_token.feature', envUrlAsJson)
    * def accessToken = keycloak.response.access_token

    * url envUrl

 Scenario: 10 Patron Place hold in Polaris - not a patron

   Given path  '/api/gates-edge/gates/patrons/10043000897251111111/holds'
   And header Content-Type = 'application/json'
   And header Authorization = 'Bearer ' + accessToken
   And header iii-customer-domain = custDomain
   And header iii-customer-site-code = ilsSiteCode
   And header iii-customer-id = customerInfo.customerData.id
   And header Accept-Encoding =  '*'
   And header api-version = 1
   And request
  """
  {
    "recordNumber":741917,
    "pickupLocation":"90",
    "recordType":"b"
  }
  """
   When method post
   Then status 500
   And match response.message  == "Error retrieving patron ID. "


 Scenario: 11 Patron Place hold in in Polaris - bad api version


   Given path  '/api/gates-edge/gates/patrons/1004300089725/holds'
   And header Content-Type = 'application/json'
   And header Authorization = 'Bearer ' + accessToken
   And header iii-customer-domain = custDomain
   And header iii-customer-site-code = ilsSiteCode
   And header iii-customer-id = customerInfo.customerData.id
   And header Accept-Encoding =  '*'
   And header api-version = 5
   And request
  """
  {
    "recordNumber":506984,
    "pickupLocation":"23",
    "recordType":"b"
  }
  """
   When method post
   Then status 404
   And match response.error  == "Not Found"

 Scenario: 12 Patron Place hold in in Polaris - not a customer - fake site, good format


   Given path  '/api/gates-edge/gates/patrons/1004300089725/holds'
   And header Content-Type = 'application/json'
   And header Authorization = 'Bearer ' + accessToken
   And header iii-customer-domain = custDomain
   And header iii-customer-site-code = 'fakesite'
   And header iii-customer-id = '6343a48a-dead-c0de-d00d-0fbccb7a3b2f'
   And header Accept-Encoding =  '*'
   And header api-version = 1
   And request
  """
  {
    "recordNumber":506984,
    "pickupLocation":"23",
    "recordType":"b"
  }
  """
   When method post
   Then status 400
   And match response.message == "Customer '6343a48a-dead-c0de-d00d-0fbccb7a3b2f' doesn't exist"

 Scenario: 13 Patron Place hold in in Polaris - not a customer - fake site, bad format

   Given path  '/api/gates-edge/gates/patrons/1004300089725/holds'
   And header Content-Type = 'application/json'
   And header Authorization = 'Bearer ' + accessToken
   And header iii-customer-domain = custDomain
   And header iii-customer-site-code = 'fakesite'
   And header iii-customer-id = 'fakesite'
   And header Accept-Encoding =  '*'
   And header api-version = 1
   And request
  """
  {
    "recordNumber":506984,
    "pickupLocation":"23",
    "recordType":"b"
  }
  """
   When method post
   Then status 400
   And match response.message == "Invalid Customer ID format: fakesite"

 Scenario: 14 Patron Place hold in in Polaris - missing api version

   Given path  '/api/gates-edge/gates/patrons/1004300089725/holds'
   And header Content-Type = 'application/json'
   And header Authorization = 'Bearer ' + accessToken
   And header iii-customer-domain = custDomain
   And header iii-customer-site-code = ilsSiteCode
   And header iii-customer-id = customerInfo.customerData.id
   And header Accept-Encoding =  '*'
   And request
  """
  {
    "recordNumber":506984,
    "pickupLocation":"23",
    "recordType":"b"
  }
  """
   When method post
   Then status 404
   And match response.error  == "Not Found"


  Scenario: clean up after - archive licences for customer

    * def results = karate.call('../customer/make_licences_inactive.feature',customerInfo)
