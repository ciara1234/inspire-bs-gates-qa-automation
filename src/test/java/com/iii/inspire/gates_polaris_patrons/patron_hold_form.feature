Feature: Create customer on Customer API and assign valid Connection and check patron hold form

  #https://rd-polaris.polarislibrary.com/LeapWebApp/circulation/default.aspx#patrons/367068/record

  Background:

    * def now = function(){ return java.lang.System.currentTimeMillis() }

    * def envUrl = "https://sandbox-stargates.iii-inspire.dev"
    * def ilsSiteCode = "polaris" + now()
    * def ilsDescription = "Gates QA - Polaris: Patron Hold Form"

    * def custDomain = "admin.sandbox-stargates.iii-inspire.dev"

    * def customerSiteDetails = { url: '#(envUrl)', ilsSiteCode: '#(ilsSiteCode)', custdomain: '#(custDomain)',description: '#(ilsDescription)' }
    * def customerInfo = callonce read('../customer/create_polaris_customer_k8s_1.feature') customerSiteDetails

    * def envUrlAsJson = { url: '#(envUrl)'}
    * def keycloak = karate.call('../keycloak/keycloak_token.feature', envUrlAsJson)
    * def accessToken = keycloak.response.access_token

    * url envUrl

  Scenario: 00 Get valid pickup locations with a valid patron Id and Valid HomeLibrary Code

    Given path '/api/gates-edge/gates/patrons/tiago/holds/request-form'
    And header Content-Type = 'application/json'
    And header Authorization = 'Bearer ' + accessToken
    And header iii-customer-domain = custDomain
    And header iii-customer-site-code = ilsSiteCode
    And header iii-customer-id = customerInfo.customerData.id
    And header Accept-Encoding =  '*'
    And header api-version = 2
    And request {"homeLibraryCode": "105"}
    When method post
    Then status 200
    And match response.pickupLocations[0].code == "3"
    And match response.pickupLocations[1].code == "5"

  Scenario: 01 Get valid pickup locations with a valid patron Id and Valid HomeLibrary Code, adding placeholder values for recordNumber and RecordType

    Given path  '/api/gates-edge/gates/patrons/367068/holds/request-form'
    And header Content-Type = 'application/json'
    And header Authorization = 'Bearer ' + accessToken
    And header iii-customer-domain = custDomain
    And header iii-customer-site-code = ilsSiteCode
    And header iii-customer-id = customerInfo.customerData.id
    And header Accept-Encoding =  '*'
    And header api-version = 2
    And request {"homeLibraryCode": "105","recordNumber" : 1234,"recordType" : "b",}
    When method post
    Then status 200
    And match response.pickupLocations[0].code == "3"
    And match response.pickupLocations[1].code == "5"

  Scenario: clean up after

    * def results = karate.call('../customer/make_licences_inactive.feature',customerInfo)
