Feature: Create customer on Customer API and assign connection and check items status

  Background:
    * def now = function(){ return java.lang.System.currentTimeMillis() }

    * def envUrl = "https://ds-conv-bs-gates-polaris-pr-23.iii-inspire.dev"
    * def ilsSiteCode = "polaris" + now()
    * def ilsDescription = "Polaris Gates QA Item: Item Status"

    * def custDomain = "admin.ds-conv-bs-gates-polaris-pr-23.iii-inspire.dev"

    * def customerSiteDetails = { url: '#(envUrl)', ilsSiteCode: '#(ilsSiteCode)', custdomain: '#(custDomain)',description: '#(ilsDescription)' }
    * def customerInfo = callonce read('../customer/create_polaris_customer_k8s_1.feature') customerSiteDetails

    * def envUrlAsJson = { url: '#(envUrl)'}
    * def keycloak = karate.call('../keycloak/keycloak_token.feature',envUrlAsJson)
    * def accessToken = keycloak.response.access_token

    * url envUrl

  Scenario: Get Items Statuses in Polaris : fake site, bad format

    Given path '/api/gates-edge/gates/itemstatus'
    And header Content-Type = 'application/json'
    And header Authorization = 'Bearer ' + accessToken
    And header iii-customer-domain = custDomain
    And header iii-customer-site-code = "fake-site"
    And header iii-customer-id = "fake-id"
    And header Accept-Encoding =  '*'
    And header api-version = 1
    When method get
    Then status 400
    And match response.error == "Bad Request"
    And match response.message == "Invalid Customer ID format: fake-id"

  Scenario: Get Items Statuses in Polaris : good format, not a customer

    Given path '/api/gates-edge/gates/itemstatus'
    And header Content-Type = 'application/json'
    And header Authorization = 'Bearer ' + accessToken
    And header iii-customer-domain = custDomain
    And header iii-customer-site-code = "fakesite"
    And header iii-customer-id = "6343a48a-dead-c0de-d00d-0fbccb7a3b2f"
    And header Accept-Encoding =  '*'
    And header api-version = 1
    When method get
    Then status 400
    And match response.error == "Bad Request"
    And match response.message == "Customer '6343a48a-dead-c0de-d00d-0fbccb7a3b2f' doesn't exist"


  Scenario: clean up after - make customer licenses inactive

    * def results = karate.call('../customer/make_licences_inactive.feature',customerInfo)
