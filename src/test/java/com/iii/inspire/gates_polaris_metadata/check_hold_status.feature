Feature: Create customer on Customer API and assign connection and check hold status

  Background:

    * def now = function(){ return java.lang.System.currentTimeMillis() }

    * def envUrl = "https://sandbox-stargates.iii-inspire.dev"
    * def ilsSiteCode = "polaris" + now()
    * def ilsDescription = "Polaris Gates QA Test: Hold Status"

    * def custDomain = "admin.sandbox-stargates.iii-inspire.dev"

    * def customerSiteDetails = { url: '#(envUrl)', ilsSiteCode: '#(ilsSiteCode)', custdomain: '#(custDomain)',description: '#(ilsDescription)' }
    * def customerInfo = callonce read('../customer/create_polaris_customer_k8s_1.feature') customerSiteDetails

    * def envUrlAsJson = { url: '#(envUrl)'}
    * def keycloak = karate.call('../keycloak/keycloak_token.feature',envUrlAsJson)
    * def accessToken = keycloak.response.access_token

    * url envUrl


  Scenario: Get Hold Statuses in Polaris

    Given path '/api/gates-edge/gates/holdstatus'
    And header Content-Type = 'application/json'
    And header Authorization = 'Bearer ' + accessToken
    And header iii-customer-domain = custDomain
    And header iii-customer-site-code = ilsSiteCode
    And header iii-customer-id = customerInfo.customerData.id
    And header Accept-Encoding =  '*'
    And header api-version = 1
    When method get
    Then status 200
    * def expectedResponse =
    """
{
  "entries": [ {
    "code": "1",
    "description": "Inactive"
  }, {
    "code": "3",
    "description": "Active"
  }, {
    "code": "4",
    "description": "Pending"
  }, {
    "code": "5",
    "description": "Shipped"
  }, {
    "code": "6",
    "description": "Held"
  }, {
    "code": "7",
    "description": "Not Supplied"
  }, {
    "code": "8",
    "description": "Unclaimed"
  }, {
    "code": "9",
    "description": "Expired"
  }, {
    "code": "16",
    "description": "Cancelled"
  }, {
    "code": "17",
    "description": "Out to patron"
  }, {
    "code": "18",
    "description": "Located"
  } ]
}
    """
    * match response contains expectedResponse


    Given path '/api/gates-edge/gates/holdstatus'
    And header Content-Type = 'application/json'
    And header Authorization = 'Bearer ' + accessToken
    And header iii-customer-domain = custDomain
    And header iii-customer-site-code = ilsSiteCode
    And header iii-customer-id = customerInfo.customerData.id
    And header Accept-Encoding =  '*'
    And header api-version = 1
    When method get
    Then status 200
    * def expectedResponse =
    """
{
  "entries": [ {
    "code": "1",
    "description": "Inactive"
  }, {
    "code": "3",
    "description": "Active"
  }, {
    "code": "4",
    "description": "Pending"
  }, {
    "code": "5",
    "description": "Shipped"
  }, {
    "code": "6",
    "description": "Held"
  }, {
    "code": "7",
    "description": "Not Supplied"
  }, {
    "code": "8",
    "description": "Unclaimed"
  }, {
    "code": "9",
    "description": "Expired"
  }, {
    "code": "16",
    "description": "Cancelled"
  }, {
    "code": "17",
    "description": "Out to patron"
  }, {
    "code": "18",
    "description": "Located"
  } ]
}
    """
    * match response contains expectedResponse


    Given path '/api/gates-edge/gates/holdstatus'
    And header Content-Type = 'application/json'
    And header Authorization = 'Bearer ' + accessToken
    And header iii-customer-domain = custDomain
    And header iii-customer-site-code = ilsSiteCode
    And header iii-customer-id = customerInfo.customerData.id
    And header Accept-Encoding =  '*'
    And header api-version = 1
    When method get
    Then status 200
    * def expectedResponse =
    """
{
  "entries": [ {
    "code": "1",
    "description": "Inactive"
  }, {
    "code": "3",
    "description": "Active"
  }, {
    "code": "4",
    "description": "Pending"
  }, {
    "code": "5",
    "description": "Shipped"
  }, {
    "code": "6",
    "description": "Held"
  }, {
    "code": "7",
    "description": "Not Supplied"
  }, {
    "code": "8",
    "description": "Unclaimed"
  }, {
    "code": "9",
    "description": "Expired"
  }, {
    "code": "16",
    "description": "Cancelled"
  }, {
    "code": "17",
    "description": "Out to patron"
  }, {
    "code": "18",
    "description": "Located"
  } ]
}
    """
    * match response contains expectedResponse


  Scenario: clean up after - make customer inactive
    * def results = karate.call('../customer/make_licences_inactive.feature',customerInfo)

