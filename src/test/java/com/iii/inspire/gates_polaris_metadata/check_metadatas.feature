Feature: Create customer on Customer API and assign connection and check hold status

  Background:

    * def now = function(){ return java.lang.System.currentTimeMillis() }

    * def envUrl = "https://ds-conv-bs-gates-polaris-pr-23.iii-inspire.dev"
    * def ilsSiteCode = "polaris" + now()
    * def ilsDescription = "Polaris Gates QA Test: Hold Status"

    * def custDomain = "admin.ds-conv-bs-gates-polaris-pr-23.iii-inspire.dev"

    * def customerSiteDetails = { url: '#(envUrl)', ilsSiteCode: '#(ilsSiteCode)', custdomain: '#(custDomain)',description: '#(ilsDescription)' }
    * def customerInfo = callonce read('../customer/create_polaris_customer_k8s_1.feature') customerSiteDetails

    * def envUrlAsJson = { url: '#(envUrl)'}
    * def keycloak = karate.call('../keycloak/keycloak_token.feature',envUrlAsJson)
    * def accessToken = keycloak.response.access_token

    * url envUrl


  Scenario: Get Hold Statuses in Polaris

    Given path '/api/gates-edge/gates/holdstatus'
    And header Content-Type = 'application/json'
    And header Authorization = 'Bearer ' + accessToken
    And header iii-customer-domain = custDomain
    And header iii-customer-site-code = ilsSiteCode
    And header iii-customer-id = customerInfo.customerData.id
    And header Accept-Encoding =  '*'
    And header api-version = 1
    When method get
    Then status 200
    * def expectedResponse =
    """
{
  "entries": [ {
    "code": "1",
    "description": "Inactive"
  }, {
    "code": "3",
    "description": "Active"
  }, {
    "code": "4",
    "description": "Pending"
  }, {
    "code": "5",
    "description": "Shipped"
  }, {
    "code": "6",
    "description": "Held"
  }, {
    "code": "7",
    "description": "Not Supplied"
  }, {
    "code": "8",
    "description": "Unclaimed"
  }, {
    "code": "9",
    "description": "Expired"
  }, {
    "code": "16",
    "description": "Cancelled"
  }, {
    "code": "17",
    "description": "Out to patron"
  }, {
    "code": "18",
    "description": "Located"
  } ]
}
    """
    * match response contains expectedResponse



  Scenario: Get Material types in Polaris
    Given path '/api/gates-edge/gates/materialtypes'
    And header Content-Type = 'application/json'
    And header Authorization = 'Bearer ' + accessToken
    And header iii-customer-domain = custDomain
    And header iii-customer-site-code = ilsSiteCode
    And header iii-customer-id = customerInfo.customerData.id
    And header Accept-Encoding =  '*'
    And header api-version = 1
    When method get
    Then status 200


  Scenario: Get Items Statuses in Polaris - happy path
    Given path  '/api/gates-edge/gates/itemstatus'
    And header Content-Type = 'application/json'
    And header Authorization = 'Bearer ' + accessToken
    And header iii-customer-domain = custDomain
    And header iii-customer-site-code = ilsSiteCode
    And header iii-customer-id = customerInfo.customerData.id
    And header Accept-Encoding =  '*'
    And header api-version = 1
    When method get
    Then status 200
    * def expectedResponse =
    """
  {
  "entries": [ {
  "code": "1",
  "description": "In"
  }, {
  "code": "2",
  "description": "Out"
  }, {
  "code": "3",
  "description": "Out-ILL"
  }, {
  "code": "4",
  "description": "Held"
  }, {
  "code": "5",
  "description": "Transferred"
  }, {
  "code": "6",
  "description": "In-Transit"
  }, {
  "code": "7",
  "description": "Lost"
  }, {
  "code": "8",
  "description": "Claim Returned"
  }, {
  "code": "9",
  "description": "Claim Never Had"
  }, {
  "code": "10",
  "description": "Missing"
  }, {
  "code": "11",
  "description": "Withdrawn"
  }, {
  "code": "12",
  "description": "Bindery"
  }, {
  "code": "13",
  "description": "On-Order"
  }, {
  "code": "14",
  "description": "In-Repair"
  }, {
  "code": "15",
  "description": "In-Process"
  }, {
  "code": "16",
  "description": "Unavailable"
  }, {
  "code": "17",
  "description": "Returned-ILL"
  }, {
  "code": "18",
  "description": "Routed"
  }, {
  "code": "19",
  "description": "Shelving"
  }, {
  "code": "20",
  "description": "Non-circulating"
  }, {
  "code": "21",
  "description": "Claim Missing Parts"
  }, {
  "code": "22",
  "description": "EContent External Loan"
  } ]
  }
    """
    * match response contains expectedResponse


  #Scenario: clean up after - make customer inactive
   # * def results = karate.call('../customer/make_licences_inactive.feature',customerInfo)

