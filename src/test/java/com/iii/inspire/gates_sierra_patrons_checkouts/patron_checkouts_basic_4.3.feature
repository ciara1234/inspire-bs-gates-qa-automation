Feature: Validate Patron checkouts for Sierra 4.3

  Background:
    * def now = function(){ return java.lang.System.currentTimeMillis() }

    * url envUrl

    * def ilsSiteCode = "patroncheckouts" + now()
    * def ilsDescription = "Gates QA Test: get patron checkouts"

    * def customerSiteDetails = { url: '#(envUrl)', ilsSiteCode: '#(ilsSiteCode)', custdomain: '#(custDomain)',description: '#(ilsDescription)' }
    * def customerInfo = callonce read('../customer/create_sierra_customer_k8s_1.feature') customerSiteDetails

    * def envUrlAsJson = { url: '#(envUrl)'}
    * def keycloak = karate.call('../keycloak/keycloak_token.feature', envUrlAsJson)
    * def accessToken = keycloak.response.access_token



  Scenario: Patron checkouts - basic - canBeRenewed field not there


    * def patronCheckoutStructure =
     """
    {
        checkoutId: '#string',
        patronId: '#string',
        itemId: '#string',
        barcode: '##string',
        dueDate: '##regex [0-9]{4}[-]{1}[0-9]{2}[-]{1}[0-9]{2}[T]{1}[0-9]{2}[:]{1}[0-9]{2}[:]{1}[0-9]{2}[Z]{1}',
        callNumber: '##string',
        recallDate: '##regex [0-9]{4}[-]{1}[0-9]{2}[-]{1}[0-9]{2}[T]{1}[0-9]{2}[:]{1}[0-9]{2}[:]{1}[0-9]{2}[Z]{1}',
        renewalCount: '#number',
        checkOutDate: '##regex [0-9]{4}[-]{1}[0-9]{2}[-]{1}[0-9]{2}[T]{1}[0-9]{2}[:]{1}[0-9]{2}[:]{1}[0-9]{2}[Z]{1}',
        recallDate: '##regex [0-9]{4}[-]{1}[0-9]{2}[-]{1}[0-9]{2}[T]{1}[0-9]{2}[:]{1}[0-9]{2}[:]{1}[0-9]{2}[Z]{1}'
    }
    """

    Given path '/api/gates-edge/gates/patrons/2183455/checkouts'
    And header Content-Type = 'application/json'
    And header Authorization = 'Bearer ' + accessToken
    And header iii-customer-domain = custDomain
    And header iii-customer-site-code = ilsSiteCode
    And header iii-customer-id = customerInfo.customerData.id
    And header Accept-Encoding =  '*'
    And header api-version = 1
    When method get
    Then status 200
    And match each response.checkouts contains patronCheckoutStructure

  Scenario: Patron checkouts - limit is 1 and offset 0


    Given path '/api/gates-edge/gates/patrons/2183455/checkouts'
    And header Content-Type = 'application/json'
    And header Authorization = 'Bearer ' + accessToken
    And header iii-customer-domain = custDomain
    And header iii-customer-site-code = ilsSiteCode
    And header iii-customer-id = customerInfo.customerData.id
    And header Accept-Encoding =  '*'
    And header api-version = 1
    And param limit = 1
    And param offset = 0
    When method get
    Then status 200
    * def responseBody = response
    And assert responseBody.total == 12
    And assert responseBody.start == 0
    And match $.checkouts.length() == 1

  Scenario: Patron checkouts - offset is 1

    Given path '/api/gates-edge/gates/patrons/2183455/checkouts'
    And header Content-Type = 'application/json'
    And header Authorization = 'Bearer ' + accessToken
    And header iii-customer-domain = custDomain
    And header iii-customer-site-code = ilsSiteCode
    And header iii-customer-id = customerInfo.customerData.id
    And header Accept-Encoding =  '*'
    And header api-version = 1
    And param offset = 1
    When method get
    Then status 200
    * def responseBody = response
    And assert responseBody.total == 12
    And assert responseBody.start == 1
    And match $.checkouts.length() == 11

  Scenario: Patron checkouts - offset is and limit is 1


    Given path '/api/gates-edge/gates/patrons/2183455/checkouts'
    And header Content-Type = 'application/json'
    And header Authorization = 'Bearer ' + accessToken
    And header iii-customer-domain = custDomain
    And header iii-customer-site-code = ilsSiteCode
    And header iii-customer-id = customerInfo.customerData.id
    And header Accept-Encoding =  '*'
    And header api-version = 1
    And param offset = 1
    And param limit = 1
    When method get
    Then status 200
    * def responseBody = response
    And assert responseBody.total == 12
    And assert responseBody.start == 1
    And match $.checkouts.length() == 1


    Scenario: clean up and archive customer
    * def results = karate.call('../customer/make_licences_inactive.feature',customerInfo)
