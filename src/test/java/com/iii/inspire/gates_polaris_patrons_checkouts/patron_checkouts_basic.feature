Feature: Validate Patron checkouts

  Background:
    * def now = function(){ return java.lang.System.currentTimeMillis() }

    * url envUrl

    * def ilsSiteCode = "patroncheckouts" + now()
    * def ilsDescription = "Gates QA Test: get patron checkouts on polaris"
    * def customerSiteDetails = { url: '#(envUrl)', ilsSiteCode: '#(ilsSiteCode)', custdomain: '#(custDomain)',description: '#(ilsDescription)' }
    * def customerInfo = callonce read('../customer/create_polaris_customer_k8s_1.feature') customerSiteDetails

    * def envUrlAsJson = { url: '#(envUrl)'}
    * def keycloak = karate.call('../keycloak/keycloak_token.feature', envUrlAsJson)
    * def accessToken = keycloak.response.access_token



  Scenario: I can get a Patron with checkouts and validate output structure



    * def patronCheckoutStructure =
     """
    {
        patronId: '#string',
        itemId: '#string',
        barcode: '##string',
        dueDate: '##regex [0-9]{4}[-]{1}[0-9]{2}[-]{1}[0-9]{2}[T]{1}[0-9]{2}[:]{1}[0-9]{2}[:]{1}[0-9]{2}[Z]{1}',
        callNumber: '##string',
        renewalCount: '#number',
        checkOutDate: '##regex [0-9]{4}[-]{1}[0-9]{2}[-]{1}[0-9]{2}[T]{1}[0-9]{2}[:]{1}[0-9]{2}[:]{1}[0-9]{2}[Z]{1}'
    }
    """


    Given path '/api/gates-edge/gates/patrons/1004300089725/checkouts'
    And header Content-Type = 'application/json'
    And header Authorization = 'Bearer ' + accessToken
    And header iii-customer-domain = custDomain
    And header iii-customer-site-code = ilsSiteCode
    And header iii-customer-id = customerInfo.customerData.id
    And header Accept-Encoding =  '*'
    And header api-version = 1
    When method get
    Then status 200
    And match response.checkouts contains patronCheckoutStructure


    Scenario: clean up and archive customer
    * def results = karate.call('../customer/make_licences_inactive.feature',customerInfo)
