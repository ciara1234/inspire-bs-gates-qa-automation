Feature: Create customer on Customer API and assign connection and check resource

  Background:

    * def now = function(){ return java.lang.System.currentTimeMillis() }

    * url envUrl

    * def ilsSiteCode = "patroncheckouts" + now()
    * def ilsDescription = "Gates QA Test: get patron checkouts on polaris"
    * def customerSiteDetails = { url: '#(envUrl)', ilsSiteCode: '#(ilsSiteCode)', custdomain: '#(custDomain)',description: '#(ilsDescription)' }
    * def customerInfo = callonce read('../customer/create_polaris_customer_k8s_1.feature') customerSiteDetails

    * def envUrlAsJson = { url: '#(envUrl)'}
    * def keycloak = karate.call('../keycloak/keycloak_token.feature', envUrlAsJson)
    * def accessToken = keycloak.response.access_token



  Scenario: 01 Get patron checkouts by patron Id: patron doesn't exist

    Given path '/api/gates-edge/gates/patrons/joesoap/checkouts'
    And header Content-Type = 'application/json'
    And header Authorization = 'Bearer ' + accessToken
    And header iii-customer-domain = custDomain
    And header iii-customer-site-code = ilsSiteCode
    And header iii-customer-id = customerInfo.customerData.id
    And header Accept-Encoding =  '*'
    And header api-version = 1
    When method get
    Then status 500
    * def responseBody = response
    And assert responseBody.message == "Error retrieving patron ID. "


  Scenario: 02 Get patron checkouts by patron Id: invalid characters - %00

    Given path '/api/gates-edge/gates/patrons/%00/checkouts'
    And header Content-Type = 'application/json'
    And header Authorization = 'Bearer ' + accessToken
    And header iii-customer-domain = custDomain
    And header iii-customer-site-code = ilsSiteCode
    And header iii-customer-id = customerInfo.customerData.id
    And header Accept-Encoding =  '*'
    And header api-version = 1
    When method get
    Then status 403
    * def responseBody = response
    And assert responseBody.message == "The request was rejected because the URL contained a potentially malicious String \"%25\""

  Scenario: 03 Get patron checkouts by patron Id: fake site code, bad format for customer id

    Given path '/api/gates-edge/gates/patrons/1004300089725/checkouts'
    And header Content-Type = 'application/json'
    And header Authorization = 'Bearer ' + accessToken
    And header iii-customer-domain = custDomain
    And header iii-customer-site-code = 'fakey'
    And header iii-customer-id = 'fakefake'
    And header Accept-Encoding =  '*'
    And header api-version = 1
    When method get
    Then status 400
    * def responseBody = response
    And assert responseBody.message == "Invalid Customer ID format: fakefake"

  Scenario: 04 Get patron checkouts by patron Id: good customer id format, not a customer

    Given path '/api/gates-edge/gates/patrons/1004300089725/checkouts'
    And header Content-Type = 'application/json'
    And header Authorization = 'Bearer ' + accessToken
    And header iii-customer-domain = custDomain
    And header iii-customer-site-code = 'fakey'
    And header iii-customer-id = '6343a48a-dead-c0de-d00d-0fbccb7a3b2f'
    And header Accept-Encoding =  '*'
    And header api-version = 1
    When method get
    Then status 400
    * def responseBody = response
    And assert responseBody.message == "Customer '6343a48a-dead-c0de-d00d-0fbccb7a3b2f' doesn't exist"

  Scenario: 05 Get patron checkouts by patron Id : good customer,no api header

    Given path '/api/gates-edge/gates/patrons/1004300089725/checkouts'
    And header Content-Type = 'application/json'
    And header Authorization = 'Bearer ' + accessToken
    And header iii-customer-domain = custDomain
    And header iii-customer-site-code = ilsSiteCode
    And header iii-customer-id = customerInfo.customerData.id
    And header Accept-Encoding =  '*'
   # And header api-version = 1
    When method get
    Then status 404
    * def responseBody = response
    And assert responseBody.error == "Not Found"

  Scenario: 06 Get patron checkouts by patron Id: good customer, bad api header

    Given path '/api/gates-edge/gates/patrons/1004300089725/checkouts'
    And header Content-Type = 'application/json'
    And header Authorization = 'Bearer ' + accessToken
    And header iii-customer-domain = custDomain
    And header iii-customer-site-code = ilsSiteCode
    And header iii-customer-id = customerInfo.customerData.id
    And header Accept-Encoding =  '*'
    And header api-version = 27
    When method get
    Then status 404
    * def responseBody = response
      And assert responseBody.error == "Not Found"

  Scenario: Clean up after tests - Make customer inactive

    * def results = karate.call('../customer/make_licences_inactive.feature',customerInfo)
