Feature: Create customer on Customer API and assign connection and check connection test

  Background:

    * def now = function(){ return java.lang.System.currentTimeMillis() }

    * def envUrl = "https://ds-conv-bs-gates-polaris-pr-19.iii-inspire.dev"
    * def ilsSiteCode = "polaris" + now()
    * def ilsDescription = "Sierra Gates QA Test: connection test"

    * def custDomain = "admin.ds-conv-bs-gates-polaris-pr-19.iii-inspire.dev"

    * def customerSiteDetails = { url: '#(envUrl)', ilsSiteCode: '#(ilsSiteCode)', custdomain: '#(custDomain)',description: '#(ilsDescription)' }
    * def customerInfo = callonce read('../customer/create_sierra_customer_k8s_1.feature') customerSiteDetails

    * def envUrlAsJson = { url: '#(envUrl)'}
    * def keycloak = karate.call('../keycloak/keycloak_token.feature', envUrlAsJson)
    * def accessToken = keycloak.response.access_token

    * url envUrl


  Scenario: Connection test - valid details

    * def body =
    """
      {
        "type": "sierra",
        "apiUrl": "https://devops-8868-app.iii-lab.eu:443/iii/sierra-api",
        "clientId": "SRuu5FEMfqHxrl6hfR4FxHRG41me",
        "clientSecret": "goodsecret",
      }
    """

    Given path '/api/gates-edge/gates/connection/test'
    And header Content-Type = 'application/json'
    And header Authorization = 'Bearer ' + accessToken
    And header iii-customer-domain = custDomain
   # And header iii-customer-site-code = ilsSiteCode
  #  And header iii-customer-id = customerInfo.customerData.id
    And header Accept-Encoding =  '*'
    And header api-version = 1
    And request body
    When method post
    Then status 204


  Scenario: Connection test - bad secret

    * def body =
    """
      {
        "type": "sierra",
        "apiUrl": "https://devops-8868-app.iii-lab.eu:443/iii/sierra-api",
        "clientId": "SRuu5FEMfqHxrl6hfR4FxHRG41me",
        "clientSecret": "NOTgoodsecret",
      }
    """

    Given path '/api/gates-edge/gates/connection/test'
    And header Content-Type = 'application/json'
    And header Authorization = 'Bearer ' + accessToken
    And header iii-customer-domain = custDomain
    And header iii-customer-site-code = ilsSiteCode
    And header iii-customer-id = customerInfo.customerData.id
    And header Accept-Encoding =  '*'
    And header api-version = 1
    And request body
    When method post
    Then status 401



  Scenario: Make customer inactive

    * def results = karate.call('../customer/make_licences_inactive.feature',res)
