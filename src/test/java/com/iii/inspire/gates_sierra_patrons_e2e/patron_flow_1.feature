Feature: Create customer on Customer API and assign connection and simulate patron flow
  Background:
    * def now = function(){ return java.lang.System.currentTimeMillis() }

    * def envUrl = "https://ds-conv-is-gates-edge-pr-36.iii-inspire.dev"
    * def ilsSiteCode = "polaris" + now()
    * def ilsDescription = "Gates QA - Sierra: Patron E2E Flow"

    * def custDomain = "admin.ds-conv-is-gates-edge-pr-36.iii-inspire.dev"

    * def customerSiteDetails = { url: '#(envUrl)', ilsSiteCode: '#(ilsSiteCode)', custdomain: '#(custDomain)',description: '#(ilsDescription)' }
    * def customerInfo = callonce read('../customer/create_sierra_6916_customer_k8s_1.feature') customerSiteDetails

    * def envUrlAsJson = { url: '#(envUrl)'}
    * def keycloak = karate.call('../keycloak/keycloak_token.feature', envUrlAsJson)
    * def accessToken = keycloak.response.access_token

    * url envUrl

  Scenario: E2E Flow: Patron Validate, Get Patron Data, and then get list of Pickuplocations, Then Get Holds, and then try to freeze holds

    Given path '/api/gates-edge/gates/patrons/validate'
    And header Content-Type = 'application/json'
    And header Authorization = 'Bearer ' + accessToken
    And header iii-customer-domain = custDomain
    And header iii-customer-site-code = ilsSiteCode
    And header iii-customer-id = customerInfo.customerData.id
    And header Accept-Encoding =  '*'
    And header api-version = 1
    And request
  """
  {
  barcode:"tomcruise",
  pin:"1234"
  }
  """
    When method post
    Then status 200
    And match response ==
  """
  {
  "valid": true,
  "patronId": "2183607"
  }
  """

    * def patronId = response.patronId

    * def patronStructure =
  """
    {
        id: '#number',
        barcodes: '##array',
        patronName: '##array',
        emailAddress: '##array',
        birthDate: '#regex [0-9]{4}[-]{1}[0-9]{2}[-]{1}[0-9]{2}',
        homeLibraryCode: '##string',
        messages:'##array',
        blocks: '##array',
        address: '##array',
        phoneNumber: '##array',
        expirationDate: '#regex [0-9]{4}[-]{1}[0-9]{2}[-]{1}[0-9]{2}',
        moneyOwed: '#number'
    }
    """

    Given path '/api/gates-edge/gates/patrons/' + patronId
    And header Content-Type = 'application/json'
    And header Authorization = 'Bearer ' + accessToken
    And header iii-customer-domain = custDomain
    And header iii-customer-site-code = ilsSiteCode
    And header iii-customer-id = customerInfo.customerData.id
    And header Accept-Encoding =  '*'
    And header api-version = 1
    When method get
    Then status 200

    #* def patronId = response.id
    * def patronHomeLibraryCode = response.homeLibraryCode

    Given path '/api/gates-edge/gates/patrons/' + patronId + '/holds/request-form'
    And header Content-Type = 'application/json'
    And header Authorization = 'Bearer ' + accessToken
    And header iii-customer-domain = custDomain
    And header iii-customer-site-code = ilsSiteCode
    And header iii-customer-id = customerInfo.customerData.id
    And header Accept-Encoding =  '*'
    And header api-version = 2
    And request {"homeLibraryCode": '#(patronHomeLibraryCode)',"recordNumber" : 1204955,"recordType" : "b"}
    When method post
    Then status 200

    * def patronHoldsStructure =
     """
    {

       holdRequestId : '#string',
       recordId : '#string',
       recordType : '#string',
       activationDate : '##string',
       neededBy : '##string',
       location : '#string',
       expirationDate :'##string',
       status : '#string',
       frozen : '#boolean',
       canBeFrozen : '#boolean',
       priority : '#number',
       priorityQueueLength: '#number',
       note: '##string'

       }
    """

    Given path '/api/gates-edge/gates/patrons/' + patronId + '/holds'
    And header Content-Type = 'application/json'
    And header Authorization = 'Bearer ' + accessToken
    And header iii-customer-domain = custDomain
    And header iii-customer-site-code = ilsSiteCode
    And header iii-customer-id = customerInfo.customerData.id
    And header Accept-Encoding =  '*'
    And header api-version = 1
    When method get
    Then status 200
    And match each response.holds contains patronHoldsStructure
    * def responseBody = response
    And assert responseBody.total == 2
    And assert responseBody.start == 0

    * def holdId1 = response.holds[0].holdRequestId
    * def holdId2 = response.holds[1].holdRequestId

    Given path '/api/gates-edge/gates/patrons/holds/'+ holdId1
    And header Content-Type = 'application/json'
    And header Authorization = 'Bearer ' + accessToken
    And header iii-customer-domain = custDomain
    And header iii-customer-site-code = ilsSiteCode
    And header iii-customer-id = customerInfo.customerData.id
    And header Accept-Encoding =  '*'
    And header api-version = 1
    And request {"pickupLocation": "bd","freeze":true}
    When method put
    Then status 403
    And match response.message == "WebPAC Error : This hold can not be frozen."


    Given path '/api/gates-edge/gates/patrons/holds/'+ holdId2
    And header Content-Type = 'application/json'
    And header Authorization = 'Bearer ' + accessToken
    And header iii-customer-domain = custDomain
    And header iii-customer-site-code = ilsSiteCode
    And header iii-customer-id = customerInfo.customerData.id
    And header Accept-Encoding =  '*'
    And header api-version = 1
    And request {"pickupLocation": "bd","freeze":true}
    When method put
    Then status 403
    And match response.message == "WebPAC Error : This hold can not be frozen."




  Scenario: clean up after - make customer inactive after tests

    * def results = karate.call('../customer/make_licences_inactive.feature',customerInfo)

