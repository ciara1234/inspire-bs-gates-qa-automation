Feature: Create customer on Customer API and assign connection and check bib resource count

  Background:

    * def now = function(){ return java.lang.System.currentTimeMillis() }

    * url envUrl


    * def ilsSiteCode = "getit"
    #* def ilsSiteCode = "rabbit"
   * def ilsDescription = "Ģ̸̡̝̖̮̰͓͉̩̭̫͚̜̜͍̙̱̠̪͚̞̟͎̘̱̫̥͚̬̝͇̝̠̜̹͇̻̫̺̫̪̫̙̟̯̼̞̻̤̩̰̾̎͌̈́̆̀͒̀̆͊̀́̒̏̇̎̌̽̊͗͜͜͜͜͜͠ͅͅͅͅȀ̴̢̧̢̡̨̨̡̧̢̛̗̻̤̳̳̱͔̦̗̱̳͎̪͔̱̱̮͖̬̭̫͉̮̻̳̱̼̻̤̪̝̺̞̩̤̹̞͉͓̗͓̯̻̙̬̠̟͈̑̽̅̐̍̊̑̌͋͆̈́̍͐̀̽̃̀̈́̉̔̌̂̇̎̈́̀͐̓̀̉̔̊̽͂́̽̇̐̂̂́͆̀̇̒͗͛͗̏͛̊͗̀͒͐̽̈̅̈́͌͘̕̚̚͜T̸̡̛̛̛̛̮͈͕̬̝͍͓͙͎̭̬̻̹̼̜͓̲̗̳̱͎̘̟̯̞̭̰̯̰̯͎̭̄̓̅̆͗͂̃̓͌̽͒̓̌͛͋͋̇̾̂̎̈́͗͂́͆̋̆͌̅͆̂͒̽̄̿́͋̿̏͆̿͛̐͊̓̍̄̑́͋͂͊̂͛͒̚̚̚̚͜͝͠͝͝͝͝͠E̴̢͎̯̬͇̼͓̲̤̠̝̮̙͙̮̫̠͕̬̞͔̞̙̝̗̥̞͈̩͙̩̠̹̮͚̟̯̫̟̥͉͛̇̌͐̎͑̂͐̇͋́́̌̏͆͆͊̋̈͂͊̈̈́̂̀̈́̋͌́̆̈́̐̄̅̌͊̊̃͌̌̆͊͒̂̐̚͘̚͝͝͝S̷̨̡̢̢̛̛̺̮̱͖͙̪̝͓͙̩̪͔̰͈̭̖̥̯̬̫̪̤͂͂̏͌̆̽̄͛́̃͆̈́̑͆̏́̌͛̋͐̑̆͛̕͜͜͜͝ ̶̡̡͈͙̲̥̲̦̹̗̳͖̯̼̟̣̝̞͕̙͔̥͈̱̮̟̝̖̲̮͔̼̠̥̲͂͂̾͋̏́̑́̌̓̏̒͛͐̂̋̓͛͐͐̏͋̕͘̚͜͝͝͠͠ͅT̸̡̧̧̛̛̻͔̺̯̟̭͉̠̙̞̘̦͓̼̤͔͓̼̟̱͍̭̳̲̙͚̠͍͎̳̺̩͍̯̼̮͚͎̯͕̹̻̼̞̳̦͙̠̭̗͐͂̄̑̈͌̇̌̈̏̇͋͛͋̊͛̓̀͌͒̇̋̇̓̉͆̆̆͑̐̿̾͒͋̓̊̅́̈́͑̉͊͊̐͋͋̓̉̅̏̚͜͜͝͝ͅḜ̸̢̡̰̫̗͉͕̹̬͉̼̖͈̞͔̞͙̪̬̱̹̻̙̫̳̯̣̼̱̮̬͎̩̣̣̹͖̬͕̩͕̳͇̥̺̌̍̒͋̆̚͠ͅS̵̨̱̫̘̘̳̥̰̟̹̜̫̰̰̞̆̀͑͌̑́̃͐͗́͂̅͐̌͐̃̈́͒͆̓͒̒̓͆͑̈͂̒͋̆̊̈͌̐̆̊̀́̾̀́̓̒̎̒͘Ţ̷̛̛̪̰̦͓̘̠̗̣̣͚͉̙̲̥̪͚͍͉̮̝̫̗̟̰̰͚̟̤̭͔̰̲̝̱͙̬̮̱͍̖͋́̐͒͐͌͗̆̑̂̽͋̀̿̈́͑́̓̈́̊̌̓̿̅͂͌̽̃̇̍͋̃͌̀́͑̃͗́̽͊̌̍̾̎̿̈́͛̀̅̐͘̚͘̚̕͠ ̷̧̨̢̱̭̟͈͕̤̯̠͙͗̿́̾͛̈́͆͗͑͗͋̆̍͌̑͐̑͊̆̃̂̈̌͊̂̂̌́͑͆̋̿͑́͆̈͐̆̿̋̈́̚̚̕͠͝͝Ç̴̢̡̛̛̻̥̹̜͙͕͙̼̩͈͈̙͎̮̺̥̗͎̻̳̤̭̮̲̤̫̯̫̈́͗̄͛́͗͋͂̈́̄̍̂̌̏̆͛̃́͒͊́̃̓͑̆͂̃̌̇̈͂́̔̅̈̍̈́́̽͐̑̑̇̃̓͛͛͐͛͊̄̀̅̏̐͒̕̕͘͜͝͝͠ͅƯ̴̢̧̨̨̥̭̘͉͇̬̘̻̝͓͈̟͖̺̜͓̖͎̣̱̟̫͕̦̟͙̫̳̫̆͗͊͋͐̃͂̍́̊͆̈́̋̃̒͋̈́̈́͘͠͝ͅŞ̶̧̨̟͕̪͈͈̗̣̤̖̦̮̩̟͈̰̤̞̪̠̦̙͇̙̼̝̯̬͕̰̼̣͖̻̱̜͕͍̹͚͖͓͎̺̭̳̾͐͒͐̓̓͜͝ͅͅT̷̢̨̲͉͇̜͎̺̰͓̮̜̺̘̭̥͖̱̦̼̭̻̥̩͔͇̗͚̻̗̣̹̙̪̳̖͔̳̟̃̓͆̿̾̉͂͊̾̅̊̈́͊̎̇͌̎̐̔͗̀̀̿́̇̿̓́̌̈́̍̔̀̑̀͐̅̐̕͠͝͝͝͝ͅǪ̵̢̛̥͓̲̬̙̺͍̭͔͚̬̓͐̌͗̉̂̊̔͋̉̃̕͘M̴̧̢̨̧̨̨̛̛̛̛̬̮͔͇͚͕̮̲͈͙̖̝̪͖̼͇̼̭͕̞̤̰͉̝͓̪̹̼̫͕͓͎̪̳̣͙̯̭̪̮̫̜̰̺̜̤̼̹̲̮̳̤͕̩̱̘̯̭̓̀̇́̔̏̀̒̊̆̑̋͌̉̔̔̐̔̌́̇̑͆͂̄̈̽̾͊̂͛̒̌͂̃́̍̋͐̉̇̈́̈́͌͑̈̅͌̎̿̕͘̕͜͜͜͠͝͠͝ͅͅE̶̫͙̝͇̜̞̭͈̺̪̠̙͉̝͋̚R̷̨̛̘͇̙̥̞̰̯̲̦̠͈͙̩͔̘̞̗̰̫̈́͒͋̅̑̐͑́̊͌̔̾̊̒̀͋́̌̽͒̓̈́̓͆̂͋̀́̄̏̊̇͘͝͝"
    #* def ilsDescription = "test 1"


    #* def custDomain = "admin.staging.iii-conv.com"

    * def customerSiteDetails = { url: '#(envUrl)', ilsSiteCode: '#(ilsSiteCode)', custdomain: '#(custDomain)',description: '#(ilsDescription)' }
    * def customerInfo = callonce read('../customer/create_sierra_customer_k8s_1.feature') customerSiteDetails

    * def envUrlAsJson = { url: '#(envUrl)'}
    * def keycloak = karate.call('../keycloak/keycloak_token.feature', envUrlAsJson)
    * def accessToken = keycloak.response.access_token



  Scenario: 01 Get bib resources by Id in Sierra : fake site code, bad format for customer id

    Given path '/api/gates-edge/gates/resource-count'
    And header Content-Type = 'application/json'
    And header Authorization = 'Bearer ' + accessToken
    And header iii-customer-domain = custDomain
    And header iii-customer-site-code = 'fakey'
    And header iii-customer-id = 'fakefake'
    And header Accept-Encoding =  '*'
    And header api-version = 1
    When method get
    Then status 400
    * def responseBody = response
    And assert responseBody.message == "Invalid Customer ID format: fakefake"

  Scenario: 02 Get bib resources by Id in Sierra: good id format, not a customer


    Given path '/api/gates-edge/gates/resource-count'
    And header Content-Type = 'application/json'
    And header Authorization = 'Bearer ' + accessToken
    And header iii-customer-domain = custDomain
    And header iii-customer-site-code = 'fakey'
    And header iii-customer-id = '6343a48a-dead-c0de-d00d-0fbccb7a3b2f'
    And header Accept-Encoding =  '*'
    And header api-version = 1
    And param bibIds = "1000008"
    When method get
    Then status 400
    * def responseBody = response
    And assert responseBody.message == "Customer '6343a48a-dead-c0de-d00d-0fbccb7a3b2f' doesn't exist"

  Scenario: 03 Get bib resources by Id in Sierra : good customer,no api header

    Given path '/api/gates-edge/gates/resource-count'
    And header Content-Type = 'application/json'
    And header Authorization = 'Bearer ' + accessToken
    And header iii-customer-domain = custDomain
    And header iii-customer-site-code = ilsSiteCode
    And header iii-customer-id = customerInfo.customerData.id
    And header Accept-Encoding =  '*'
    #And header api-version = 1
    And param bibIds = "1000008"
    When method get
    Then status 404

  Scenario: 04 Get bib resources by Id in Sierra: good customer, bad api header

    Given path '/api/gates-edge/gates/resource-count'
    And header Content-Type = 'application/json'
    And header Authorization = 'Bearer ' + accessToken
    And header iii-customer-domain = custDomain
    And header iii-customer-site-code = ilsSiteCode
    And header iii-customer-id = customerInfo.customerData.id
    And header Accept-Encoding =  '*'
    And header api-version = 12
    And param bibIds = "1000008"
    When method get
    Then status 404
#  Scenario: After tests - Make customer Archived

 #  * def results = karate.call('../customer/make_licences_inactive.feature',customerInfo)
