Feature: Create customer on Customer API on Staging and run a request

  Background:
    * def now = function(){ return java.lang.System.currentTimeMillis() }

    * def envUrl = "prodlite.iii-conv.com"
    * def ilsSiteCode = "gates" + now()
    * def description = "prodlite: zero carbs"

    * def custDomain = "admin.prodlite.iii-conv.com"
    * def json = { url: '#(envUrl)', ilsSiteCode: '#(ilsSiteCode)', custdomain: '#(custDomain)',description: '#(description)' }

    #* def res = callonce read('../customer/create_sierra_customer_k8s_1.feature') json
    #* def res = callonce read('../customer/create_sierra_customer_k8s_1.feature') json
    * def res = callonce read('../customer/create_sierra_6916_customer_k8s_1.feature') json

    * def json1 = { url: '#(envUrl)'}
    * def kc = karate.call('../keycloak/keycloak_token.feature',json1)
    * def access_token = kc.response.access_token


  Scenario: Get Resources

    Given url 'https://' + envUrl + '/api/gates-edge/gates/resource/1000094'
    And header Content-Type = 'application/json'
    And header Authorization = 'Bearer ' + access_token
    And header iii-customer-domain = custDomain
    And header iii-customer-site-code = ilsSiteCode
    And header iii-customer-id = res.customerData.id
    And header Accept-Encoding =  '*'
    And header api-version = 1
    When method get
    Then status 200


#  Scenario: Clean up after tests - Make customer inactive

    #* def results = karate.call('../customer/make_licences_inactive.feature',res)
