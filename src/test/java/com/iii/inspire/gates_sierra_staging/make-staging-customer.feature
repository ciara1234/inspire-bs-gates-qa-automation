Feature: Create customer on Customer API on Staging and run some requests - to help AppDynamics stuff

  Background:
    * def now = function(){ return java.lang.System.currentTimeMillis() }

    * def envUrl = "staging.iii-conv.com"
    * def ilsSiteCode = "stagingappdynamics" + now()
    * def description = "aaaAppDynamics Customer"

    * def custDomain = "admin.staging.iii-conv.com"
    * def json = { url: '#(envUrl)', ilsSiteCode: '#(ilsSiteCode)', custdomain: '#(custDomain)',description: '#(description)' }

    #* def res = callonce read('../customer/create_sierra_customer_k8s_1.feature') json
    * def res = callonce read('../customer/create_sierra_customer_k8s_1.feature') json

    * def json1 = { url: '#(envUrl)'}
    * def kc = karate.call('../keycloak/keycloak_token.feature',json1)
    * def access_token = kc.response.access_token



  Scenario: Get Hold Statuses

    Given url 'https://' + envUrl + '/api/gates-edge/gates/holdstatus'
    And header Content-Type = 'application/json'
    And header Authorization = 'Bearer ' + access_token
    And header iii-customer-domain = custDomain
    And header iii-customer-site-code = ilsSiteCode
    And header iii-customer-id = res.customerData.id
    And header Accept-Encoding =  '*'
    And header api-version = 1
    When method get
    Then status 200

  Scenario: Get Resources

    Given url 'https://' + envUrl + '/api/gates-edge/gates/resource/1000094'
    And header Content-Type = 'application/json'
    And header Authorization = 'Bearer ' + access_token
    And header iii-customer-domain = custDomain
    And header iii-customer-site-code = ilsSiteCode
    And header iii-customer-id = res.customerData.id
    And header Accept-Encoding =  '*'
    And header api-version = 1
    When method get
    Then status 200

  Scenario: patron hold form

    Given url 'https://' + envUrl + '/api/gates-edge/gates/patrons/2183680/holds/request-form'
    And header Content-Type = 'application/json'
    And header Authorization = 'Bearer ' + access_token
    And header iii-customer-domain = custDomain
    And header iii-customer-site-code = ilsSiteCode
    And header iii-customer-id = res.customerData.id
    And header Accept-Encoding =  '*'
    And header api-version = 1
    And request {"recordNumber": 1204955,"recordType":"b"}
    When method post
    * def uploadStatusCode = responseStatus
    Then assert responseStatus == 200 || responseStatus == 404


  Scenario: Renew a checkout

    Given url 'https://' + envUrl + '/api/gates-edge/gates/patrons/6798/checkouts'
    And header Content-Type = 'application/json'
    And header Authorization = 'Bearer ' + access_token
    And header iii-customer-domain = custDomain
    And header iii-customer-site-code = ilsSiteCode
    And header iii-customer-id = res.customerData.id
    And header Accept-Encoding =  '*'
    And header api-version = 1
    And request {}
    When method post
    * def uploadStatusCode = responseStatus
    Then assert responseStatus == 200 || responseStatus == 404


  Scenario: patron PUT hold

    Given url 'https://' + envUrl + '/api/gates-edge/gates/patrons/2183680/holds/4585'
    And header Content-Type = 'application/json'
    And header Authorization = 'Bearer ' + access_token
    And header iii-customer-domain = custDomain
    And header iii-customer-site-code = ilsSiteCode
    And header iii-customer-id = res.customerData.id
    And header Accept-Encoding =  '*'
    And header api-version = 1
    And request {"pickupLocation": "nb","freeze":false}
    When method put
    * def uploadStatusCode = responseStatus
    Then assert responseStatus == 200 || responseStatus == 404

  Scenario: patron delete hold

    Given url 'https://' + envUrl + '/api/gates-edge/gates/patrons/2183680/holds/4585'
    And header Content-Type = 'application/json'
    And header Authorization = 'Bearer ' + access_token
    And header iii-customer-domain = custDomain
    And header iii-customer-site-code = ilsSiteCode
    And header iii-customer-id = res.customerData.id
    And header Accept-Encoding =  '*'
    And header api-version = 1
    When method delete
    * def uploadStatusCode = responseStatus
    Then assert responseStatus == 200 || responseStatus == 404


  Scenario: Clean up after tests - Make customer inactive

    * def results = karate.call('../customer/make_licences_inactive.feature',res)
