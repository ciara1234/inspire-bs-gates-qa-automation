Feature: Get a patron from Polaris

  Background:

    Scenario: test function

    * def getDate =
    """
      function() {
        var SimpleDateFormat = Java.type('java.text.SimpleDateFormat');
        var sdf = new SimpleDateFormat('yyyy/MM/dd');
        var date = new java.util.Date();
        return sdf.format(date);
    }
    """

    * def temp = getDate()
    * print temp

    Scenario: Do a simple test on polaris

      * def preWork =
      """
      function(){
        var accessKey = "4B3C1D34-7BA6-4D5E-8112-1C9A6BB7CD74"
        var accessID = "polarisdev"
        var httpMethod = "GET"
        var uri = "https://rd-polaris.polarislibrary.com/PAPIService/REST/public/v1/1033/500/1/bib/1163687"

        var httpDate = (new Date()).toUTCString();
        var patronPassword = ""
        var message = httpMethod + uri + httpDate + patronPassword;
        karate.log("Using message as " + message)

        var hash = CryptoJS.HmacSHA1(message, accessKey);
        var hashInBase64 = CryptoJS.enc.Base64.stringify(hash);
        karate.log("hash = " + hashInBase64)


      }
      """
      * def temp = preWork()
      * print temp
