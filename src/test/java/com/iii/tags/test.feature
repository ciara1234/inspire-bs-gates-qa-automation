Feature: Helper feature

  Scenario:

    * def data =
  """
{
  "application": {
    "name": "GATES-CONNECTOR",
    "instance": [
      {
        "instanceId": "7efda4ef9e15:gates-connector:8810",
        "hostName": "172.21.0.10",
        "app": "GATES-CONNECTOR",
        "ipAddr": "172.21.0.10",
        "status": "UP",
        "overriddenStatus": "UNKNOWN",
        "port": {
          "$": 8810,
          "@enabled": "true"
        },
        "securePort": {
          "$": 443,
          "@enabled": "false"
        },
        "countryId": 1,
        "dataCenterInfo": {
          "@class": "com.netflix.appinfo.InstanceInfo$DefaultDataCenterInfo",
          "name": "MyOwn"
        },
        "leaseInfo": {
          "renewalIntervalInSecs": 3,
          "durationInSecs": 4,
          "registrationTimestamp": 1561112640864,
          "lastRenewalTimestamp": 1561112776910,
          "evictionTimestamp": 0,
          "serviceUpTimestamp": 1561112640722
        },
        "metadata": {
          "management.port": "8810"
        },
        "homePageUrl": "http://172.21.0.10:8810/",
        "statusPageUrl": "http://172.21.0.10:8810/actuator/info",
        "healthCheckUrl": "http://172.21.0.10:8810/actuator/health",
        "vipAddress": "gates-connector",
        "secureVipAddress": "gates-connector",
        "isCoordinatingDiscoveryServer": "false",
        "lastUpdatedTimestamp": "1561112640864",
        "lastDirtyTimestamp": "1561112640682",
        "actionType": "ADDED"
      },
      {
        "instanceId": "8be4b6f28d93:gates-connector:8809",
        "hostName": "172.21.0.9",
        "app": "GATES-CONNECTOR",
        "ipAddr": "172.21.0.9",
        "status": "UP",
        "overriddenStatus": "UNKNOWN",
        "port": {
          "$": 8809,
          "@enabled": "true"
        },
        "securePort": {
          "$": 443,
          "@enabled": "false"
        },
        "countryId": 1,
        "dataCenterInfo": {
          "@class": "com.netflix.appinfo.InstanceInfo$DefaultDataCenterInfo",
          "name": "MyOwn"
        },
        "leaseInfo": {
          "renewalIntervalInSecs": 3,
          "durationInSecs": 4,
          "registrationTimestamp": 1561112758079,
          "lastRenewalTimestamp": 1561112776596,
          "evictionTimestamp": 0,
          "serviceUpTimestamp": 1561112613362
        },
        "metadata": {
          "ILS-SITE-CODE": "ILS-SITE_1",
          "management.port": "8809"
        },
        "homePageUrl": "http://172.21.0.9:8809/",
        "statusPageUrl": "http://172.21.0.9:8809/actuator/info",
        "healthCheckUrl": "http://172.21.0.9:8809/actuator/health",
        "vipAddress": "gates-connector",
        "secureVipAddress": "gates-connector",
        "isCoordinatingDiscoveryServer": "false",
        "lastUpdatedTimestamp": "1561112758079",
        "lastDirtyTimestamp": "1561112757564",
        "actionType": "ADDED"
      }
    ]
  }
}
  """
    * match data $.application.instance.[1].metadata.ILS-SITE-CODE == "ILS-SITE_1"
