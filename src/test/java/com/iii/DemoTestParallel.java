package com.iii;

import com.intuit.karate.KarateOptions;
import com.intuit.karate.Results;
import com.intuit.karate.Runner;
import com.intuit.karate.cucumber.CucumberRunner;
import com.intuit.karate.cucumber.KarateStats;
import cucumber.api.CucumberOptions;
import java.io.File;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import net.masterthought.cucumber.Configuration;
import net.masterthought.cucumber.ReportBuilder;
import org.apache.commons.io.FileUtils;
import static org.junit.Assert.*;
import org.junit.BeforeClass;
import org.junit.Test;
//file:///C:/development/repos/inspire-bs-gates-qa-automation/target/cucumber-html-reports/report-feature_com-iii-sapi-gates_role-create_gates_api_key-feature.html

@CucumberOptions(tags = "@gateskey") // important: do not use @RunWith(Karate.class) !
public class DemoTestParallel {

  @BeforeClass
  public static void beforeClass() throws Exception {
   // TestBase.beforeClass();
    System.setProperty("karate.env", "test"); // ensure reset if other tests (e.g. mock) had set env in CI
  }


  @Test
  public void testAll()  {

    String karateOutputPath = Paths.get("build","surefire-reports").toString();
    System.out.println("karateOutputPath" + karateOutputPath);
    KarateStats stats = CucumberRunner.parallel(getClass(), 1, karateOutputPath);
    String msg = String.format("There are %d scenario failures.\nFull report: %s",
        stats.getFailCount(),
        "inspire-bs-gates-qa-automation/target/cucumber-html-reports/overview-features.html");

    assertTrue(msg, stats.getFailCount() == 0);
  }



/** @Test
 public void testParallel() {

   Results results = Runner.parallel(getClass(), 1);
   generateReport(results.getReportDir());
   assertTrue(results.getErrorMessages(), results.getFailCount() == 0);
 }

 public static void generateReport(String karateOutputPath) {
   Collection<File> jsonFiles = FileUtils.listFiles(new File(karateOutputPath), new String[] {"json"}, true);
   List<String> jsonPaths = new ArrayList(jsonFiles.size());
   jsonFiles.forEach(file -> jsonPaths.add(file.getAbsolutePath()));
   Configuration config = new Configuration(new File("target"), "demo");
   ReportBuilder reportBuilder = new ReportBuilder(jsonPaths, config);
   reportBuilder.generateReports();
 }
*/
}
