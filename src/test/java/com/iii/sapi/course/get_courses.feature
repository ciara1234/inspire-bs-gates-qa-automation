@sapifines
Feature: Get some courses
  Background:
    * def wait = read('classpath:wait-util.js')
    * def sapi = karate.call('../util/get_sapi_token.feature')
    * call wait 2

  Scenario: Try to get some courses from SAPI
    Given url sapi_url + '/v5/'+ 'courses/?limit=1&offset=1'
    And header Content-Type = 'application/json'
    * header Authorization = 'Bearer ' + sapi.response.access_token
    When method get
    Then status 200
