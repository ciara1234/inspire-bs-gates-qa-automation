@sapifines
Feature: Get some fines
  Background:
    * def wait = read('classpath:wait-util.js')
    * def sapi = karate.call('../util/get_sapi_token.feature')
    * call wait 2

  Scenario: Try to get some fines from SAPI
    Given url sapi_url + '/v5/'+ 'fines/?limit=2000'
    And header Content-Type = 'application/json'
    * header Authorization = 'Bearer ' + sapi.response.access_token
    When method get
    Then status 200

  Scenario: Put on a fine for a patron
    Given url sapi_url + '/v5/'+ 'patrons/2183481/fines/charge'
    And header Content-Type = 'application/json'
    * header Authorization = 'Bearer ' + sapi.response.access_token
    And request  { "amount": 299,"reason": "Testing reason", "location": "earch" }
    When method post
    Then status 204

  Scenario: Get fines for a patron
    Given url sapi_url + '/v5/'+ 'patrons/2183481/fines?fields=id,item,assessedDate,datePaid,description,location,chargeType,itemCharge,processingFee,billingFee,paidAmount'
    And header Content-Type = 'application/json'
    * header Authorization = 'Bearer ' + sapi.response.access_token
    When method get
    Then status 200
