@gateskey
Feature: Create Gates Key on Sierra API and validate all expected permissions are present
  Background:

    * def now = function(){ return java.lang.System.currentTimeMillis() }
   # * def name = 'GatesSierra5Bar7BundleTest-' + now()
    * def name = 'GatesQA' + now()
    * def wait = read('classpath:wait-util.js')
    * def sapi = karate.call('../util/get_sapi_token.feature')
    * call wait 2


  Scenario: Connect to SAPI as iii and get SAPI Token and then make a key for GATES

    Given url sapi_url +'/v5/'+ 'apiKeys?role=GATES'
    And header Content-Type = 'application/json'
    * header Authorization = 'Bearer ' + sapi.response.access_token
    And request
    """
    {

      owner: '#(name)',
      email: "ciara.brennan@iii.com",
      client_key : "GatesUser",
      roles: ["GATES"],
      expires:false
    }
    """
    When method post
    Then status 201
    * def key_id = response.id
    * def client_key = response.client_key
   And match response ==
   """
   {     "id":'#ignore',
         "owner":'#ignore',
         "email":'#ignore',
         "client_key":'#ignore',
         "roles":[
                   {"name":"GATES",
                    "tokenLifetime":"60",
                    "description":"Gates",
                    "permissions":[

                     "Bibs_Filter",
                     "Bibs_List",
                     "Bibs_Read",
                     "Bibs_Read_Marc",
                     "Bibs_Search",
                     "Branches_List",
                     "Branches_Read",
                     "Internal_Metadata_Read",
                     "Items_Filter",
                     "Items_List",
                     "Items_Read",
                     "Patrons_Checkouts_List",
                     "Patrons_Checkouts_Read",
                     "Patrons_Checkouts_Renew",
                     "Patrons_Filter",
                     "Patrons_Fines_List",
                     "Patrons_Fines_Read",
                     "Patrons_Hold_Delete",
                     "Patrons_Hold_List",
                     "Patrons_Hold_Read",
                     "Patrons_Hold_Request_Create",
                     "Patrons_Hold_Update",
                     "Patrons_List",
                     "Patrons_Read",
                     "Patrons_Verify_Pin",
                     "Patrons_Validate",
                     "Patrons_Find",
                     "Internal_Patrons_Hold_Form_Get",
                     "Internal_Patrons_Hold_Post",
                     "Courses_List",
                     "Bibs_Delete_Marc"
                                         ]}],
                     "enabled":true,
                     "created":'#ignore',
                     "createdBy":"APIKEYADMIN",
                     "pending":'#ignore'
   }
   """



    Given url sapi_url + '/v5/'+ 'apiKeys/' + key_id + '?role=GATES'
    * header Authorization = 'Bearer ' + sapi.response.access_token
    And header Content-Type = 'application/json'
    And request
    """
     {
        "action":"setsecret",
        "values": {"secret":"goodsecret"}
    }
    """
    When method post
    Then status 200

    #* call read('../../util/make_customer.feature') { key: '#(client_key)' }

