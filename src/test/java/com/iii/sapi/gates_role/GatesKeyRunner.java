package com.iii.sapi.gates_role;

import com.intuit.karate.junit4.Karate;
import com.intuit.karate.KarateOptions;
import org.junit.BeforeClass;
import org.junit.runner.RunWith;
import com.intuit.karate.cucumber.CucumberRunner;
import com.intuit.karate.cucumber.KarateStats;
import com.intuit.karate.junit4.Karate;
import cucumber.api.CucumberOptions;

import org.junit.runner.RunWith;

@RunWith(Karate.class)
@CucumberOptions(features = {"src/test/java/com/iii/sapi/gates_role/create_gates_api_key.feature"})
public class GatesKeyRunner {

}
