# inspire-bs-gates-qa-automation

Test scripts for QA processes on PR environments to make life easier. Work in progress, not final version.


Basic Steps:

Copy  the feature templates e.g. C:\.....\inspire-bs-gates-qa-automation\src\test\java\com\iii\inspire\gates_template\template.feature

Set variables to point to a PR ENV

For old docker compose [OBSOLETE I THINK]

    * def custDomain = "admin.ds-conv-bs-gates-polaris-pr-11.iii-conv.com"
    * def envUrl = "ds-conv-bs-gates-polaris-pr-11.iii-conv.com"
 
 OR for K8s   

    * def custDomain = "admin.ds-conv-bs-gates-polaris-pr-11.inspire.dev""
    * def envUrl = "ds-conv-bs-gates-polaris-pr-11.iii-inspire.dev"

The last api request on page will be the one calling Gates

e.g.


         Given url 'https://' + envUrl + '/api/gates-edge/gates/locations'
         And header Content-Type = 'application/json'
         And header Authorization = 'Bearer ' + access_token
         And header iii-customer-domain = cust_domain
         And header iii-customer-site-code = site_code
         And header iii-customer-id = customer_id
         And header X-III-POLARIS-ORG-ID = 3
         And header Accept-Encoding =  '*'
         And header api-version = 2
         When method get
         Then status 200
         
Then you can run feature with RMB Context Menu(Right Mouse Button) in IntelliJ

After test runs you can check Karate report in Web Browser via:

C:/your-folder/your-folder/inspire-bs-gates-qa-automation/target/surefire-reports/src.test.java.com.iii.inspire.customer.create_customer_and_check_locations_k8s.html


Troubleshooting: If PKIX error happens when trying to run locally follow steps on wiki to add cacert to Java
https://wiki.iii.com/display/ROAR/Entering+to+PR+envs
[ error e.g. javax.net.ssl.SSLHandshakeException: PKIX path building failed: sun.security.provider.certpath.SunCertPathBuilderException: unable to find valid certification path to requested target)

/// TODO
 - Start making branches
 - Clean out old code
 - refactor

